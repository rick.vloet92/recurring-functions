# libraries
import pandas as pd 
import numpy as np 
import re


# variabels
COLUMNS = [
    'invoice_number',
    'description', 
    'date',
    'tracking_number',
    'weight_class',
    'real_price'
]


TYPES = ['CUI', 
         'Devolución Paquetería Empresas Z1',
         'Devolución Paquetería Empresas Z3',
         'Devolución Paquetería Empresas Z4',
         'Devolución Paquetería Empresas Z5',
         'Paq Estándar Entrega en CityPaq Z1',
         'Paq Estándar Entrega en CityPaq Z2',
         'Paq Estándar Entrega en CityPaq Z3',
         'Paq Estándar Entrega en Domicilio Z 7',
         'Paq Estándar Entrega en Domicilio Z1',
         'Paq Estándar Entrega en Domicilio Z2',
         'Paq Estándar Entrega en Domicilio Z3', 
         'Dim. extra',
         'Paq Estándar Entrega en Domicilio Z4',
         'Paq Estándar Entrega en Domicilio Z5',
         'Paq Estándar Entrega en Domicilio Z6',
         'Paq Estándar Entrega en Oficina Z1',
         'Paq Estándar Entrega en Oficina Z2',
         'Paq Estándar Entrega en Oficina Z3',
         'Paq Estándar Entrega en Oficina Z4',
         'Paq Estándar Entrega en Oficina Z5',
         'Paq Premium Entrega en CityPaq Z2',
         'Paq Premium Entrega en Domicilio Z 7',
         'Paq Premium Entrega en Domicilio Z1',
         'Paq Premium Entrega en Domicilio Z2',
         'Paq Premium Entrega en Domicilio Z3',
         'Paq Premium Entrega en Domicilio Z4',
         'Paq Premium Entrega en Domicilio Z5',
         'Paq Premium Entrega en Oficina Z4', 
         'Paq Retorno Z1',
         'Paq Retorno Z2', 
         'Paq Retorno Z3', 
         'Standard insur.',
         'Paq Retorno Z4', 
         'Devolución Paquetería Empresas Z2', 
         'Paq Premium Entrega en CityPaq Z3', 
         'Paq Premium Entrega en Domicilio Z6', 
         'Paq Premium Entrega en Oficina Z3', 
         'Devolución Paquetería Empresas Z 7', 
         'Paq Estándar Entrega en CityPaq Z4', 
         'Paq Estándar Entrega en Domicilio Z 8', 
         'Paq Premium Entrega en Oficina Z2', 
         'Paq Estándar Entrega en Oficina Z6', 
         'Paq Premium Entrega en Oficina Z1', 
         'Paq Retorno Z6']


RETURNS = [ 'Paq Retorno Z1',
            'Paq Retorno Z2', 
            'Paq Retorno Z3', 
            'Paq Retorno Z4', 
            'Paq Retorno Z6']


SURCHARGES = ['Dim. extra']


OTHER = ['Standard insur.', 
         'Autorización Previa para la Entrega',]


WEIGHT_DICT = {
    '0-50 grs': 0.05,
    'Hasta 50 grs.': 0.05, 
    'De 51 a 100 grs.': 0.1, 
    '51-100grs': 0.1,
    '101-250 grs': 0.25,
    'De 101 a 250 grs.': 0.25, 
    '0-250 grs': 0.25,
    'De 251 a 500 grs.':0.5, 
    '251-500 grs':0.5, 
    'De 501 a 1.000 grs.':1,
    '501-1000 grs':1, 
    'De 1.001 a 1.500 grs.':1.5, 
    '1001-2000 grs':2, 
    'De 1.501 a 2.000 grs.':2,
    '2001-3000 grs':3, 
    '3001-4000 grs':4,
    '4001-5000 grs':5, 
    '5001-6000 grs':6, 
    '6001-7000 grs':7,
    '7001-8000 grs':8, 
    '8001-9000 grs':9, 
    '9001-10000 grs':10,
    '10001-11000 grs':11,
    '11001-12000 grs':12,
    '12001-13000 grs':13, 
    '13001-14000 grs':14,
    '14001-15000 grs':15,
    '15001-16000 grs':16,
    '16001-17000 grs':17,
    '17001-18000 grs':18,
    '18001-19000 grs':19,
    '19001-20000 grs':20,
    '20001-21000 grs':21, 
    '21001-22000 grs':22, 
    '22001-23000 grs':23,
    '23001-24000 grs':24, 
    '24001-25000 grs':25, 
    '25001-26000 grs':26,    
    '26001-27000 grs':27, 
    '27001-28000 grs':28, 
    '28001-29000 grs':29,
    '29001-30000 grs':30,
    '34001-35000 grs': 35,
    '59001-60000 grs': 60    
}


# functions
def get_invoice_data(invoice):
    colnames= [str(i) for i in range(14)]
    df = pd.read_csv(invoice, 
                     header=None, 
                     delimiter='¬', 
                     engine='python', 
                     names=colnames,
                     encoding='utf-8-sig')
    df = df.iloc[24:, :].copy()

    return df


def get_select_columns(df):  
    df = df[df['3'] == 'DET'].copy()
    df.reset_index().drop(['index'], axis=1)
    df = df.iloc[1:, :].reset_index().drop(['index'], axis=1)
    df = df.iloc[:, [0, 5, 6, 7, 8, 11]].copy()
    df.columns = COLUMNS
    df['date'] = [i if i != '00000000' else np.nan for i in list(df['date'])]
    df[['date', 'tracking_number', 'weight_class']] = df[['date', 'tracking_number', 'weight_class']].ffill(axis=0)
    df['real_price'] = df['real_price'].str.replace(',', '.').astype(float)
    df['weight'] = [WEIGHT_DICT.get(i) if i in WEIGHT_DICT.keys() else np.nan for i in list(df['weight_class'])]
    df['date'] = pd.to_datetime(df['date'])

    return df


def get_divide_types(df):
    df = df[df['real_price'] != 0]
    not_created = df[df['description_panel'].isna()]
    df = df[~df['description_panel'].isna()]
    returns = df[df['description'].isin(RETURNS)].copy()
    surcharges = df[df['description'].isin(SURCHARGES)].copy()
    other = df[df['description'].isin(OTHER)].copy()
    shipments = df[(~df['description'].isin(RETURNS)) & (~df['description'].isin(SURCHARGES)) & (~df['description'].isin(OTHER))]

    return shipments, returns, surcharges, not_created, other    


def get_weight_class_correos(df):
    if df['international'] == False:
        if df['weight'] <= 1:
            return 'kg=0-1'
        elif df['weight'] <= 2:
            return 'kg=1-2'
        elif df['weight'] <= 3:
            return 'kg=2-3'
        elif df['weight'] <= 4:
            return 'kg=3-4'
        elif df['weight'] <= 5:
            return 'kg=4-5'
        elif df['weight'] <= 10:
            return 'kg=5-10'
        elif df['weight'] <= 15:
            return 'kg=10-15'
        elif df['weight'] <= 20:
            return 'kg=15-20'
        elif df['weight'] <= 30:
            return 'kg=20-30'
        else:
            return 'max'   
    else:
        if df['weight'] <= 0.05:
            return 'kg=0-0.05'
        elif df['weight'] <= 0.1:
            return 'kg=0.05-0.1'
        elif df['weight'] <= 0.25:
            return 'kg=0.1-0.25'
        elif df['weight'] <= 0.5:
            return 'kg=0.25-0.5'
        elif df['weight'] <= 1:
            return 'kg=0.5-1'
        elif df['weight'] <= 1.5:
            return 'kg=1-1.5'
        elif df['weight'] <= 2:
            return 'kg=1.5-2'
        else:
            return 'max'


def get_code_correos(df):
    if df['international'] == False:
        return re.sub(r'kg=\d+\-\d+', df['weight_class_correos'], df['code'])
    else:
        if df['weight'] <= 0.05:
            return re.sub(r'kg=\d+\-\d+\.\d+', df['weight_class_correos'], df['code'])
        elif df['weight'] <= 1:
            return re.sub(r'kg=\d+\.\d+\-\d+\.\d+', df['weight_class_correos'], df['code'])
        elif df['weight'] <= 1.5:
            return re.sub(r'kg=\d+\-\d+\.\d+', df['weight_class_correos'], df['code'])
        else:
            return re.sub(r'kg=\d+\.\d+\-\d+', df['weight_class_correos'], df['code'])


def get_real_price_comparison_dataframe(shipments, returns):
    df = pd.concat([shipments, returns])
    df['international'] = df['code'].str.contains('international')
    df['weight_class_correos'] = df.apply(get_weight_class_correos, axis=1)
    df['code_correos'] = df.apply(get_code_correos, axis=1)

    df['filter'] = (df['code_correos'] == df['code'])
    diff = df[df['filter'] == False]
    df = df[df['filter'] == True].copy()
    df = df.drop('filter', axis=1)    

    gr = pd.DataFrame(df.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_correos', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_correos']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_correos'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_correos'] = gr['shipments'] * gr['real_price_correos']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('#shipments * dif')
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(2) 

    return gr, diff 


def get_price_check_summary(gr, diff, not_created, surcharges, other, sc):
    gr = pd.DataFrame({'description': ['shipping method sendcloud == shipping method carrier'], 'count': [gr['shipments']['total']], 'value': [gr['#shipments * real_price_correos']['total']]}).set_index('description')

    diff['description'] = 'shipping method sendcloud != shipping method carrier'
    diff = pd.DataFrame(diff.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    diff.columns = ['count', 'value']

    not_created['description'] = 'parcel not created at sendcloud'
    not_created = pd.DataFrame(not_created.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    not_created.columns = ['count', 'value']

    surcharges = pd.DataFrame(surcharges.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    surcharges.columns = ['count', 'value']

    other = pd.DataFrame(other.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    other.columns = ['count', 'value']

    df = pd.concat([gr, diff, not_created, surcharges, other])
    df = df.sort_values('value', ascending=False)
    df = df.append(df.sum().rename('total')).round(2)
    df['distribution'] = df.iloc[:, -1] / sc.real_price.sum()

    return df    


def get_invoice_parcel_check(i):
    ts = pd.read_excel(i, sheet_name='ts')
    ts.columns = ['description', 'count', 'value', 'distribution']
    ts = ts[ts['description'].isin(['delivered', 'collected_by_customer'])].distribution.sum().round(4)

    not_created = pd.read_excel(i, sheet_name='nc')

    dn = pd.read_excel(i, sheet_name='di').real_price.sum()

    if len(not_created) == 0:
        nc = len(not_created)
        nv = 0
    else:
        nc = len(not_created['7'].unique())
        nv = not_created['11'].str.replace(',', '.').astype(float).sum()

    dp = pd.read_excel(i, sheet_name='dp')
    if len(dp) == 0:
        dc = len(dp)
        dv = 0
    else:
        dc = len(dp.tracking_number.unique())
        dv = dp.real_price.sum()

    np = pd.read_excel(i, sheet_name='np')
    np = np.iloc[-1,-1]

    na = pd.read_excel(i, sheet_name='nan')
    lst = []

    for i in range(len(na)):
        if na['nan_value'][i] != 0:
            lst.append(na['column'][i])

    if len(lst) == 0:
        na = 'no NaN values'
    else:
        na = lst

    return ts, nc, nv, dc, dv, np, na, dn


def get_invoice_price_check(i):
    df = pd.read_excel(i, sheet_name='price-check')
    di = df['#shipments * dif'][len(df) -1]
    ca = df['#shipments * real_price_correos'][len(df) -1]

    return di, ca


def get_invoice_summary(di, ca, nc, nv, dc, dv, np, na, sc, ts, dn):
    df = pd.DataFrame({'checked amount of carrier invoice': [ca],
                       'deviation in real_price between carrier and sendcloud: ': [di],
                       'percentage of parcels that are delivered: ': [''],
                       '# parcels not created via sendcloud: ': [nc],
                       'invoice amount parcels not created via sendcloud: ': [nv],
                       '# direct parcels invoiced by carrier: ': [dc],
                       'invoice amount direct parcels invoiced by carrier: ': [dv],
                       'new descriptions added to the invoice: ': np, 
                       '# columns with NaN values: ': [na], 
                       'amount double invoiced: ': [dn],
                       'total invoice value: ': [sc.real_price.sum()]}).transpose()

    df.columns = ['metrics']
    df['relative'] = ''
    df['relative']['checked amount of carrier invoice'] = (df['metrics']['checked amount of carrier invoice'] / sc.real_price.sum()).round(4)
    df['relative']['deviation in real_price between carrier and sendcloud: '] = (df['metrics']['deviation in real_price between carrier and sendcloud: '] / ca).round(4)
    df['relative']['invoice amount parcels not created via sendcloud: '] = (df['metrics']['invoice amount parcels not created via sendcloud: '] / sc.real_price.sum()).round(4)
    df['relative']['percentage of parcels that are delivered: '] = ts

    return df  