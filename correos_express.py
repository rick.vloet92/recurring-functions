# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import re
from sqlalchemy import create_engine
import datetime as dt 


# variabels
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
PRICING = 'C:\\Users\\Rick Vloet\\Documents\\carrier-pricing\\pricing_postnl.csv'
DATE_RANGE = str((dt.date.today() - dt.timedelta(290)))
NAMES = [
    'shipment_number', 
    'date',
    'weight', 
    'real_price', 
    'invoice_number'
]

     


# queries
panel_data_query = """
select 
    p.tracking_number, 
    json(p.extra_data) ->> 'shipment_number' as "shipment_number",
    ii.description "description_panel", 
    ii.price "price_panel",
    ii.real_price "real_price_panel", 
    p.weight "weight_panel", 
    p.shipping_method_id, 
    (case when sm."returns" then sc.iso_2
    	  else rc.iso_2 end) "from_country",
    (case when sm."returns" then rc.iso_2
    	  else sc.iso_2 end) "shipping_country", 
    sm.code,
    p.collo_count,
    p.direction,
    cc."type" "contract_type",
    p.parent_parcel_status_id
from 
    parcel p
left join invoice_item ii
	on p.parcel_id = ii.parcel_id
left join shipping_country sc
	on p.shipping_country_id = sc.shipping_country_id
left join shipping_country rc 
	on p.from_country_id = rc.shipping_country_id
left join shipping_method sm 
	on p.shipping_method_id = sm.shipping_method_id
left join contracts_contract cc 
    on cc.id = p.contract_id
where
     p.carrier_code = 'correos_express' 
and 
    json(p.extra_data) ->> 'shipment_number' in {}
"""


parcel_data_query = """
select
    p.tracking_number,
    json(p.extra_data) ->> 'shipment_number' as "shipment_number", 
    p.weight "weight_panel", 
    p.shipping_method_id, 
    (case when sm."returns" then sc.iso_2
    	  else rc.iso_2 end) "from_country",
    (case when sm."returns" then rc.iso_2
    	  else sc.iso_2 end) "shipping_country", 
    sm.code,
    p.collo_count,
    p.direction,
    cc."type" "contract_type",
    p.parent_parcel_status_id
from 
    parcel p
left join shipping_country sc
	on p.shipping_country_id = sc.shipping_country_id
left join shipping_country rc 
	on p.from_country_id = rc.shipping_country_id
left join shipping_method sm 
	on p.shipping_method_id = sm.shipping_method_id
left join contracts_contract cc 
    on cc.id = p.contract_id
where   
    p.announced_at >= '{}'    
and 
    json(p.extra_data) ->> 'shipment_number' in {}     
"""


invoice_item_data_query = """
select
    p.tracking_number, 
    ii.description "description_panel", 
    ii.price "price_panel",
    ii.real_price "real_price_panel"
from 
    parcel p
left join invoice_item ii
	on p.parcel_id = ii.parcel_id
where
    ii."type" = 'shipment'
and
    ii.description != 'Reversed cancellation'    
and
    p.announced_at >= '{}'    
and 
    p.tracking_number in {} 
"""


# functions
def get_invoice_data(path, invoice):
    df = pd.read_csv(path + invoice + '.csv', 
                     encoding='latin-1', 
                     sep=',',
                     skiprows=[0,1], 
                     converters={'Nº ENVIO': lambda i: str(i), 
                                 'PESO KILOS': lambda i : i.replace(',', '.'), 
                                 'IMP. BASE': lambda i: i.replace(',', '.')})
    df = df.copy()
    df['invoice_number'] = invoice

    return df


def get_select_columns(df):
    df = df.iloc[:, [0, 1, 4, 6, -1]].copy()
    df.columns = NAMES
    df[['weight', 'real_price']] = df[['weight', 'real_price']].astype(float)
    df['shipment_number'] = df['shipment_number'].astype(str).str.strip()
    df['date'] = df['date'].astype(str)
    df['date'] = ['0' + i if len(i) ==7 else i for i in list(df['date'])]
    df['date'] = [(str(i[:2]) + '-' + str(i[2:4]) + '-' + str(i[4:])) for i in list(df['date'])]
    df['date'] = pd.to_datetime(df['date'])

    return df    


def get_panel_details(df):
    tn = list(df['shipment_number'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(panel_data_query.format(tu), con=ENGINE)
    df = df.join(pa.set_index('shipment_number'), on='shipment_number', how='left')
    df['tracking_number'] = df['tracking_number'].fillna('nomatch')
    
    return df      


def get_panel_details(df):
    tn = list(df['shipment_number'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(parcel_data_query.format(DATE_RANGE, tu), con=ENGINE)
    df = df.join(pa.set_index('shipment_number'), on='shipment_number', how='left')
    df['tracking_number'] = df['tracking_number'].fillna('nomatch')
    tn = list(df['tracking_number'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(invoice_item_data_query.format(DATE_RANGE, tu), con=ENGINE)
    df = df.join(pa.set_index('tracking_number'), on='tracking_number', how='left')
    
    return df      


def get_weight_class_correos_express(df):
    """
    Function that returns the weight class based on the weight measured by the carrier.
    """
    if df['weight'] <= 1:
        return 'kg=0-1'
    elif df['weight'] <= 2:
        return 'kg=1-2'
    elif df['weight'] <= 3:
        return 'kg=2-3'
    elif df['weight'] <= 4:
        return 'kg=3-4'
    elif df['weight'] <= 5:
        return 'kg=4-5'
    elif df['weight'] <= 10:
        return 'kg=5-10'
    elif df['weight'] <= 15:
        return 'kg=10-15'
    else:
        return 'kg=15-40'
  


def get_correos_express_code(df):
    """
    Function that returns the method code.
    """
    if pd.isnull(df['description_panel']) == False:
        return re.sub(r'kg=\d+\-\d+', df['weight_class_correos_express'], df['code'])
    else:
        return np.nan


def get_real_price_comparison_dataframe(df):
    """
    Returns a DataFrame where the price between panel and mondial relay are compared.
    """
    df['weight_class_correos_express'] = df.apply(get_weight_class_correos_express, axis=1)

    df['correos_express_code'] = df.apply(get_correos_express_code, axis=1)
    df['filter'] = (df['correos_express_code'] == df['code'])
    # diff = df[df['filter'] == False]
    # diff['weight_panel'] = diff['weight_panel'].fillna(0)
    # diff['filter'] = df['weight'] > df['weight_panel']
    # ws = diff[diff['filter'] == True]
    # cr = diff[diff['filter'] == False]
    df = df[df['filter'] == True]
    
    df = df.drop('filter', axis=1)
    
    gr = pd.DataFrame(df.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_correos_express', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_correos_express']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_correos_express'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_correos_express'] = gr['shipments'] * gr['real_price_correos_express']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('dif (%)')
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(2) 

    # return df, gr, ws, cr

    return df, gr