# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import datetime as dt 
import numpy as np
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine


# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})


# queries
broker_rev_query = """
select
	ii.user_id,
	ii."type",
	date_trunc('month', "date") "date",
	uc.iso_2 "country_user",
	sum(ii.price) "price",
	sum(ii.price - ii.real_price) "net_rev"
from
	invoice_item ii 
left join users_invoiceaddress ui 
	on ii.user_id = ui.user_id
left join shipping_country uc 
	on ui.country_id = uc.shipping_country_id
where	
	"type" not in ('subscription', 
				   'subscription_refund')	
and
	"date" between '{}' and '{}'
group by ii.user_id, ii."type", date_trunc('month', "date"), uc.iso_2
"""


broker_rev_query = """
select
	ii.user_id,
	ii."type",
	date_trunc('month', "date") "date",
	ii."country_user",
	sum(ii.price) "price",
	sum(ii."net_rev") "net_rev",
	sum(ii."parcels") "parcels"
from
(select
	ii.user_id,
	ii."type",
	ii."date",
	uc.iso_2 "country_user",
	ii.price,
	(ii.price - ii.real_price) "net_rev",
	(case when ii."type" = 'shipment' then 1
	      when ii."type" = 'parcel_cancelled' then -1
		  else 0 end)  "parcels"
from
	invoice_item ii 
left join users_invoiceaddress ui 
	on ii.user_id = ui.user_id
left join shipping_country uc 
	on ui.country_id = uc.shipping_country_id
where	
	"type" not in ('subscription', 
				   'subscription_refund')	
and
	"date" between '{}' and '{}') ii
group by ii.user_id, date_trunc('month', ii."date"), ii."type", ii."country_user"	
"""


# broker_rev_query = """
# select
# 	ii.user_id,
# 	ii."type",
# 	date_trunc('month', "date") "date",
# 	ii."country_user",
# 	ii.carrier_id,
# 	sum(ii.price) "price",
# 	sum(ii."net_rev") "net_rev",
# 	sum(ii."parcels") "parcels"
# from
# (select
# 	ii.user_id,
# 	ii."type",
# 	ii."date",
# 	uc.iso_2 "country_user",
# 	ii.carrier_id,
# 	ii.price,
# 	(ii.price - ii.real_price) "net_rev",
# 	(case when ii."type" = 'shipment' then 1
# 	      when ii."type" = 'parcel_cancelled' then -1
# 		  else 0 end)  "parcels"
# from
# 	invoice_item ii 
# left join users_invoiceaddress ui 
# 	on ii.user_id = ui.user_id
# left join shipping_country uc 
# 	on ui.country_id = uc.shipping_country_id
# where	
# 	"type" not in ('subscription', 
# 				   'subscription_refund')	
# and
# 	"date" between '{}' and '{}') ii
# group by ii.user_id, date_trunc('month', ii."date"), ii."type", ii."country_user", ii.carrier_id	
# """


# functions
def get_broker_data(start_date, end_date):
    df = pd.read_sql_query(broker_rev_query.format(start_date, end_date), con=ENGINE)

    return df   


def get_broker_cohort_month_with_treshhold(df, tresh_hold):
    lst = []

    for i in range(len(df)):
        if i == 0: 
            if df['net_rev_tot'][i] >= tresh_hold:
                lst.append(df['date'][i])
            else:
                lst.append(np.nan)
        elif (df['user_id'][i] == df['user_id'][i - 1]):
            if pd.isnull(lst[i - 1]) == False:
                lst.append(lst[i - 1])
            elif df['net_rev_tot'][i] >= tresh_hold:
                lst.append(df['date'][i])    
            else:
                lst.append(np.nan)
        else:
            if df['net_rev_tot'][i] >= tresh_hold:
                lst.append(df['date'][i])
            else:
                lst.append(np.nan)

    return lst


def get_broker_data_manipulations(df, broker_types, tresh_hold):
    df['date'] = df['date'].apply(pd.to_datetime, utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]
    df['country_user'] = [i.replace('AD', 'FR') for i in list(df['country_user'])]
    df['country_user'] = [i.replace('MC', 'FR') for i in list(df['country_user'])]
    ot = df[~df['type'].isin(broker_types)]
    df = df[df['type'].isin(broker_types)]
    df = pd.DataFrame(df.groupby(['user_id', 'type', 'date', 'country_user']).agg({'price':sum, 'net_rev':sum, 'parcels':sum}).reset_index())
    df.columns = ['user_id', 'type', 'date', 'country_user', 'price', 'net_rev', 'parcels']
    
    nr = pd.DataFrame(df.groupby(['user_id', 'date']).agg({'net_rev':sum}).reset_index())
    nr.columns = ['user_id', 'date', 'net_rev_tot']

    df = df.merge(nr, how='left', left_on=['user_id', 'date'], right_on=['user_id', 'date'])

    df = df.sort_values(['user_id', 'date']).reset_index().drop(['index'], axis=1)
    df['cohort_month'] = get_broker_cohort_month_with_treshhold(df, tresh_hold)
    fr = df[df['cohort_month'].isna()]
    df = df[~df['cohort_month'].isna()]
    
    return df, ot, fr


def get_broker_data_manipulations_2(df, broker_types):
    df['date'] = df['date'].apply(pd.to_datetime, utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]
    df['country_user'] = [i.replace('AD', 'FR') for i in list(df['country_user'])]
    df['country_user'] = [i.replace('MC', 'FR') for i in list(df['country_user'])]
    ot = df[~df['type'].isin(broker_types)]
    df = df[df['type'].isin(broker_types)]
    df = pd.DataFrame(df.groupby(['user_id', 'type', 'date', 'country_user']).agg({'price':sum, 'net_rev':sum, 'parcels':sum}).reset_index())
    df.columns = ['user_id', 'type', 'date', 'country_user', 'price', 'net_rev', 'parcels']
    
    # nr = pd.DataFrame(df.groupby(['user_id', 'date']).agg({'price':sum, 'net_rev':sum, 'parcels':sum}).reset_index())
    # nr.columns = ['user_id', 'date', 'price_tot', 'net_rev_tot', 'parcels_tot']

    # df = df.merge(nr, how='left', left_on=['user_id', 'date'], right_on=['user_id', 'date'])
    
    return df, ot


# def get_broker_data_manipulations_2(df, broker_types):
#     df['date'] = df['date'].apply(pd.to_datetime, utc=True)
#     df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
#     df['date'] = [i.tz_localize(None) for i in list(df['date'])]
#     df['country_user'] = [i.replace('AD', 'FR') for i in list(df['country_user'])]
#     df['country_user'] = [i.replace('MC', 'FR') for i in list(df['country_user'])]
#     df['carrier_id'] = df['carrier_id'].fillna(100000)
#     ot = df[~df['type'].isin(broker_types)]
#     df = df[df['type'].isin(broker_types)]
#     df = pd.DataFrame(df.groupby(['user_id', 'type', 'date', 'country_user', 'carrier_id']).agg({'price':sum, 'net_rev':sum, 'parcels':sum}).reset_index())
#     df.columns = ['user_id', 'type', 'date', 'country_user', 'carrier_id', 'price', 'net_rev', 'parcels']
    
#     return df, ot    


def get_broker_data_manipulations_broker_users(df, broker_types):
    df['date'] = df['date'].apply(pd.to_datetime, utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]
    df['country_user'] = [i.replace('AD', 'FR') for i in list(df['country_user'])]
    df['country_user'] = [i.replace('MC', 'FR') for i in list(df['country_user'])]
    ot = df[~df['type'].isin(broker_types)]
    df = df[df['type'].isin(broker_types)]
    df = pd.DataFrame(df.groupby(['user_id', 'type', 'date', 'country_user']).agg({'price':sum, 'net_rev':sum, 'parcels':sum}).reset_index())
    df.columns = ['user_id', 'type', 'date', 'country_user', 'price', 'net_rev', 'parcels']
    
    return df, ot   