# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import datetime as dt 
import numpy as np
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine


# variabels
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
SAAS_DATE_LIMIT = pd.to_datetime(dt.date.today())


BROKER_TYPES = ['shipment',
                'pickup',
            	'surcharge',
            	'insurance_refund',
            	'pickup_cancelled',
            	'insurance',
            	'parcel_cancelled', 
                'pickup_subscription']


COLUMNS = ['invoice_item_id', 'description', 'reference', 'price', 'date',
          'invoice_id', 'user_id', 'type', 'parcel_id', 'pickup_id', 'carrier_id',
          'from_country_id', 'real_price', 'internal_note',
          'user_subscription_id', 'currency', 'country_user']

# query
broker_query = """
select 
	ii.*, 
	sc.iso_2 "country_user"
from 
	invoice_item ii
left join (select 
		   		ui.user_id, 
				sc.iso_2
		   from 
			   	users_invoiceaddress ui 
		   left join shipping_country sc 
				on ui.country_id = sc.shipping_country_id) sc
	on ii.user_id = sc.user_id
where 
	ii."date" between '{}' and '{}'
and 
	ii."type" in {}
"""


saas_query = """
select 
	ii.*, 
    su.*, 
    sc."country_user"
from 
	invoice_item ii
left join (select 
				cast(su.id as varchar), 
                su.started_at, 
                su.expires_at, 
                ss.plan_type, 
                sp.price "price_plan",
                su.price_override,
                ss.code
            from subscriptions_usersubscription su 
                left join subscriptions_subscription ss
                    on su.subscription_id = ss.id
                    left join subscriptions_subscriptionprice sp 
                        on su.subscription_id = sp.subscription_id
                        where sp.currency = 'EUR') su 
                                on ii.reference = su.id
left join (select 
                ui.user_id, sc.iso_2 "country_user" 
            from 
                users_invoiceaddress ui 
            left join shipping_country sc 
                on ui.country_id = sc.shipping_country_id) sc 
    on ii.user_id = sc.user_id
where 
	ii."date" between '{}' and '{}'
and 
	ii."type" in ('subscription', 'subscription_refund')
and
	su.plan_type in ('monthly', 'yearly')
and
	su.code != 'pickup'	
and
	ii.user_id in (select user_id from users_invoiceaddress where email not ilike '%%@sendcloud%%')
"""


# functions
def get_broker_data(start_date, end_date):  
    tu = tuple(BROKER_TYPES[i] for i in range(len(BROKER_TYPES)))
    df = pd.read_sql_query(broker_query.format(start_date, end_date, tu), con=ENGINE)
    df['country_user'] = df['country_user'].replace('MC', 'FR')
    df['country_user'] = df['country_user'].replace('AD', 'FR')
    df['date'] = pd.to_datetime(df['date'], utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]
    df['date'] = df['date'].apply(general_carrier.get_first_day_of_month)

    # print('unique country users: ', df['country_user'].unique())

    return df


def get_subscription_months_remaining(df):
    if df['plan_type'].item() == 'yearly':
        if int(np.ceil(((df['expires_at'] - df['date'])/np.timedelta64(1, 'M')))) > 12:
            return 12
        else:
            return int(np.ceil(((df['expires_at'] - df['date'])/np.timedelta64(1, 'M'))))         
    else:
        if (12 - (df['date'] - df['started_at'])/np.timedelta64(1, 'M')).item() >= 0:
            if (12 - int(np.ceil((df['date'] - df['started_at'])/np.timedelta64(1, 'M')))) == 0:
                return 1
            elif (12 - int(np.ceil((df['date'] - df['started_at'])/np.timedelta64(1, 'M')))) == 12:
                return 12
            else: 
                return 12 - int(((df['date'] - df['started_at'])/np.timedelta64(1, 'M')))
        else:
            return int(np.ceil(((df['expires_at'] - df['date'])/np.timedelta64(1, 'M'))))


def get_spread_out_subscription_refund(df):
    df = df[(df['plan_type'] == 'yearly') & (df['type'] == 'subscription_refund')]

    reference = list(df['invoice_item_id'])
    frames = []
    
    for ref in reference:
        dff = df[df['invoice_item_id'] == ref]
    
        remaining_months = get_subscription_months_remaining(dff)
        
        if remaining_months > 0:
            index = dff.index[0]
            price = dff['price'][index]
            price = price / 12
            
            dff = dff.copy()
            dff = pd.concat([dff] * remaining_months, ignore_index=True)
            dff['price'] = round(price, 2)
            
            invoice_month = list(dff['date'])
            invoice_month_to_be = []
            x = 0
            
            for date in invoice_month:
                invoice_month_to_be.append(date + relativedelta(months=x))
                x = x + 1
                
            dff['date'] = invoice_month_to_be
            frames.append(dff)
            
    df = pd.concat(frames) 
    
    return df


def get_spread_out_subscription(df):
    df = df[df['type'] == 'subscription']
    reference = list(df['invoice_item_id'])
    frames = []
    
    for ref in reference:    
        dff = df[df['invoice_item_id'] == ref]
        dff = pd.concat([dff]*12, ignore_index=True)
        
        index = dff.index[0]
        price = dff['price'][index]
        dff['price'] = round(price / 12, 2)
        
        invoice_month = list(dff['date'])
        invoice_month_to_be = []
        x = 0
            
        for date in invoice_month:
            invoice_month_to_be.append(date + relativedelta(months=x))
            x = x + 1
                
        dff['date'] = invoice_month_to_be    
        frames.append(dff)
        
    df = pd.concat(frames)
    
    return df


def get_price_subscription_refund_yearly(df):
    if df['type'] == 'subscription_refund':
        if df['price'] == 0:
            return 0
        elif np.isnan(df['price_override']) == False:
            return -df['price_override']   
        else:
            return -df['price_plan']    
    else:
        return df['price']                       


def get_saas_data(start_date, end_date):  
    df = pd.read_sql_query(saas_query.format(start_date, end_date), con=ENGINE)
    df['country_user'] = df['country_user'].replace('MC', 'FR')
    df['country_user'] = df['country_user'].replace('AD', 'FR')
    df['date'] = pd.to_datetime(df['date'], utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]

    # print('unique country users: ', df['country_user'].unique())

    return df    


def get_monthly_subscriptions(df):
    df = df[(df['plan_type'] == 'monthly') & (df['type'] == 'subscription')]
    df = df[(df['date'].dt.day == 1) & (df['date'].dt.hour <= 7)]
    
    return df     


def get_yearly_subscriptions(df):
    df = df[df['plan_type'] == 'yearly']
    df['price_override'].replace(to_replace=[None], value=np.nan, inplace=True)
    df['price'] = df.apply(get_price_subscription_refund_yearly, axis=1)
    df['date'] = [i if i.day == 1 and i.hour <= 7 else i + relativedelta(months=1) for i in list(df['date'])]
    df['date'] = df['date'].apply(general_carrier.get_first_day_of_month)
    df['expires_at'] = df['expires_at'].apply(general_carrier.get_first_day_of_month)
    su = get_spread_out_subscription(df)
    sr = get_spread_out_subscription_refund(df)
    df = pd.concat([su, sr])

    return df     


def get_monthly_and_yearly_subscriptions(monthly, yearly):
    df = pd.concat([monthly, yearly])
    df = df[df['date'] < SAAS_DATE_LIMIT]
    df['date_m'] = df['date'].apply(general_carrier.get_first_day_of_month)
    df = df.sort_values(['date', 'user_id'], ascending=False)
    df = df.drop_duplicates(['date_m', 'user_id'])
    df['date'] = df['date'].apply(general_carrier.get_first_day_of_month)
    df = df[COLUMNS]

    return df     


def get_parcels_shipped_data(path):    
    df = pd.read_csv(path, low_memory=False)
    df['median'] = df.iloc[:, 2:].median(numeric_only=True, axis=1)
    df['segment'] = df['median'].apply(general_carrier.get_user_segment)
    df = df[['user_id', 'median', 'segment']]
    df = df.dropna()
    
    return df    


def get_broker_subscription_and_parcel_data(br, sa, segment):
    df = pd.concat([br, sa])
    df = df.join(segment.set_index('user_id'), on='user_id', how='left')
    df = df.dropna(subset=['segment'])
    df['mrr'] = df['price'] - df['real_price']
    
    return df    


def get_pivot_table(df, column, agg):
    df = df.pivot_table(index=['country_user', 'segment'], columns='date', values=column, aggfunc=agg).round()
    df = df.append(df.sum().rename('global'))    
    
    return df    


def get_active_users_per_country_per_segment(df):
    df = df[['user_id', 'date', 'segment', 'country_user']]
    df = df.sort_values(['user_id', 'date'])    
    df = df.drop_duplicates(['user_id', 'date'])
    df = df.pivot_table(index=['country_user', 'segment'], columns='date', values='user_id', aggfunc='count')
    df = df.append(df.sum().rename('global'))      
    
    return df


def get_average_per_country_per_segment(data, active):
    df = data.divide(active).round()
    
    return df    