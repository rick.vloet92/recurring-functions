# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import re
from sqlalchemy import create_engine


# variables
PRICING = 'C:\\Users\\Rick Vloet\\Documents\\carrier-pricing\\pricing_postnl_fulfilment.csv'


PRICING_DICT = {
    'Overschrijding maximum pakket dimensies': 74.95, 
    'Handmatige verwerking': 3.05,
    'Toeslag pakketten >23kg': 3.05,
    'Formaat pakket groter dan 100 dm³': 1.50,
    'Formaat pakket groter dan 50 dm³': 0.5,
    'Formaat pakket groter dan 200 dm³': 3.01
} 


MASK = ['tracking_number', 
        'description_4', 
        'price', 
        'real_price', 
        'type', 
        'carrier', 
        'from_country',
        'currency', 
        'internal_note']


NMG = [329, 320, 327, 328, 330, 317, 331, 332, 333]


def get_weight_class_postnl(df):
    if df['weight'] <= 23:
        return 'kg=0-23'
    else:
        return 'kg=23-31.5'


def get_code_postnl(df):
    if df['code'] in ['postnl_fulfilment:standard/eu']:
        return 'postnl:standard/eu,non_conveyable'
    else:
        if df['weight'] <= 23:
            return re.sub(r'kg=\d+\-\d+', df['weight_class_postnl'], df['code'])
        else:
            return re.sub(r'kg=\d+\-\d+\.\d+', df['weight_class_postnl'], df['code'])


def get_real_price_comparison_dataframe(df):
    df = df.copy()
    df['weight_class_postnl'] = df.apply(get_weight_class_postnl, axis=1)
    df['code_postnl'] = df.apply(get_code_postnl, axis=1)
    df['filter'] = (df['code_postnl'] == df['code'])

    diff = df[df['filter'] == False]
    df = df[df['filter'] == True]

    gr = pd.DataFrame(df.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_postnl', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_postnl']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_postnl'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_postnl'] = gr['shipments'] * gr['real_price_postnl']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('#shipments * dif')
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(4)   
    
    return gr, diff


def get_postnl_surcharge_price(i):
    """
    Returns the price of the surcharge based on the description.
    """
    return PRICING_DICT.get(i)


def get_overschrijding_maximale_dimensies_description_postnl(df):
    """
    Function that returns the correct description regarding the overschrijving maximale dimensies surcharge.
    """
    if df['weight'] > 31500:
        return 'Toeslag overschrijding maximale dimensies - gewogen gewicht: ' + str(df['weight']) + ' (gr)'
    else:
        return 'Toeslag overschrijding maximale dimensies - afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) + ' (mm)'


def get_overschrijding_maximale_dimensies_toeslag_postnl(df):
    """
    Returns a DataFrame of the overschrijving maximale dimensies toeslag.
    """
    df = df[df['description_4'] == 'Overschrijding maximum pakket dimensies'].copy()
    df['price'] = df['description_4'].apply(get_postnl_surcharge_price)
    df['description_4'] = df.apply(get_overschrijding_maximale_dimensies_description_postnl, axis=1)
    df = df[(df['length'] > 1750) | (df['width'] > 780) | (df['height'] > 580) | (df['weight'] > 31500)]
    print('# overschrijding maximale dimensies: ', len(df))

    return df


def get_niet_machine_geschikt_description_postnl(df):
    if df['length'] <= 1750 and (df['length'] > 1000 or df['length'] < 100):
        return 'Toeslag niet machine geschikt - afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) +' (mm)'
    elif df['width'] <= 780 and (df['width'] > 700 or df['width'] < 100):
        return 'Toeslag niet machine geschikt - afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) +' (mm)'
    elif df['height'] < 10:
        return 'Toeslag niet machine geschikt - afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) +' (mm)'        
    else:
        return 'Toeslag wegens aard van verpakking'        


def get_handmatige_verwerking_toeslag_postnl(df):
    """
    Returns a DataFrame of the handmatige verwerking surcharge.
    """
    df = df[df['description_4'] == 'Handmatige verwerking'].copy()
    pa = df[df['shipping_method_id'].isin(NMG)]
    print('# NMG created: ', len(pa))
    df = df[~df['tracking_number'].isin(list(pa['tracking_number']))]
    df['price'] = df['description_4'].apply(get_postnl_surcharge_price)
    df['description_4'] = df.apply(get_niet_machine_geschikt_description_postnl, axis=1)
    print('# handmatige verwerking: ', len(df))
    
    return df


def get_toeslag_pakketten_23kg(df):
    """
    Returns a DataFrame of the toeslag pakketten > 23 kg.
    """
    df = df[df['description_4'] == 'Toeslag pakketten >23kg'].copy()
    pa = df[df['shipping_method_id'].isin(NMG)]
    print('# NMG created: ', len(pa))
    df = df[~df['shipping_method_id'].isin(NMG)]
    df['price'] = df['description_4'].apply(get_postnl_surcharge_price)
    df['description_4'] = 'Toeslag pakket zwaarder dan 23kg - gewogen gewicht: ' + df['weight'].astype(str) + ' (gr)'
    print('# toeslag pakket > 23kg: ', len(df))
    
    return df


def get_retouren_postnl(df):
    """
    Returns a DataFrame of the return surcharges.
    """
    re = df[(df['description_5'] == 'Retouren') & (df['real_price'] > 0) & (~df['price_panel'].isna()) & (df['direction'] == 'o')].copy()
    re['price'] = re['price_panel']
    re['description_4'] = 'Pakket retour afzender'
    print('# returns: ', len(re))
    
    return re


def get_piek_toeslag_postnl(df):
    """
    Returns a DataFrame of the piek surcharge.
    """
    df = df[df['description_4'] == 'Piek toeslag']
    df['description_4'] = 'PostNL piektoeslag'
    df['price'] = df['real_price']
    print('# piektoeslag: ', len(df))
    
    return df


def get_liter_toeslagen_postnl(df):
    df = df[df['description_4'].isin(['Formaat pakket groter dan 50 dm³', 'Formaat pakket groter dan 200 dm³', 'Formaat pakket groter dan 100 dm³'])].copy()
    df.loc[df['description_4'] == 'Formaat pakket groter dan 50 dm³', 'price'] = 0.5
    df.loc[df['description_4'] == 'Formaat pakket groter dan 100 dm³', 'price'] = 1.5
    df.loc[df['description_4'] == 'Formaat pakket groter dan 200 dm³', 'price'] = 3.01
    print('# volume toeslagen postnl: ', len(df))

    return df    


def get_surcharges_postnl(df):
    """
    Returns a DataFrame of all surcharges.
    """
    df = df[~df['length'].isna()].copy()
    df['length'] = [int(i) for i in list(df['length'])]
    df['height'] = [int(i) for i in list(df['height'])]
    df['width'] = [int(i) for i in list(df['width'])]
    df['weight'] = [int(i) for i in list(df['weight'])]
    ma = get_overschrijding_maximale_dimensies_toeslag_postnl(df)
    ha = get_handmatige_verwerking_toeslag_postnl(df)
    tp = get_toeslag_pakketten_23kg(df)
    re = get_retouren_postnl(df)
    # pi = get_piek_toeslag_postnl(df)
    li = get_liter_toeslagen_postnl(df)
    df = pd.concat([ma, ha, tp, re, li])
    df = df.dropna(subset=['description_panel'])
    df['carrier'] = 'postnl_fulfilment'
    df['type'] = 'surcharge'
    df['currency'] = 'EUR'
    df['from_country'] = 'NL'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df = df[MASK]
    df.columns = ['tracking_number', 
                  'description', 
                  'price', 
                  'real_price', 
                  'type', 
                  'carrier', 
                  'from_country',
                  'currency', 
                  'internal_note']

    return df


def get_globalpack_surcharges_postnl(df):
    """
    Returns a DataFrame of the weight allowances surcharges postnl.
    """
    df = df[(df['description_1'] == 'Pakketten niet-EU') & (df['weight'] > 0) & (df['description_4'] == 'Collo')]
    df['weight_class'] = df['weight'].apply(np.ceil)
    df['weight_class_panel'] = df['weight_panel'].apply(np.ceil)
    df['filter'] = df['weight_class'] - df['weight_class_panel']
    df = df[df['filter'] > 0]
    df['code'] = df['code'] + 'NL' + df['shipping_country']    

    pa = pd.read_csv(PRICING)
    pa = pa[pa['weight_allowance'] > 0]
    pa['method_code'] = pa['method_code'] + 'NL' + pa['to_country']
    
    pr = pa[['method_code', 'weight_allowance']]
    rp = pa[['method_code', 'real_weight_allowance']]
    
    df = df.join(pr.set_index('method_code'), on='code', how='left')
    df = df.join(rp.set_index('method_code'), on='code', how='left')
    
    df['price'] = df['filter'] * df['weight_allowance']
    df['real_price'] = df['filter'] * df['real_weight_allowance']
    df = df[df['price'] > 0]
    df['description_4'] = 'Surcharge - selected weight: ' + df['weight_panel'].astype(str) + ' (kg)' + ' measured weight: ' + df['weight'].astype(str) + ' (kg)'    
    df['carrier'] = 'postnl_fulfilment'
    df['type'] = 'surcharge'
    df['currency'] = 'EUR'
    df['from_country'] = 'NL'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df = df[MASK]
    df.columns = ['tracking_number', 
                  'description', 
                  'price', 
                  'real_price', 
                  'type', 
                  'carrier', 
                  'from_country',
                  'currency', 
                  'internal_note']

    return df 