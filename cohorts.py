# libraries

import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import datetime as dt 
import numpy as np
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine


# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})

# queries

check_ii_query = """
select
	distinct("type"),
	max(price),
	min(price)
from
	invoice_item
where 
	"date" between '{}' and '{}'
group by "type"	
"""



# functions

def get_check_ii(start_date, end_date):
    date_range = pd.date_range(start_date, end_date, freq='YS')
    start = date_range[:-1]
    end = date_range[1:]
    lst = []

    for i in range(len(start)):
        df = pd.read_sql_query(check_ii_query.format(str(start[i])[:10], str(end[i])[:10]), con=ENGINE)
        df['start_date'] = str(start[i])[:10]
        df['end_date'] = str(end[i])[:10]
        lst.append(df)

    df = pd.concat(lst)

    return df


def get_fremium_users_on_parcels(df):
    if pd.isnull(df['cohort_month']):
        return False
    elif pd.to_datetime(df['date']) >= pd.to_datetime(df['cohort_month']):
        return True 
    else:
        return False         


def get_broker_and_subscription_data(pa, bm):
    df = pd.concat([pa, bm]).copy()
    df['date'] = df['date'].apply(pd.to_datetime, utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]
    df['cohort_month'] = df.groupby('user_id')['date'].transform('min')
    df['date'] = [i.date() for i in list(df['date'])]
    df['cohort_month'] = [i.date() for i in list(df['cohort_month'])]
    
    return df  


def get_broker_and_subscription_data_2(pa, bm, cohort_month):
    bm = bm.merge(cohort_month, how='left', left_on=['user_id'], right_on=['user_id']).copy()
    bm['filter'] = bm.apply(get_fremium_users_on_parcels, axis=1)
    # bm = bm.drop(['net_rev', 'parcels', 'price'], axis=1).drop_duplicates(['user_id', 'date'])
    # bm.columns = ['user_id', 'type', 'date', 'country_user', 'price', 'net_rev', 'parcels', 'cohort_month', 'filter']
    bm_fr = bm[bm['filter'] == False].copy()
    bm = bm[bm['filter'] == True].copy()
    bm_rb = bm[bm['type'].isin(['shipment', 'parcel_cancelled'])].copy()
    bm_ob = bm[~bm['type'].isin(['shipment', 'parcel_cancelled'])].copy()
    bm = bm.drop(['filter', 'type'], axis=1)

    pa = pa.merge(cohort_month, how='left', left_on=['user_id'], right_on=['user_id']).copy()
    pa['filter'] = pa.apply(get_fremium_users_on_parcels, axis=1)
    pa_fr = pa[pa['filter'] == False].copy()
    pa = pa[pa['filter'] == True].copy()
    pa = pa.drop(['filter'], axis=1)

    df = pd.concat([pa, bm]).copy()
    df['date'] = df['date'].apply(pd.to_datetime, utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]
    df['cohort_month'] = pd.to_datetime(df['cohort_month'])
    df['date'] = [i.date() for i in list(df['date'])]
    df['cohort_month'] = [i.date() for i in list(df['cohort_month'])]
    
    return df, bm, pa, bm_fr, pa_fr, bm_rb, bm_ob


def get_broker_and_broker_data_2(bm, cohort_month):
    """
    broker only
    """
    bm = bm.merge(cohort_month, how='left', left_on=['user_id'], right_on=['user_id']).copy()
    bm['filter'] = bm.apply(get_fremium_users_on_parcels, axis=1)
    # bm = bm.drop(['net_rev', 'parcels', 'price'], axis=1).drop_duplicates(['user_id', 'date'])
    # bm.columns = ['user_id', 'type', 'date', 'country_user', 'price', 'net_rev', 'cohort_month', 'filter']
    bm_fr = bm[bm['filter'] == False].copy()
    bm = bm[bm['filter'] == True].copy()
    bm_rb = bm[bm['type'].isin(['shipment', 'parcel_cancelled'])].copy()
    bm_ob = bm[~bm['type'].isin(['shipment', 'parcel_cancelled'])].copy()
    bm = bm.drop(['filter', 'type'], axis=1)

    df = bm.copy()
    df['date'] = df['date'].apply(pd.to_datetime, utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]
    df['cohort_month'] = pd.to_datetime(df['cohort_month'])
    df['date'] = [i.date() for i in list(df['date'])]
    df['cohort_month'] = [i.date() for i in list(df['cohort_month'])]
    
    return df, bm_fr, bm_rb, bm_ob  


def get_pivot_table_source(df, br, sn, value, agg):
    df = pd.DataFrame(df.groupby(['type']).agg({value:agg}).reset_index())
    df.columns = ['type', value]

    br = pd.DataFrame(br.groupby(['type']).agg({value:agg}).reset_index())
    br.columns = ['type', value]

    df = pd.concat([df, br]).copy()

    sn = get_pivot_table(sn, value, agg)
    sn = pd.DataFrame({'type':['subscription_no_plan_type'], value:[sn.sum().sum()]})

    df = df.append(sn)
    df = df.append(df.sum(numeric_only=True).rename('total'))

    return df


def get_pivot_table(df, value, agg):
    df = df.pivot_table(columns=['date'], values=value, aggfunc=agg)

    return df       


def get_pivot_table_parcel_cohort(df):
    lst = []

    for i in ('price', 'net_rev', 'parcels'):
        a = df.pivot_table(columns=['date'], values=i, aggfunc=sum)
        a.index = [i]
        lst.append(a)

    df = pd.concat(lst)
    df = df.iloc[:, ::-1]

    return df


def get_total_overview(sn, ot, pa, bm_rb, bm_ob, bm_fr, pa_fr, value, agg, names):
    lst = []
    x = 0

    for i in (sn, ot, pa, bm_rb, bm_ob, bm_fr, pa_fr):
        i = i.copy()
        i['date'] = i['date'].apply(general_carrier.get_first_day_of_month)
        pi = get_pivot_table(i, value, agg)
        pi.index = [names[x]]
        x += 1
        lst.append(pi)

    df = pd.concat(lst)
    df = df.iloc[:, ::-1]

    return df        


def get_pivot_table(df, value, agg):
    df = df.pivot_table(index=['type', 'country_user'], columns=['date'], values=value, aggfunc=agg)

    return df       


def get_total_overview(sn, ot, pa, bm_rb, bm_ob, bm_fr, pa_fr, value, agg, names):
    lst = []
    x = 0

    for i in (sn, ot, pa, bm_rb, bm_ob, bm_fr, pa_fr):
        i = i.copy()
        i['type'] = names[x]
        pi = get_pivot_table(i, value, agg)
        # pi.index = [names[x]]
        x += 1
        lst.append(pi)

    df = pd.concat(lst)
    df = df.iloc[:, ::-1]

    return df    


def get_date_int(df, column):
    year = df[column].dt.year
    month = df[column].dt.month
    day = df[column].dt.day
    
    return year, month, day


def get_cohort(df, column, aggregation, date):
    df = df.copy()
    df['date'] = pd.to_datetime(df['date'])
    df['cohort_month'] = pd.to_datetime(df['cohort_month'])
    df = df[df['date'] < date]
    invoice_year, invoice_month, _ = get_date_int(df, 'date')
    cohort_year, cohort_month, _ = get_date_int(df, 'cohort_month')
    years_diff = invoice_year - cohort_year
    months_diff = invoice_month - cohort_month
    df['cohort_index'] = years_diff * 12 + months_diff
    cdt = df.groupby(['cohort_month', 'cohort_index']).agg({column:aggregation}).reset_index()
    uc = cdt.pivot(index='cohort_month', 
                   columns='cohort_index', \
                   values=column)
    uc.index = [str(i)[:10] for i in uc.index]
    uc = uc.append(uc.sum().rename('total'))
    uc = uc.round()
    
    return uc       


def get_inverted_cohort(df, column, aggregation, date):
    df = df.copy()
    df['date'] = pd.to_datetime(df['date'])
    df['cohort_month'] = pd.to_datetime(df['cohort_month'])
    df = df[df['date'] < date]
    cdt = df.groupby(['cohort_month', 'date']).agg({column:aggregation}).reset_index()
    uc = cdt.pivot(index='cohort_month', 
                   columns='date', 
                   values=column)
    uc.columns = [str(i)[:10] for i in uc.columns]
    uc = uc.iloc[:, ::-1]
    uc.index = [str(i)[:10] for i in uc.index]
    uc = uc.append(uc.sum().rename('total'))
    uc = uc.round()
    
    return uc    


def get_parcel_cohort_data(bm):
    ob = bm[~bm['type'].isin(['shipment', 'parcel_cancelled'])].copy()
    bm = bm[bm['type'].isin(['shipment', 'parcel_cancelled'])].copy()
    bm = pd.DataFrame(bm.groupby(['user_id', 'date', 'country_user', 'cohort_month']).agg({'price':sum, 'net_rev':sum, 'parcels':sum}).reset_index())
    bm.columns = ['user_id', 'date', 'country_user', 'cohort_month', 'price', 'net_rev', 'parcels']

    return bm, ob    


def get_quarter_integer(df):
    lst = []
    x = 0

    for i in range(len(df)):
        if i == 0:
            lst.append(x)
        elif (df['user_id'][i] == df['user_id'][i - 1]):
            if (df['quarter'][i] == df['quarter'][i - 1]):
                lst.append(x)
            else:
                x = x + 1
                lst.append(x)
        else:
            x = 0
            lst.append(x)

    return lst


def get_quarter_cohort(df, column, date, aggregation):
    df = df[df['date'] < date]
    # df = df.sort_values(['user_id', 'date']).reset_index().drop(['index'], axis=1)
    # df['quarter'] = pd.PeriodIndex(df['date'] ,freq='Q')
    # df['quarter_integer'] = get_quarter_integer(df)
    # df['min_quarter'] = df.groupby('user_id')['quarter'].transform('min')
    df = df.pivot_table(index='min_quarter', columns='quarter_integer', values=column, aggfunc=aggregation)
    df = df.append(df.sum().rename('total'))

    return df


def get_inverted_quarter_cohort(df, column, date, aggregation):
    df = df[df['date'] < date]
    # df = df.sort_values(['user_id', 'date']).reset_index().drop(['index'], axis=1)
    # df['quarter'] = pd.PeriodIndex(df['date'] ,freq='Q')
    # df['min_quarter'] = df.groupby('user_id')['quarter'].transform('min')
    df = df.pivot_table(index='min_quarter', columns='quarter', values=column, aggfunc=aggregation)
    df = df.append(df.sum().rename('total'))

    return df


def get_cohort_month_user_data(path):
    df = pd.read_csv(path)
    df = df.dropna()
    df = df[['user_id', 'cohort_month']].copy()
    df['cohort_month'] = pd.to_datetime(df['cohort_month'], dayfirst=True)

    return df