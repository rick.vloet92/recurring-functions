# libraries

import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd
import datetime as dt
from sqlalchemy import create_engine
import numpy as np


# variables

CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
DATE_RANGE = str((dt.date.today() - dt.timedelta(290)))


COLUMNS_BPOST = ['Invoice Number', 
                 'Date', 
                 'Contract', 
                 'Barcode', 
                 'Product', 
                 'Quantity',
                 'Description', 
                 'Weight']


NAMES_BPOST = ['invoice_number', 
               'date', 
               'contract', 
               'tracking_number', 
               'product', 
               'quantity',
               'description', 
               'weight']


TYPES_BPOST = []       


SHIPMENT = ['BPACK 24H PRO',
            'BPACK@BPOST PRO',
            'BPACK 24/7',
            'BPACK EASY RETOUR']


ADD_ON = ['Signature', 
          'Warranty',
          'Saturday Delivery']


COLUMNS_POSTNL = ['FACTUUR_NUMMER',
                  'AANBIED_NUMMER',
                  'TRANSACTIE_DATUM',
                  'COLLO',
                  'PRODUCT_OMSCHRIJVING',
                  'OMSCHRIJVING_NIVEAU_2',
                  'OMSCHRIJVING_NIVEAU_3',
                  'OMSCHRIJVING_NIVEAU_4',
                  'OMSCHRIJVING_NIVEAU_5',
                  'AANTAL_EENHEID',
                  'ONTVANGER_LAND',
                  'COLLO_HOOGTE',
                  'COLLO_LENGTE',
                  'COLLO_BREEDTE',
                  'COLLO_GEWICHT',
                  'COLLO_VOLUME']       


NAMES_POSTNL = ['invoice_number',
                'contract',
                'date',
                'tracking_number',
                'description_1',
                'description_2',
                'description_3',
                'description_4',
                'description_5',
                'count_unit',
                'country_postnl',
                'height',
                'length',
                'width',
                'weight',
                'volume']           


PRICES = ['Collo', 'Zending', 'Kg', 'Ophalen bij een PostNL locatie', 'Ophalen bij een pakketautomaat', 'Notificatie bezorgstatus (SMS)']          


SURCHARGES = ['Overschrijding maximum pakket dimensies', 'Piek toeslag', 'Niet (correct) voorgemeld', 'Handmatige verwerking', 
              'Herlabeling', 'Schotland', 'Retour afzender', 'Noord-Ierland', 'Label',
              '2de levering', 'Supplement Back To Sender', 
             'Supplement for drop in retail network', 
             'Supplement Parcel Density', 
             'Supplement Manual Handling (cylindrical shape)',
             'Supplement illegible Shipping Label',
             'Supplement for Announcement Data Missing',
             'Supplement for Peak Period', 'Retour Afzender', 'Corsica', 'Handmatige verwerking', 'HV',
             'Morbihan (Eiland)', 'Corsica ', 'Belle-Île-en-Mer',
             'Geen relefoonnummer of e-mailadres bijgevoegd']                            


COLUMNS_HERMES = ['Zendingsnummer', 
                  'Referentie', 
                  'Datum', 
                  'Staffel',
                  'invoice_number']       


NAMES_HERMES = ['tracking_number',
                'contract',
                'date',
                'description',
                'invoice_number'] 

# queries

panel_data_query = """
select
    p.tracking_number, 
    p.weight "weight_panel", 
    p.shipping_method_id, 
    (case when sm."returns" then sc.iso_2
    	  else rc.iso_2 end) "from_country",
    (case when sm."returns" then rc.iso_2
    	  else sc.iso_2 end) "shipping_country", 
    sm.code,
    p.collo_count,
    p.carrier_code,
    p.direction,
    cc."type" "contract_type",
    p.parent_parcel_status_id,
    p.user_id,
    ui.company_name
from 
    parcel p
left join shipping_country sc
	on p.shipping_country_id = sc.shipping_country_id
left join shipping_country rc 
	on p.from_country_id = rc.shipping_country_id
left join shipping_method sm 
	on p.shipping_method_id = sm.shipping_method_id
left join contracts_contract cc 
    on cc.id = p.contract_id
left join users_invoiceaddress ui 
    on p.user_id = ui.user_id
where   
    p.announced_at >= '{}'    
and
    p.tracking_number in {}   
"""


panel_data_order_number_query = """
select
    p.tracking_number "tracking_number_sendcloud", 
    p.order_number,
    p.weight "weight_panel", 
    p.shipping_method_id, 
    (case when sm."returns" then sc.iso_2
    	  else rc.iso_2 end) "from_country",
    (case when sm."returns" then rc.iso_2
    	  else sc.iso_2 end) "shipping_country", 
    sm.code,
    p.collo_count,
    p.carrier_code,
    p.direction,
    cc."type" "contract_type",
    p.parent_parcel_status_id,
    p.user_id,
    ui.company_name
from 
    parcel p
left join shipping_country sc
	on p.shipping_country_id = sc.shipping_country_id
left join shipping_country rc 
	on p.from_country_id = rc.shipping_country_id
left join shipping_method sm 
	on p.shipping_method_id = sm.shipping_method_id
left join contracts_contract cc 
    on cc.id = p.contract_id
left join users_invoiceaddress ui 
    on p.user_id = ui.user_id
where   
    p.carrier_code = 'hermes_gb'        
and
    p.announced_at >= '{}'
and 
	p.order_number in {}
"""

# functions

def get_data_bpost(path):
    df = pd.ExcelFile(path)

    lst = []

    for i in df.sheet_names:
        if i != 'Export Summary':
            df = pd.read_excel(path, sheet_name=i)
            lst.append(df)

    df = pd.concat(lst)

    return df


def get_select_columns_bpost(df):
    df = df.reset_index().drop(['index'], axis=1)
    df['Description'] = [df['Product'][i] if pd.isnull(df['Description'][i]) == True else df['Description'][i] for i in range(len(df))]
    df = df[COLUMNS_BPOST]
    df.columns = NAMES_BPOST
    df['invoice_number'] = df['invoice_number'].astype(str)

    return df


def get_new_products_of_bpost(df):
    lst = []

    for i in list(df['product'].unique()):
        if i not in TYPES_BPOST:
            lst.append(i)

    if len(lst) > 0:
        df = pd.DataFrame({'new products':[lst]})
    else:
        df = pd.DataFrame({'new products':[['no products added!']]})
    
    return df     


def get_new_products_of_postnl(df):
    lst = []

    for i in list(df['description_4'].unique()):
        if i not in TYPES_BPOST:
            lst.append(i)

    if len(lst) > 0:
        df = pd.DataFrame({'new products':[lst]})
    else:
        df = pd.DataFrame({'new products':[['no products added!']]})
    
    return df       


def get_panel_details(df):
    tn = df[['tracking_number']]
    tn = tn.dropna().drop_duplicates('tracking_number')
    tn = list(df['tracking_number'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(panel_data_query.format(DATE_RANGE, tu), con=ENGINE)
    df = df.join(pa.set_index('tracking_number'), on='tracking_number', how='left')
    
    return df     


def get_product_pivot_bpost(df):
    df = pd.DataFrame(df.groupby('Product').agg({'Barcode':len}))
    df.columns = ['count']
    df = df.sort_values('count', ascending=False)
    df['distribution'] = df.iloc[:, -1] / df.iloc[:, -1].sum()
    df = df.append(df.sum(numeric_only=True).rename('total'))

    return df    


def get_tracking_status_pivot(df):
    df = df[~df['parent_parcel_status_id'].isna()].copy()
    df['status'] = [general_carrier.shipping_status_dic.get(i) if i in general_carrier.shipping_status_dic.keys() else i for i in list(df['parent_parcel_status_id'])]
    df['status'] = df['status'].fillna('NaN value')
    df = pd.DataFrame(df.groupby('status').agg({'tracking_number':pd.Series.nunique})).reset_index()
    df.columns = ['status', 'count']
    df['distribution'] = df.iloc[:, -1] / df.iloc[:, -1].sum()
    df = df.sort_values('distribution', ascending=False)
    df = df.append(df.sum(numeric_only=True).rename('total'))

    return df


def get_invoice_check_summary(df, np, nc, br, ts, di):
    df = pd.DataFrame({'Number of unmatched lines: ': [len(nc)],
                       'Number of items multiple times invoiced: ': [len(di)],
                       'Number of broker lines invoiced: ': [len(br)],
                       'Parcels delivered: ': [ts[ts['status'].isin(['delivered', 'collected_by_customer'])]['distribution'].sum()],

                       'Number of invoices: ': [len(df.invoice_number.unique())], 
                       'Number of unique tracking numbers invoiced: ': [len(df.tracking_number.unique())],
                       'Number of companies: ': [len(df[~df.company_name.isna()].company_name.unique())],
                       'Min invoice date: ': [min(df['date'])],
                       'Max invoice date: ': [max(df['date'])],                       
                       'Shipping country: ': [df[~df['shipping_country'].isna()].shipping_country.unique()]}).transpose()

    df.columns = ['metric']

    return df        


def get_select_columns_postnl(df):
    df = df.reset_index().drop(['index'], axis=1)
    df['COLLO'] = [df['ZENDING'][i] if pd.isnull(df['COLLO'][i]) else df['COLLO'][i] for i in range(len(df))]
    df = df[COLUMNS_POSTNL]
    df.columns = NAMES_POSTNL

    df['description_2'] = [df['country_postnl'][i] if pd.isnull(df['description_2'][i]) else df['description_2'][i] for i in range(len(df))]
    df['description_6'] = [df['description_4'][i] if pd.isnull(df['description_5'][i]) else df['description_4'][i] + ' (' + df['description_5'][i] + ')' for i in range(len(df))]
    df['height'] = [float(i) for i in list(df['height'])]
    df['length'] = [float(i) for i in list(df['length'])]
    df['width'] = [float(i) for i in list(df['width'])]
    df['weight'] = [float(i) for i in list(df['weight'])]
    df['volume'] = df['volume'].replace('', np.nan)
    df['volume'] = [float(i) for i in list(df['volume'])] 
    df['date'] = pd.to_datetime(df['date'])
    
    return df


def get_surcharges_postnl(df):
    df = df[df['description_4'].isin(SURCHARGES)].copy()

    return df    


def get_data_hermes_and_colis_prive(path):
    df = pd.ExcelFile(path)

    lst = []

    for i in df.sheet_names:
        df = pd.read_excel(path, sheet_name=i, skiprows=[0,1])
        df['invoice_number'] = i
        lst.append(df)

    df = pd.concat(lst)
    df['Zendingsnummer'] = df['Zendingsnummer'].astype(str)

    return df    


def get_add_digit_hermes_tracking_number(i):
    if len(i) != 16:
        return '0' + i
    else:
        return i

def get_select_columns_hermes(df, country):
    df = df[df['Land'] == country].copy()
    df = df.reset_index().drop(['index'], axis=1)

    lst = []

    pa = df[~df['Legendacode'].isna()]
    p1 = df[df['Legendacode'].isna()]

    for i in pa.index:
        x = pa[pa.index == i]
        x = pd.concat([x] * 2).reset_index().drop(['index'], axis=1)
        x.iloc[1, 4] = x['Legendacode'][0]
        lst.append(x)

    df = pd.concat(lst)

    df = pd.concat([p1, df])
    df = df[COLUMNS_HERMES]
    df.columns = NAMES_HERMES
    df['contract'] = df['contract'].astype(str)
    df['tracking_number'] = df['tracking_number'].astype(str)
    df['tracking_number'] = df['tracking_number'].apply(get_add_digit_hermes_tracking_number)

    return df


def get_panel_details_hermes(df):
    d1 = df[~df['company_name'].isna()].copy()
    d2 = df[(~df['contract'].isna()) & (df['company_name'].isna())].iloc[:, :5].copy()
    d3 = df[(df['contract'].isna()) & (df['company_name'].isna())].copy()

    tn = d2[['contract']]
    tn = tn.dropna().drop_duplicates('contract')
    tn = list(tn['contract'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(panel_data_order_number_query.format(DATE_RANGE, tu), con=ENGINE)
    d2 = d2.join(pa.set_index('order_number'), on='contract', how='left')

    df = pd.concat([d1, d2, d3])
    
    return df     


def get_surcharges_hermes(df):
    df = df[df['description'].isin(SURCHARGES)].copy()

    return df        


def get_items_double_invoiced(df, column):
    lst = []
    x = 0

    for i in range(len(df)):
        if i == 0:
            lst.append(x)
        elif (df['tracking_number'][i] == df['tracking_number'][i - 1]):
            if (df[column][i] == df[column][i - 1]):
                x = x + 1
                lst.append(x)
                lst[i-1] = x
            else:
                x = 0
                lst.append(x)
        else:
            x = 0
            lst.append(x)

    return lst


def get_double_items(df, column):
    df['count'] = df.groupby(column)[column].transform(len)
    df = df[df['count'] > 1].copy()
    df = df.sort_values(column)
    # df = df[(df['count'] != 1) & (df['collo_count'] == 1)].copy()

    return df
 

def get_double_items_ups(df):
    lst = []

    for i in df.description.unique():
        x = df[df['description'] == i].copy()
        x['count_description'] = x.groupby('tracking_number')['tracking_number'].transform(len)
        x = x[x['count_description'] > 1].copy()
        lst.append(x)

    pa = pd.concat(lst)

    return pa