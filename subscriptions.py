# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import datetime as dt 
import numpy as np
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine


# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})


# queries
subscription_rev_query = """
select 
	ii.*, 
	ii.price "net_rev",
    su.*, 
    sc."country_user"
from 
	invoice_item ii
left join (select 
		   	cast(su.id as varchar), 
                    su.started_at, 
                    su.expires_at, 
                    ss.plan_type, 
                    sp.price "price_plan",
                    su.price_override,
                    ss.code
			   from subscriptions_usersubscription su 
					left join subscriptions_subscription ss
						on su.subscription_id = ss.id
					left join subscriptions_subscriptionprice sp 
					    on su.subscription_id = sp.subscription_id
				where sp.currency = 'EUR') su 
					on ii.reference = su.id
left join (select 
				ui.user_id, sc.iso_2 "country_user" 
		    from 
				users_invoiceaddress ui 
			left join shipping_country sc 
				on ui.country_id = sc.shipping_country_id) sc 
	on ii.user_id = sc.user_id
where 
	ii."date" between '{}' and '{}'
and 
	ii."type" in ('subscription', 'subscription_refund')
and
	su.plan_type in ('monthly', 'yearly')
and
	su.code != 'pickup'	
and
	ii.user_id in (select user_id from users_invoiceaddress where email not ilike '%%@sendcloud%%')
"""


subscription_rev_without_tag_query = """
select
	ii.*,
	ii.price "net_rev",
	uc.iso_2 "country_user"
from 
	invoice_item ii 
left join users_invoiceaddress ui 
	on ii.user_id = ui.user_id
left join shipping_country uc 
	on ui.country_id = uc.shipping_country_id
where 
	ii."type" in ('subscription', 'subscription_refund')
and
    ii."date" between '{}' and '{}'    
and 
	ii.invoice_item_id not in {}
"""

# functions
def get_subscription_data(start_date, end_date):
    df = pd.read_sql_query(subscription_rev_query.format(start_date, end_date), con=ENGINE)

    return df


def get_subscription_data_reporting(start_date, end_date):
    start_date = pd.to_datetime(start_date) - relativedelta(months=14)
    df = pd.read_sql_query(subscription_rev_query.format(start_date, end_date), con=ENGINE)

    return df    


def get_subscrption_data_manipulations(df):
    df[['date', 'expires_at']] = df[['date', 'expires_at']].apply(pd.to_datetime, utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['expires_at'] = df['expires_at'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]
    df['expires_at'] = [i.tz_localize(None) for i in list(df['expires_at'])]
    df['date'] = [i.date() for i in list(df['date'])]
    df['expires_at'] = [i.date() for i in list(df['expires_at'])]
    df['country_user'] = [i.replace('AD', 'FR') for i in list(df['country_user'])]
    df['country_user'] = [i.replace('MC', 'FR') for i in list(df['country_user'])]
    df.loc[df['invoice_item_id'] == 19924109, 'price_plan'] = 199

    return df


def get_subtract_one_day_from_refund_date(df):
    if df['day_date'] == df['end_date_day']:
        return df['date'] + relativedelta(days=1)
    else:
        return df['date']


def get_yearly_subscription_refund_manipulations(df):
    df = df[(df['plan_type'] == 'yearly') & (df['type'] == 'subscription_refund')].reset_index().drop(['index'], axis=1)
    df['price_plan'] = [df['price_override'][i] if pd.isnull(df['price_override'][i]) == False else df['price_plan'][i] for i in range(len(df))]

    lst = []

    df['date_1'] = ''
    df['day_date'] = ''
    df['end_date_day'] = ''

    for i in range(len(df)):
        end = (df['expires_at'][i] + relativedelta(months=1)).replace(day=1) - dt.timedelta(1)
        start = df['date'][i]
        df.loc[i, 'date_1'] = (df['date'][i] + relativedelta(months=1)).replace(day=1) - dt.timedelta(1)
        df.loc[i, 'day_date'] = df['date'][i].day
        df.loc[i, 'end_date_day'] = df['date_1'][i].day

    df['date'] = df.apply(get_subtract_one_day_from_refund_date, axis=1)

    for i in range(len(df)):
        end = (df['expires_at'][i] + relativedelta(months=1)).replace(day=1) - dt.timedelta(1)
        start = df['date'][i]
        months_remaining = len(pd.date_range(start, end, freq='M'))
        if months_remaining == 0:
            lst.append(1)
        else:
            lst.append(months_remaining)

    df['months_remaining'] = lst
    df = df.drop(['date_1', 'day_date', 'end_date_day'], axis=1)

    lst = []

    for i in range(len(df)):
        x = df[df['invoice_item_id'] == df['invoice_item_id'][i]]
        x = pd.concat([x] * df['months_remaining'][i]).reset_index().drop(['index'], axis=1)
        ends = df['expires_at'][i]
        end = (df['expires_at'][i] + relativedelta(months=1)).replace(day=1) - dt.timedelta(1)
        start = df['date'][i]
        months_remaining = list(pd.date_range(start, end, freq='M'))
        x['date_1'] = months_remaining
        months_remaining[0] = pd.to_datetime(start)
        months_remaining[-1] = pd.to_datetime(ends)
        x['date'] = months_remaining
        x['days'] = x['date'].dt.day
        x['days_1'] = x['date_1'].dt.day
        x['day_dif'] = (x['days'] / x['days_1'])
        x['refund'] = [-x['price_plan'][i] / 12 if x['day_dif'][i] == 1 else 0 for i in range(len(x))]
        x['refund'] = [0 if x['price'][i] == 0 else x['refund'][i] for i in range(len(x))]

        refund = (x['price'][0] - x.refund.sum()).round(2)
        if len(x[x['refund'] == 0].reset_index().drop(['index'], axis=1)) > 1:
            x1 = x[x['refund'] == 0].reset_index().drop(['index'], axis=1)
            x2 = x[x['refund'] != 0].reset_index().drop(['index'], axis=1)
            x1.loc[x1.index == 0, 'day_dif'] = (1 - x1['day_dif'][0])
            x1['day_dif_sum'] = x1['day_dif'].sum()
            x1['day_dif'] = x1['day_dif'] / x1['day_dif_sum']
            x1['refund'] = (refund * x1['day_dif']).round(2)
            x = pd.concat([x1, x2])
            x['sum'] = x.groupby('invoice_item_id')['refund'].transform(sum)
            x['sum'] = x['sum'].round(2)
            x['filter'] = (x['price'] == x['sum'])
        else:
            x.loc[x['refund'] == 0, 'refund'] = refund
            x['sum'] = x.groupby('invoice_item_id')['refund'].transform(sum)
            x['sum'] = x['sum'].round(2)
            x['filter'] = (x['price'] == x['sum'])

        lst.append(x)

    pa = pd.concat(lst).reset_index().drop(['index'], axis=1)
    pa['price'] = pa['refund']
    pa = pa.drop(['months_remaining', 'date_1', 'days', 'days_1', 'day_dif', 'refund', 'sum', 'filter', 'day_dif_sum'], axis=1)

    return pa


def get_yearly_subscription_manipulations(df):
    df = df[(df['plan_type'] == 'yearly') & (df['type'] == 'subscription')].reset_index().drop(['index'], axis=1)
    lst = []

    for i in range(len(df)):
        x = df[df['invoice_item_id'] == df['invoice_item_id'][i]]
        x = pd.concat([x] * 12).reset_index().drop(['index'], axis=1)
        x['price'] = (x['price'] / 12)

        for d in list(range(len(x))):
            x.loc[d, 'date'] = (x['date'][d] + relativedelta(months=d))

        lst.append(x)

    df = pd.concat(lst).reset_index().drop(['index'], axis=1)

    return df


def get_monthly_subscription_manipulations(df):
    df = df[df['plan_type'] == 'monthly']

    return df


def get_monthly_and_yearly_subscription(yr, ys, ms):
    df = pd.concat([yr, ys, ms]).reset_index().drop(['index'], axis=1)
    df['date'] = df['date'].apply(general_carrier.get_first_day_of_month)
    df = pd.DataFrame(df.groupby(['user_id', 'date', 'country_user']).agg({'price':sum}).reset_index())
    df.columns = ['user_id', 'date', 'country_user', 'price']
    df['net_rev'] = df['price']
    
    return df


def get_monthly_and_yearly_subscription_monthly_reporting(yr, ys, ms):
    df = pd.concat([yr, ys, ms]).reset_index().drop(['index'], axis=1)
    # df['date'] = df['date'].apply(general_carrier.get_first_day_of_month)
    # df = pd.DataFrame(df.groupby(['user_id', 'date', 'country_user']).agg({'price':sum}).reset_index())
    # df.columns = ['user_id', 'date', 'country_user', 'price']
    # df['net_rev'] = df['price']
    
    return df    


def get_subscription_and_subscription_refund_items_without_plan_type(df, start_date, end_date):
    tn = list(df['invoice_item_id'])
    tu = tuple(tn[i] for i in range(len(tn)))
    df = pd.read_sql_query(subscription_rev_without_tag_query.format(start_date, end_date, tu), con=ENGINE)
    df['date'] = df['date'].apply(general_carrier.get_first_day_of_month)
    df['net_rev'] = df['price']

    return df


def get_subscription_and_subscription_refund_items_without_plan_type(df, start_date, end_date):
    tn = list(df['invoice_item_id'])
    tu = tuple(tn[i] for i in range(len(tn)))
    df = pd.read_sql_query(subscription_rev_without_tag_query.format(start_date, end_date, tu), con=ENGINE)
    df['date'] = df['date'].apply(general_carrier.get_first_day_of_month)
    df = pd.DataFrame(df.groupby(['user_id', 'date', 'country_user']).agg({'price':sum}).reset_index())
    df.columns = ['user_id', 'date', 'country_user', 'price']
    df['net_rev'] = df['price']

    return df   


def get_monthly_and_yearly_subscription_monthly_reporting(yr, ys, ms):
    df = pd.concat([yr, ys, ms]).reset_index().drop(['index'], axis=1)
    df['date'] = df['date'].apply(pd.to_datetime, utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]
    
    return df          


def get_subscription_and_subscription_refund_items_without_plan_type_monthly_reporting(df, start_date, end_date):
    tn = list(df['invoice_item_id'])
    tu = tuple(tn[i] for i in range(len(tn)))
    df = pd.read_sql_query(subscription_rev_without_tag_query.format(start_date, end_date, tu), con=ENGINE)
    df['date'] = df['date'].apply(pd.to_datetime, utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]

    return df 