# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import re
from sqlalchemy import create_engine
import glob


# variabels
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
PRICING = 'C:\\Users\\Rick Vloet\\Documents\\carrier-pricing\\pricing_postnl.csv'


COLUMNS = ['FACTUUR_NUMMER',
           'TRANSACTIE_DATUM',
           'COLLO',
           'PRODUCT_OMSCHRIJVING',
           'OMSCHRIJVING_NIVEAU_2',
           'OMSCHRIJVING_NIVEAU_3',
           'OMSCHRIJVING_NIVEAU_4',
           'OMSCHRIJVING_NIVEAU_5',
           'AANTAL',
           'AANTAL_EENHEID',
           'TARIEF',
           'ONTVANGER_LAND',
           'NETTO_BEDRAG',
           'COLLO_HOOGTE',
           'COLLO_LENGTE',
           'COLLO_BREEDTE',
           'COLLO_GEWICHT',
           'COLLO_VOLUME']


NAMES = ['invoice_number',
         'date',
         'tracking_number',
         'description_1',
         'description_2',
         'description_3',
         'description_4',
         'description_5',
         'count',
         'count_unit',
         'tarife',
         'country_postnl',
         'real_price',
         'height',
         'length',
         'width',
         'weight',
         'volume']


TYPES = ['Collo', 'Zending', 'Afleveren alleen huisadres', 'Handtekening voor ontvangst', 'Avondbezorging', 'Ophalen bij een PostNL locatie',
         'Handtek. voor ontvangst Alleen Huisadres', 'Handmatige verwerking', 'Niet (correct) voorgemeld', 'Notificatie bezorgstatus (SMS)',
         'Ophalen bij een pakketautomaat', 'Tolheffing', 'Verh. aansprakelijkheid', 'Kg', 'Overschrijding maximum pakket dimensies', 'Maandag bezorging', 
         'Toeslag pakketten >23kg', 'Leeftijdscheck18+', 'Laat Aanleveren', 'Formaat pakket groter dan 50 dm³', 'Formaat pakket groter dan 100 dm³', 
         'Formaat pakket groter dan 200 dm³', 'Kg Volumetrisch']


PRICES = ['Collo', 'Afleveren alleen huisadres', 'Handtekening voor ontvangst', 'Avondbezorging', 'Handtek. voor ontvangst Alleen Huisadres',
          'Verh. aansprakelijkheid', 'Kg', 'Ophalen bij een PostNL locatie', 'Zending', 'Leeftijdscheck18+', 'Ophalen bij een pakketautomaat', 'Maandag bezorging',
          'Notificatie bezorgstatus (SMS)']          


OTHER = ['Niet (correct) voorgemeld', 'Tolheffing', 'Laat Aanleveren']       


SURCHARGES = ['Handmatige verwerking', 'Overschrijding maximum pakket dimensies', 'Toeslag pakketten >23kg', 'Formaat pakket groter dan 50 dm³', 'Formaat pakket groter dan 100 dm³', 
              'Formaat pakket groter dan 200 dm³', 'Kg Volumetrisch']


DIM_MASK = ['tracking_number', 
            'weight', 
            'length', 
            'height', 
            'width', 
            'volume']


PRICING_MASK = ['tracking_number', 
                'description_4', 
                'real_price']


NMG = [293, 291, 294, 282, 272, 283, 274, 284, 275, 286, 277, 285, 276, 287, 278, 288, 279, 289, 280, 290, 281, 298, 296, 
      273, 292, 295, 297, 2242, 2243, 329, 327, 330, 331, 332, 333, 320]


NMG_INCREASE = 3.01


SERVICE = [7, 293]


SERVICE_DECREASE = -0.1


MASK = ['tracking_number', 
        'description_4', 
        'price', 
        'real_price', 
        'type', 
        'carrier', 
        'from_country',
        'currency', 
        'internal_note']


PRICING_DICT = {
    'Overschrijding maximum pakket dimensies': 74.95, 
    'Handmatige verwerking': 3.05,
    'Toeslag pakketten >23kg': 3.05
}        


# queries
parcels_created_at_sendcloud = """
select 
    tracking_number
from 
    parcel
where 
    carrier_code = 'postnl'
and    
    tracking_number in {}
"""


returns_query = """
select 
    reference, 
    price 
from 
    invoice_item
where 
    "type" = 'shipment' 
and 
    "date" > '{}'
and
    carrier_id = 2
and
    reference in {}
"""


nmg_created_query = """
select 
	p.tracking_number, 
    ii.description "description_panel" 
from 
	parcel p
left join invoice_item ii
    on p.parcel_id = ii.parcel_id
where 
    p.cancellation_status = 0
and
    p.shipping_method_id in (84, 273, 291, 292, 293, 294, 295, 297, 2242, 2243)
and
    p.tracking_number in {}
and
    ii."type" = 'shipment'    
"""


get_user_details_mailbox_credit = """
select
    cast(p.user_id as varchar), 
    p.tracking_number,
    p.carrier_code "carrier_name",
    sm.friendly_name,
    ui.company_name,
    ui.postal_code
from
    parcel p
left join shipping_method sm 
    on p.shipping_method_id = sm.shipping_method_id
left join users_invoiceaddress ui 
    on p.user_id = ui.user_id
where
    p.tracking_number in {}
and
    p.announced_at >= '{}'
and 
    p.shipping_method_id in (503, 504, 505, 506, 507, 39)
"""


# functions
def get_invoice_data_postnl(path, invoice):
    df = pd.read_csv(path + invoice + '.csv', 
                     encoding='latin-1', 
                     sep=';',
                     low_memory=False,
                     converters={'TARIEF': lambda i: i.replace(',', '.'), 
                                 'AANTAL': lambda i: i.replace(',', '.'),
                                 'NETTO_BEDRAG': lambda i: i.replace(',', '.'), 
                                 'COLLO_HOOGTE': lambda i: i.replace(',', '.'),
                                 'COLLO_LENGTE': lambda i: i.replace(',', '.'),
                                 'COLLO_BREEDTE': lambda i: i.replace(',', '.'),
                                 'COLLO_GEWICHT': lambda i: i.replace(',', '.'),
                                 'COLLO_VOLUME': lambda i: i.replace(',', '.')})
    
    return df   


def get_all_invoices_in_folder_postnl(path):
    inv = glob.glob(path + '/*.csv')

    df = pd.concat([pd.read_csv(i, 
                      encoding='latin-1', 
                      sep=';', 
                      converters={'TARIEF': lambda i: i.replace(',', '.'), 
                                 'AANTAL': lambda i: i.replace(',', '.'),
                                 'NETTO_BEDRAG': lambda i: i.replace(',', '.'), 
                                 'COLLO_HOOGTE': lambda i: i.replace(',', '.'),
                                 'COLLO_LENGTE': lambda i: i.replace(',', '.'),
                                 'COLLO_BREEDTE': lambda i: i.replace(',', '.'),
                                 'COLLO_GEWICHT': lambda i: i.replace(',', '.'),
                                 'COLLO_VOLUME': lambda i: i.replace(',', '.')}, 
                      low_memory=False, 
                      error_bad_lines=False) for i in inv])
            
    return df    


def get_select_columns_postnl(df):
    """
    description_6 is added for the ease of comparing the csv to the pdf invoice.
    """
    df = df.reset_index().drop(['index'], axis=1)
    df['COLLO'] = [df['ZENDING'][i] if pd.isnull(df['COLLO'][i]) else df['COLLO'][i] for i in range(len(df))]
    df = df[COLUMNS]
    df.columns = NAMES   

    df['description_2'] = [df['country_postnl'][i] if pd.isnull(df['description_2'][i]) else df['description_2'][i] for i in range(len(df))]
    df['description_6'] = [df['description_4'][i] if pd.isnull(df['description_5'][i]) else df['description_4'][i] + ' (' + df['description_5'][i] + ')' for i in range(len(df))]
    df['height'] = [float(i) for i in list(df['height'])]
    df['length'] = [float(i) for i in list(df['length'])]
    df['width'] = [float(i) for i in list(df['width'])]
    df['weight'] = [float(i) for i in list(df['weight'])]
    df['tarife'] = [float(i) for i in list(df['tarife'])]
    df['count'] = [float(i) for i in list(df['count'])]
    df['volume'] = df['volume'].replace('', np.nan)
    df['volume'] = [float(i) for i in list(df['volume'])] 
    df['real_price'] = [float(i) for i in list(df['real_price'])]
    df['date'] = pd.to_datetime(df['date'])
    
    return df    


def get_break_down_one_postnl(df):
    df = pd.DataFrame(df.groupby('description_1').agg({'real_price':sum}).sort_values('real_price', ascending=False)).reset_index()    
    df = df.append(df.sum(numeric_only=True).rename('total')).round(2)

    return df


def get_break_down_two_postnl(df):
    df = pd.DataFrame(df.groupby(['description_1', 'description_2', 'description_3', 'description_6', 'tarife']).agg({'count':sum, 'real_price':sum})).reset_index()    
    df = df.append(df.sum(numeric_only=True).rename('total')).round(2)

    return df


def get_new_products_of_carrier(df):
    lst = []

    for i in list(df['description_4'].unique()):
        if i not in TYPES:
            lst.append(i)

    if len(lst) > 0:
        print('the carrier added these products: ', lst)
        df = pd.DataFrame({'new products':[lst]})
    else:
        print('no products added!')
        df = pd.DataFrame({'new products':[['no products added!']]})
    
    return df


def get_parcels_not_created_at_sendcloud(df, pa): 
    pa = pa[pa['description_panel'].isna()]
    df = df[df['COLLO'].isin(list(pa['tracking_number']))]

    return df


def get_direct_parcels_invoiced_by_the_carrier(df):
    df = df[df['contract_type'] == 'direct']
    
    return df         


def get_nan_values_in_invoice_postnl(df):
    df = pd.DataFrame(df.isna().sum()).reset_index()
    df.columns = ['column', 'nan_value']    

    return df


def get_dimensions_postnl(df):
    di = df[df['weight'] > 0].copy()
    di = di[DIM_MASK]
    di = di.drop_duplicates('tracking_number')

    df = df.drop(['height', 'length', 'width', 'weight', 'volume'], axis=1).copy()
    df = df.join(di.set_index('tracking_number'), on='tracking_number', how='left')

    return df


def get_divide_types_postnl(df):
    df = df[df['real_price'] != 0]
    not_created = df[df['description_panel'].isna()]
    df = df[~df['description_panel'].isna()]
    shipments = df[(df['description_4'].isin(PRICES)) & (df['description_5'] != 'Retouren')]
    returns = df[(df['description_4'].isin(PRICES)) & (df['description_5'] == 'Retouren')]
    surcharges = df[df['description_4'].isin(SURCHARGES)]
    other = df[df['description_4'].isin(OTHER)]

    return shipments, returns, surcharges, not_created, other


def get_real_price_postnl(df):
    nc = df.copy()
    pr = df[PRICING_MASK]
    pr = pr.dropna(subset=['tracking_number'])
    
    pr['real_price'] = pr.groupby('tracking_number')['real_price'].transform(sum)
    pr = pr[pr['description_4'] == 'Collo'].drop_duplicates('tracking_number')
    pr = pr.drop(['description_4'], axis=1)
    df = df[df['description_4'] == 'Collo'].drop_duplicates('tracking_number')
    df = df.drop(['real_price'], axis=1)
    df = df.join(pr.set_index('tracking_number'), on='tracking_number', how='left')
    nc = nc[~nc['tracking_number'].isin(list(df['tracking_number']))]
    
    return df, nc


def get_weight_class_postnl(df):
    if df['weight'] <= 23000:
        return 'kg=0-23'
    else:
        return 'kg=23-31.5'


def get_code_postnl(df):
    if df['code'] == 'postnl:pakjegemak/agecheck=18':
        if df['weight'] <= 23000:
            return 'postnl:pakjegemak/agecheck=18'
        else:
            return 'postnl:pakjegemak/agecheck=18/max'
    elif df['code'] in ['postnl:standard/eu', 'postnl:standard/eu,non_conveyable']:
        if df['weight'] <= 23000:
            return 'postnl:standard/eu'
        else:
            return 'postnl:standard/eu,non_conveyable'
    elif df['code'] == 'postnl:standard/kg=0-31.5,return':
        return 'postnl:standard/kg=0-31.5,return'
    else:
        if df['weight'] <= 23000:
            return re.sub(r'kg=\d+\-\d+', df['weight_class_postnl'], df['code'])
        else:
            return re.sub(r'kg=\d+\-\d+\.\d+', df['weight_class_postnl'], df['code'])


def get_real_price_comparison_dataframe(df):
    df['weight_class_postnl'] = df.apply(get_weight_class_postnl, axis=1)
    df['code_postnl'] = df.apply(get_code_postnl, axis=1)
    df['filter'] = (df['code_postnl'] == df['code'])

    diff = df[(df['filter'] == False) | (df['shipping_method_id'].isin([84, 2095]))]
    df = df[(df['filter'] == True) & (~df['shipping_method_id'].isin([84, 2095]))]

    gr = pd.DataFrame(df.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_postnl', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_postnl']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_postnl'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_postnl'] = gr['shipments'] * gr['real_price_postnl']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('#shipments * dif')
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(4)   
    
    return gr, diff


def get_returns_postnl(df):
    df = df[(df['description_4'] == 'Collo') & (df['real_price'] > 0) & (df['direction'] == 'o')]
    df['description_2'].fillna('NL', inplace=True)
    gr = pd.DataFrame(df.groupby(['description_2', 'real_price']).agg({'real_price':['count', 'mean', sum]}))
    gr.columns = ['count', 'mean', 'sum']
    gr = gr.sort_values('sum', ascending=False)
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(2)
    
    return gr


def get_globalpack_real_price_overview_postnl(df, pricing):
    df = df[(df['description_1'] == 'Pakketten niet-EU') & (df['description_4'] == 'Kg')]
    df['description'] = 'PostNL GlobalPack'
    df['id'] = df['code'] + 'NL' + df['shipping_country']

    pa = pd.read_csv(pricing)
    pa = pa[pa['weight_allowance'] > 0]
    pa['id'] = pa['method_code'] + 'NL' + pa['to_country']
    pa = pa[['id', 'real_weight_allowance']]
    
    df = df.join(pa.set_index('id'), on='id', how='left')
    df = df.groupby(['shipping_country']).agg({'tarife':'mean', 'real_weight_allowance':'mean'})
    df.columns = ['real_price_postnl', 'real_price_panel']
    df['dif'] = df['real_price_panel'] - df['real_price_postnl']
    df = df.sort_values('dif').round(2)
    
    return df


def get_surcharges_to_be_invoiced(df):
    if df['description_4'] in  ('Handmatige verwerking', 'Toeslag pakketten >23kg') and df['shipping_method_id'] in (NMG):
        return 'surcharges already invoiced'
    else:
        return 'surcharges to be invoiced'    


def get_price_check_summary(gr, diff, nc, not_created, returns, surcharges, other, sc):
    gr = pd.DataFrame({'description': ['shipping method sendcloud == shipping method carrier'], 'count': [gr['shipments']['total']], 'value': [gr['#shipments * real_price_postnl']['total']]}).set_index('description')

    diff['description'] = 'shipping method sendcloud != shipping method carrier'
    diff = pd.DataFrame(diff.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    diff.columns = ['count', 'value']

    nc['description'] = 'no collo invoiced'
    nc = pd.DataFrame(nc.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    nc.columns = ['count', 'value']

    not_created['description'] = 'parcel not created at sendcloud'
    not_created = pd.DataFrame(not_created.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    not_created.columns = ['count', 'value']

    returns['description'] = ['returns {}'.format(i) for i in list(returns['direction'])]
    returns = pd.DataFrame(returns.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    returns.columns = ['count', 'value']

    surcharges['description'] = surcharges.apply(get_surcharges_to_be_invoiced, axis=1)
    surcharges = pd.DataFrame(surcharges.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    surcharges.columns = ['count', 'value']

    other = pd.DataFrame(other.groupby('description_4').agg({'tracking_number':len, 'real_price':sum}))
    other.columns = ['count', 'value']

    df = pd.concat([gr, diff, nc, not_created, returns, surcharges, other])
    df = df.sort_values('value', ascending=False)
    df = df.append(df.sum().rename('total')).round(2)
    df['distribution'] = df.iloc[:, -1] / sc.real_price.sum()

    return df


def get_invoice_distribution_carrier(ts, dis):
    pa = pd.DataFrame({'description': [], 'count': [], 'value': [], 'distribution': []})
    tracking = pd.DataFrame({'description': ['tracking status distribution'], 'count': [np.nan], 'value': [np.nan], 'distribution': [np.nan]})
    pa = pa.append(tracking)
    tra = pd.DataFrame({'description': ts.iloc[:, 0], 'count': ts.iloc[:, 1], 'value': ts.iloc[:, 2], 'distribution': ts.iloc[:, 3]})
    pa = pa.append(tra)
    tracking = pd.DataFrame({'description': ['tracking status distribution'], 'count': [np.nan], 'value': [np.nan], 'distribution': [np.nan]})
    pa = pa.append(tracking)
    tra = pd.DataFrame({'description': dis.iloc[:, 0], 'count': dis.iloc[:, 1], 'value': dis.iloc[:, 2], 'distribution': dis.iloc[:, 3]})
    pa = pa.append(tra).round(2)

    return pa


def get_invoice_parcel_check(i):
    ts = pd.read_excel(i, sheet_name='ts')
    ts.columns = ['description', 'count', 'value', 'distribution']
    ts = ts[ts['description'].isin(['delivered', 'collected_by_customer'])].distribution.sum().round(4)

    not_created = pd.read_excel(i, sheet_name='nc')

    dn = pd.read_excel(i, sheet_name='di').real_price.sum()

    if len(not_created) == 0:
        nc = len(not_created)
        nv = 0
    else:
        nc = len(not_created['COLLO'].unique())
        nv = not_created['NETTO_BEDRAG'].astype(float).sum()

    dp = pd.read_excel(i, sheet_name='dp')
    if len(dp) == 0:
        dc = len(dp)
        dv = 0
    else:
        dc = len(dp.tracking_number.unique())
        dv = dp.real_price.sum()

    np = pd.read_excel(i, sheet_name='np')
    np = np.iloc[-1,-1]

    na = pd.read_excel(i, sheet_name='nan')
    lst = []

    for i in range(len(na)):
        if na['nan_value'][i] != 0:
            lst.append(na['column'][i])

    if len(lst) == 0:
        na = 'no NaN values'
    else:
        na = lst

    return ts, nc, nv, dc, dv, np, na, dn


def get_invoice_price_check(i):
    dis = pd.read_excel(i, sheet_name='summary')
    df = pd.read_excel(i, sheet_name='price-check')
    di = df['#shipments * dif'][len(df) -1]
    ca = df['#shipments * real_price_postnl'][len(df) -1]
    dif = pd.read_excel(i, sheet_name='gp')
    dif = dif['dif'].abs().sum()

    return dis, di, ca, dif


def get_invoice_summary(di, ca, dif, nc, nv, dc, dv, np, na, sc, ts, dn):
    """
    Returns a summary of the checked invoice.
    """
    df = pd.DataFrame({'checked amount of carrier invoice': [ca],
                       'deviation in real_price between carrier and sendcloud: ': [di],
                       'absolute sum of difference globalpack: ': [dif],
                       'percentage of parcels that are delivered: ': [''],
                       '# parcels not created via sendcloud: ': [nc],
                       'invoice amount parcels not created via sendcloud: ': [nv],
                       '# direct parcels invoiced by carrier: ': [dc],
                       'invoice amount direct parcels invoiced by carrier: ': [dv],
                       'new descriptions added to the invoice: ': np, 
                       '# columns with NaN values: ': [na], 
                       'amount double invoiced: ': [dn],
                       'total invoice value: ': [sc.real_price.sum()]}).transpose()

    df.columns = ['metrics']
    df['relative'] = ''
    df['relative']['checked amount of carrier invoice'] = (df['metrics']['checked amount of carrier invoice'] / sc.real_price.sum()).round(4)
    df['relative']['deviation in real_price between carrier and sendcloud: '] = (df['metrics']['deviation in real_price between carrier and sendcloud: '] / ca).round(4)
    df['relative']['invoice amount parcels not created via sendcloud: '] = (df['metrics']['invoice amount parcels not created via sendcloud: '] / sc.real_price.sum()).round(4)
    df['relative']['percentage of parcels that are delivered: '] = ts

    return df      


def get_postnl_surcharge_price(i):
    return PRICING_DICT.get(i)


def get_overschrijding_maximale_dimensies_description_postnl(df):
    if df['weight'] > 31500:
        return 'Toeslag overschrijding maximale dimensies - gewogen gewicht: ' + str(df['weight']) + ' (gr)'
    else:
        return 'Toeslag overschrijding maximale dimensies - afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) + ' (mm)'


def get_overschrijding_maximale_dimensies_toeslag_postnl(df):
    df = df[df['description_4'] == 'Overschrijding maximum pakket dimensies'].copy()
    df['price'] = df['description_4'].apply(get_postnl_surcharge_price)
    df['description_4'] = df.apply(get_overschrijding_maximale_dimensies_description_postnl, axis=1)
    df = df[(df['length'] > 1750) | (df['width'] > 780) | (df['height'] > 580) | (df['weight'] > 31500)]
    print('# overschrijding maximale dimensies: ', len(df))

    return df


def get_niet_machine_geschikt_description_postnl(df):
    if df['length'] <= 1750 and (df['length'] > 1000 or df['length'] < 100):
        return 'Toeslag niet machine geschikt - afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) +' (mm)'
    elif df['width'] <= 780 and (df['width'] > 700 or df['width'] < 100):
        return 'Toeslag niet machine geschikt - afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) +' (mm)'
    elif df['height'] < 10:
        return 'Toeslag niet machine geschikt - afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) +' (mm)'        
    else:
        return 'Toeslag wegens aard van verpakking'        


def get_handmatige_verwerking_toeslag_postnl(df):
    df = df[df['description_4'] == 'Handmatige verwerking'].copy()
    pa = df[df['shipping_method_id'].isin(NMG)]
    print('# NMG created: ', len(pa))
    df = df[~df['tracking_number'].isin(list(pa['tracking_number']))]
    df['price'] = df['description_4'].apply(get_postnl_surcharge_price)
    df['description_4'] = df.apply(get_niet_machine_geschikt_description_postnl, axis=1)

    hm = df.copy().sort_values('tracking_number')
    hm['count'] = hm.groupby('tracking_number')['tracking_number'].transform(len)
    hm = hm[hm['count'] > 1].drop_duplicates('tracking_number')

    df = df.drop_duplicates('tracking_number')
    print('# handmatige verwerking: ', len(df))
    print('# handmatige verwerking double in csv: ', len(hm))
    
    return df


def get_toeslag_pakketten_23kg(df):
    df = df[df['description_4'] == 'Toeslag pakketten >23kg'].copy()
    pa = df[df['shipping_method_id'].isin(NMG)]
    print('# NMG created: ', len(pa))
    df = df[~df['shipping_method_id'].isin(NMG)]
    df['price'] = df['description_4'].apply(get_postnl_surcharge_price)
    df['description_4'] = 'Toeslag pakket zwaarder dan 23kg - gewogen gewicht: ' + df['weight'].astype(str) + ' (gr)'
    print('# toeslag pakket > 23kg: ', len(df))
    
    return df


def get_retouren_postnl(df):
    re = df[(df['description_5'] == 'Retouren') & (df['real_price'] > 0) & (~df['price_panel'].isna()) & (df['direction'] == 'o')].copy()
    re['price'] = re['price_panel']
    re['description_4'] = 'Pakket retour afzender'
    print('# returns: ', len(re))
    
    return re


def get_piek_toeslag_postnl(df):
    df = df[df['description_4'] == 'Piek toeslag'].copy()
    df['description_4'] = 'PostNL piektoeslag'
    df['price'] = df['real_price']
    print('# piektoeslag: ', len(df))
    
    return df


def get_liter_toeslagen_postnl(df):
    df = df[df['description_4'].isin(['Formaat pakket groter dan 50 dm³', 'Formaat pakket groter dan 200 dm³', 'Formaat pakket groter dan 100 dm³'])].copy()
    df['price'] = df['real_price']
    print('# volume toeslagen postnl: ', len(df))

    return df    


def get_surcharges_postnl(df):
    df = df.dropna(subset=['length']).copy()
    df['length'] = [int(i) for i in list(df['length'])]
    df['height'] = [int(i) for i in list(df['height'])]
    df['width'] = [int(i) for i in list(df['width'])]
    df['weight'] = [int(i) for i in list(df['weight'])]
    ma = get_overschrijding_maximale_dimensies_toeslag_postnl(df)
    ha = get_handmatige_verwerking_toeslag_postnl(df)
    tp = get_toeslag_pakketten_23kg(df)
    re = get_retouren_postnl(df)
    # pi = get_piek_toeslag_postnl(df)
    li = get_liter_toeslagen_postnl(df)
    df = pd.concat([ma, ha, tp, re, li])
    df = df.dropna(subset=['description_panel']).reset_index().drop(['index'], axis=1)
    df['carrier'] = 'postnl'
    df['type'] = 'surcharge'
    df['currency'] = 'EUR'
    df['from_country'] = 'NL'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df = df[MASK]
    df.columns = ['tracking_number', 
                'description', 
                'price', 
                'real_price', 
                'type', 
                'carrier', 
                'from_country',
                'currency', 
                'internal_note']

    return df


def get_volumemetrisch_surcharges(df):
    """
    Returns a DataFrame of the weight allowances surcharges postnl.
    """
    df = df[df['description_4'] == 'Kg Volumetrisch'].copy()
    df['count_1'] = df['count'].apply(np.ceil)
    df['code'] = df['code'] + 'NL' + df['shipping_country']    

    pa = pd.read_csv(PRICING)
    pa = pa[pa['weight_allowance'] > 0]
    pa['method_code'] = pa['method_code'] + 'NL' + pa['to_country']
    
    pr = pa[['method_code', 'weight_allowance']]
    rp = pa[['method_code', 'real_weight_allowance']]
    
    df = df.join(pr.set_index('method_code'), on='code', how='left')
    df = df.join(rp.set_index('method_code'), on='code', how='left')
    
    df['price'] = df['count_1'] * df['weight_allowance']
    df = df[df['price'] > 0]
    df['description_4'] = 'Surcharge - selected weight: ' + df['weight_panel'].astype(str) + ' (kg)' + ' measured volume metric weight: ' + df['count'].astype(str) + ' (kg)'    
    df['carrier'] = 'postnl'
    df['type'] = 'surcharge'
    df['currency'] = 'EUR'
    df['from_country'] = 'NL'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df = df[MASK]
    df.columns = ['tracking_number', 
                  'description', 
                  'price', 
                  'real_price', 
                  'type', 
                  'carrier', 
                  'from_country',
                  'currency', 
                  'internal_note']

    return df   


def get_globalpack_surcharges_postnl(df, vm):
    """
    Returns a DataFrame of the weight allowances surcharges postnl.
    """
    df = df[(df['description_1'] == 'Pakketten niet-EU') & (df['weight'] > 0) & (df['description_4'] == 'Collo')].copy( )
    df['weight'] = df['weight'] / 1000
    df['weight_class'] = df['weight'].apply(np.ceil)
    df['weight_class_panel'] = df['weight_panel'].apply(np.ceil)
    df['filter'] = df['weight_class'] - df['weight_class_panel']
    df = df[df['filter'] > 0]
    df['code'] = df['code'] + 'NL' + df['shipping_country']    

    pa = pd.read_csv(PRICING)
    pa = pa[pa['weight_allowance'] > 0]
    pa['method_code'] = pa['method_code'] + 'NL' + pa['to_country']
    
    pr = pa[['method_code', 'weight_allowance']]
    rp = pa[['method_code', 'real_weight_allowance']]
    
    df = df.join(pr.set_index('method_code'), on='code', how='left')
    df = df.join(rp.set_index('method_code'), on='code', how='left')
    
    df['price'] = df['filter'] * df['weight_allowance']
    df['real_price'] = df['filter'] * df['real_weight_allowance']
    df = df[df['price'] > 0]
    df['description_4'] = 'Surcharge - selected weight: ' + df['weight_panel'].astype(str) + ' (kg)' + ' measured weight: ' + df['weight'].astype(str) + ' (kg)'    
    df['carrier'] = 'postnl'
    df['type'] = 'surcharge'
    df['currency'] = 'EUR'
    df['from_country'] = 'NL'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df = df[MASK]
    df.columns = ['tracking_number', 
                  'description', 
                  'price', 
                  'real_price', 
                  'type', 
                  'carrier', 
                  'from_country',
                  'currency', 
                  'internal_note']

    df = df[~df['tracking_number'].isin(list(vm['tracking_number']))]
    df = pd.concat([vm, df])

    return df 


def get_mailbox_credit_details(df, start_date, end_date):
    """
    Returns a DataFrame of parcels which are created via the panel of Sendcloud.
    """
    tn = df[['tracking_number']]
    tn = tn.dropna()
    tn = tn.drop_duplicates('tracking_number')
    tn = list(tn['tracking_number'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(get_user_details_mailbox_credit.format(tu, start_date), con=ENGINE)
    df = df.join(pa.set_index('tracking_number'), on='tracking_number', how='left')
    df = df[(df['date'] >= start_date) & (df['date'] < end_date)]
    
    return df    


def get_average_parcels_shipped_based_on_months_shipped(df):
    """
    Function that returns the average parcels shipped based on the amount of months shipped.
    """
    if df['active-months-count'] == 12:
        return df['parcels-shipped-YTD'] * (12/12)
    elif df['active-months-count'] == 11:
        return df['parcels-shipped-YTD'] * (12/11)
    elif df['active-months-count'] == 10:
        return df['parcels-shipped-YTD'] * (12/10)
    elif df['active-months-count'] == 9:
        return df['parcels-shipped-YTD'] * (12/9)
    elif df['active-months-count'] == 8:
        return df['parcels-shipped-YTD'] * (12/8)    
    elif df['active-months-count'] == 7:
        return df['parcels-shipped-YTD'] * (12/7)
    elif df['active-months-count'] == 6:
        return df['parcels-shipped-YTD'] * (12/6)
    elif df['active-months-count'] == 5:
        return df['parcels-shipped-YTD'] * (12/5)
    elif df['active-months-count'] == 4:
        return df['parcels-shipped-YTD'] * (12/4)
    elif df['active-months-count'] == 3:
        return df['parcels-shipped-YTD'] * (12/3)
    elif df['active-months-count'] == 2:
        return df['parcels-shipped-YTD'] * (12/2)
    elif df['active-months-count'] == 1:
        return df['parcels-shipped-YTD'] * (12/1)
    else:
        return 0


def get_postnl_mailbox_price(i):
    """
    Function that returns the description based on the average parcels shipped.
    """
    if i >10000:
        return 3.299
    elif i > 5000:
        return 3.299
    elif i > 2500:
        return 3.527
    elif i > 1000:
        return 3.776
    elif i > 500:
        return 3.847
    else:
        return 4.1


def get_pivot_table_mailbox_credit(df):
    """
    Returns a pivot-table of the parcels shipped per month.
    """
    df['date'] = df['date'].apply(general_carrier.get_first_day_of_month)
    pi = df.pivot_table(index='user_id', 
                        columns='date', 
                        values='tracking_number', 
                        aggfunc='count')   
    
    pi.columns = [str(i)[:10] for i in pi.columns]
    pi['active-months-count'] = pi.count(axis=1)
    pi['parcels-shipped-YTD'] = pi.iloc[:, :-1].sum(axis=1)
    pi['forecasted-yearly-volume'] = pi.apply(get_average_parcels_shipped_based_on_months_shipped, axis=1)
    pi['currently-paid-per-mailbox'] = 4.1
    pi['corrected_price'] = pi['forecasted-yearly-volume'].apply(get_postnl_mailbox_price)
    pi['credit'] = (pi['corrected_price'] - pi['currently-paid-per-mailbox']) * pi[pi.columns[-6]]
    pi['credit'] = pi['credit'].fillna(0)
    pi = pi.sort_values('parcels-shipped-YTD', ascending=False)
    
    return pi               


def get_pivot_table_credit_mailbox_overview(df, pi):
    df = df[['user_id', 'carrier_name', 'friendly_name', 'company_name', 'postal_code', 'date']]
    df['friendly_name'] = 'PostNL Mailbox Parcel Extra 0-2kg'
    df = df.sort_values('date', ascending=False)
    df = df.drop_duplicates('user_id')
    df = df.drop(['date'], axis=1)
    pi = pi.reset_index()
    pi['user_id'] = pi['user_id'].astype(str)
    pi = pi.join(df.set_index('user_id'), on='user_id', how='left')
    pi = pi.drop(['user_id'], axis=1)

    return pi         


def get_items_double_invoiced(df, column):
    lst = []
    x = 0

    for i in range(len(df)):
        if i == 0:
            lst.append(x)
        elif (df['tracking_number'][i] == df['tracking_number'][i - 1]):
            if (df[column][i] == df[column][i - 1]):
                x = x + 1
                lst.append(x)
                lst[i-1] = x
            else:
                lst.append(x)
        else:
            x = 0
            lst.append(x)

    return lst


def get_double_items(df, column):
    lst = []

    # r1 = df[df['description_5'] == 'Retouren']
    # r2 = df[df['description_1'] == 'Pakketten Europa retouren (ERS)']

    # re = pd.concat([r1, r2])

    # df = df[~df['tracking_number'].isin(list(re['tracking_number']))].copy()

    for i in df[column].unique():
        dff = df[df[column] == i].copy()
        dff['count_item_multiple_times_invoiced'] = dff.groupby('tracking_number')['tracking_number'].transform(len)
        dff = dff[(dff['count_item_multiple_times_invoiced'] > 1) & (dff['real_price'] > 0)]
        lst.append(dff)

    dd = pd.concat(lst)
    dd = dd.sort_values('tracking_number')

    return dd    