# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import re
from sqlalchemy import create_engine
import glob


# variabels
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
PRICING = 'C:\\Users\\Rick Vloet\\Documents\\carrier-pricing\\pricing_dpd.csv'


DESCRIPTIONS = ['Service Description',
                'Fuel Surcharge and Exchange Rate',
                'Third Party Collection', 
                'Fourth Party Collection',
                'Congestion Charge', 
                'EU Clearance Charge',
                'Return to Consignor Charge', 
                'Failed Collection Charge',
                'Scottish Delivery Zone', 
                'Duties & Taxes Prepaid Admin Charge',
                'Handling Charge', 
                'Contractual Liability', 
                'Oversized Exports Charge',
                'Unsuccessful EU Export Charge', 
                'EU Export Return Charge', 
                'Cover']


TYPES = ['CLASIC', 
         'Fuel Surcharge and Exchange Rate',
         'EU Clearance Charge', 
         'NXTDAY', 
         'Congestion Charge', 
         'SAT',
         'Third Party Collection', 
         'Fourth Party Collection',
         'Return to Consignor Charge', 
         'Failed Collection Charge',
         'Scottish Delivery Zone', 
         'Duties & Taxes Prepaid Admin Charge',
         'Handling Charge', 
         'Contractual Liability',
         'Oversized Exports Charge', 
         'Unsuccessful EU Export Charge',
         'EU Export Return Charge', 
         'Cover', 
         'SAT 12', 
         '2DAY', 
         'SUN 12',
         'DPD 1030', 
         'DPD 12']


SHIPMENTS = ['CLASIC', 
             'NXTDAY', 
             'SAT',
             'SAT 12', 
             '2DAY', 
             'SUN 12', 'DPD 1030', 'DPD 12'] 


PRICES = ['CLASIC', 
          'NXTDAY', 
          'SAT',
          'SAT 12', 
          'Fuel Surcharge and Exchange Rate',
          '2DAY', 
          'SUN 12', 'DPD 1030', 'DPD 12']         


MASK = ['tracking_number', 
        'description', 
        'price',
        'real_price', 
        'from_country', 
        'carrier', 
        'type',
        'currency', 
        'internal_note']        


# queries



# functions
def get_invoice_data(path, invoice):
    df = pd.read_excel(path + invoice, 
                       encoding='latin-1', 
                       converters={'Parcel No': lambda i: str(i)})

    df['invoice_number'] = invoice[:-5]

    return df


def get_select_columns(df):
    lst = []

    for i in df.index:
        x = df[df['Parcel No'] == df['Parcel No'][i]]
        for c in DESCRIPTIONS:
            if c == 'Service Description':
                y = x[['Parcel No', 'Items', 'Weight', 'Date', c, 'Revenue', 'invoice_number']].copy()
                y.columns = ['tracking_number', 'count', 'weight', 'date', 'description', 'real_price', 'invoice_number']
                lst.append(y)
            else:
                y = x[['Parcel No', 'Items', 'Weight', 'Date', 'invoice_number']].copy()
                y.columns = ['tracking_number', 'count', 'weight', 'date', 'invoice_number']
                y['description'] = c
                y['real_price'] = df[c][i]
                lst.append(y)

    df = pd.concat(lst)
    df = df[(df['real_price'] != 0) & (~df['real_price'].isna())].reset_index().drop(['index'], axis=1)
    df = df[['tracking_number', 'description', 'real_price', 'count', 'weight', 'date', 'invoice_number']]

    return df


def get_new_products_of_carrier(df):
    lst = []

    for i in list(df['description'].unique()):
        if i not in TYPES:
            lst.append(i)

    if len(lst) > 0:
        print('the carrier added these products: ', lst)
        df = pd.DataFrame({'new products':[lst]})
    else:
        print('no products added!')
        df = pd.DataFrame({'new products':[['no products added!']]})
    
    return df    


def get_parcels_not_created_at_sendcloud(df, pa): 
    pa = pa[pa['description_panel'].isna()]
    df = df[df['Parcel No'].isin(list(pa['tracking_number']))]

    return df


def get_divide_types(df):  
    df = df[df['real_price'] != 0]
    not_created = df[df['description_panel'].isna()]
    df = df[~df['description_panel'].isna()]
    shipments = df[df['description'].isin(PRICES)].copy()
    surcharges = df[~df['description'].isin(PRICES)]

    return shipments, surcharges, not_created


def get_real_price(df):
    df['real_price'] = df.groupby('tracking_number')['real_price'].transform(sum)
    df = df[df['description'].isin(SHIPMENTS)].copy()
    
    return df    


def get_real_price_comparison_dataframe(df):
    df['code_dpd_uk'] = df['code']
    df['filter'] = (df['code_dpd_uk'] == df['code'])

    gr = pd.DataFrame(df.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_dpd_uk', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_dpd_uk']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_dpd_uk'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_dpd_uk'] = gr['shipments'] * gr['real_price_dpd_uk']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('#shipments * dif')
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(4)   
    
    return gr


def get_price_check_summary(gr, not_created, surcharges, sc):
    gr = pd.DataFrame({'description': ['shipping method sendcloud == shipping method carrier'], 'count': [gr['shipments']['total']], 'value': [gr['#shipments * real_price_dpd_uk']['total']]}).set_index('description')

    not_created['description'] = 'parcel not created at sendcloud'
    not_created = pd.DataFrame(not_created.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    not_created.columns = ['count', 'value']

    # returns['description'] = ['returns {}'.format(i) for i in list(returns['direction'])]
    # returns = pd.DataFrame(returns.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    # returns.columns = ['count', 'value']

    surcharges = pd.DataFrame(surcharges.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    surcharges.columns = ['count', 'value']

    df = pd.concat([gr, not_created, surcharges])
    df = df.sort_values('value', ascending=False)
    df = df.append(df.sum().rename('total')).round(2)
    df['distribution'] = df.iloc[:, -1] / sc.real_price.sum()

    return df


def get_invoice_parcel_check(i):
    ts = pd.read_excel(i, sheet_name='ts')
    ts.columns = ['description', 'count', 'value', 'distribution']
    ts = ts[ts['description'].isin(['delivered', 'collected_by_customer'])].distribution.sum().round(4)

    not_created = pd.read_excel(i, sheet_name='nc')

    dp = pd.read_excel(i, sheet_name='dp')
    if len(dp) == 0:
        dc = len(dp)
        dv = 0
    else:
        dc = len(dp.tracking_number.unique())
        dv = dp.real_price.sum()

    np = pd.read_excel(i, sheet_name='np')
    np = np.iloc[-1,-1]

    na = pd.read_excel(i, sheet_name='nan')
    lst = []

    for i in range(len(na)):
        if na['nan_value'][i] != 0:
            lst.append(na['column'][i])

    if len(lst) == 0:
        na = 'no NaN values'
    else:
        na = lst

    return ts, dc, dv, np, na


def get_invoice_price_check(i):
    dis = pd.read_excel(i, sheet_name='summary')
    df = pd.read_excel(i, sheet_name='price-check')
    di = df['#shipments * dif'][len(df) -1]
    ca = df['#shipments * real_price_dpd_uk'][len(df) -1]

    not_created = pd.read_excel(i, sheet_name='nc')
    if len(not_created) == 0:
        nc = len(not_created)
        nv = 0
    else:
        nc = len(not_created['tracking_number'].unique())
        nv = not_created['real_price'].astype(float).sum() 

    return dis, di, ca, nc, nv    


def get_invoice_summary(di, ca, nc, nv, dc, dv, np, na, sc, ts):
    """
    Returns a summary of the checked invoice.
    """
    df = pd.DataFrame({'checked amount of carrier invoice': [ca],
                       'deviation in real_price between carrier and sendcloud: ': [di],
                       'percentage of parcels that are delivered: ': [''],
                       '# parcels not created via sendcloud: ': [nc],
                       'invoice amount parcels not created via sendcloud: ': [nv],
                       '# direct parcels invoiced by carrier: ': [dc],
                       'invoice amount direct parcels invoiced by carrier: ': [dv],
                       'new descriptions added to the invoice: ': np, 
                       '# columns with NaN values: ': [na], 
                       'total invoice value: ': [sc.real_price.sum()]}).transpose()

    df.columns = ['metrics']
    df['relative'] = ''
    df['relative']['checked amount of carrier invoice'] = (df['metrics']['checked amount of carrier invoice'] / sc.real_price.sum()).round(4)
    df['relative']['deviation in real_price between carrier and sendcloud: '] = (df['metrics']['deviation in real_price between carrier and sendcloud: '] / ca).round(4)
    df['relative']['invoice amount parcels not created via sendcloud: '] = (df['metrics']['invoice amount parcels not created via sendcloud: '] / sc.real_price.sum()).round(4)
    df['relative']['percentage of parcels that are delivered: '] = ts

    return df


def get_surcharges(df):
    df = df.dropna(subset=['description_panel'])
    df['price'] = df['real_price']
    df['carrier'] = 'dpd_gb'
    df['type'] = 'surcharge'
    df['currency'] = 'GBP'
    df['from_country'] = 'GB'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df = df[MASK]

    return df    