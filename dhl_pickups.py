# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import re
import datetime as dt 
from sqlalchemy import create_engine


# notes
notes = """
Xapat moet op user id 2543 gefactureerd worden (dat is puikman).  WoontrendsXL onder 1917.
SCOOTPLAZA = 1655
NIEHOFF HIGH TECH SOUND = 37814
JNH parts is van pakket service online
INBALANCE-PETFOOD = 39631. Maar die doet genoeg zendingen dus NIET factureren
GREENEGGTOTAAL = 5405
De andere kan ik ook niet vinden.


and add invoice_number to internal_note!

@Rinus Curiel Hi Rinus, ja zou mega chill zijn. Kleine samenvatting van hoeveel pakketten te weinig en over welke periode zou al fijn zijn. Zou dat werkbaar zijn voor jullie?


"""

# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})


MASK = ['tracking_number', 
        'description',
        'price', 
        'real_price',
        'type',
        'from_country',
        'carrier', 
        'currency',
        'internal_note']


NAMES = ['client_id',
         'debtor_nr', 
         'debtor_name', 
         'company_name', 
         'pickups_count',
         'pickups_max', 
         'pickups_to_be_invoiced',
         'shipments_count', 
         'colli_count',
         'pallet_count']

MIN_AMOUNT_SHIPMENTS = 4
PICKUP_FEE_DHL = 4.5


MASK = ['tracking_number', 
        'description', 
        'price', 
        'real_price', 
        'type', 
        'carrier', 
        'from_country',
        'currency', 
        'internal_note',
        'user_id']

# queries
dhlnl_pickup_surcharge_query = """
select 
	cc.client_id,
	cast(p.user_id as varchar),
	p.tracking_number, 
	sm.friendly_name
from
	parcel p
left join contracts_contract cc 
	on p.user_id = cc.user_id
left join parcel_status ps 
	on p.parcel_status_id = ps.parcel_status_id
left join shipping_method sm 
	on p.shipping_method_id = sm.shipping_method_id
where 
	cc.client_id in {}
and 
	p.announced_at between '{}' and '{}'
and
	p.cancellation_status = 0
and 
	p.carrier_code = 'dhl'
and 
	ps.message ilike '%%deliver%%'
"""

# functions
def get_invoice_data(i, name_sheet):
    df = pd.read_excel(i, 
                       name_sheet, 
                       converters={'VERZENDER_NR': lambda i: '0' + str(i)})

    return df    


def get_select_columns(df):
    df = df.iloc[:, :10]
    df.columns = NAMES
    df = df[~df['debtor_name'].isna()]
    df = df[df['pickups_to_be_invoiced'] > 0]

    return df


def get_dhlnl_parcels_shipped(df, start_date, end_date):
    tn = list(df['client_id'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(dhlnl_pickup_surcharge_query.format(tu, start_date, end_date), con=ENGINE)
    pa['user_id'] = [i.replace('4954', '2543') for i in list(pa['user_id'])]
    pa['pallet_count_sendcloud']= [1 if i == 'DHL Europlus Pallet' else 0 for i in list(pa['friendly_name'])]
    pa['parcel_count_sendcloud']= [1 if i != 'DHL Europlus Pallet' else 0 for i in list(pa['friendly_name'])]
    pa = pd.DataFrame(pa.groupby(['client_id', 'user_id']).agg({'pallet_count_sendcloud':sum, 'parcel_count_sendcloud':sum})).reset_index()
    df = df.join(pa.set_index('client_id'), on='client_id', how='left')

    return df


def get_dhlnl_pickup_surcharge(df, month, invoice):
    df['average_collo_per_pickup'] = (df['parcel_count_sendcloud'] / df['pickups_count']).round(2)
    df['pickups'] = np.ceil((((MIN_AMOUNT_SHIPMENTS - df['average_collo_per_pickup']) * df['pickups_count']) / MIN_AMOUNT_SHIPMENTS).round(2))
    
    df['pickups'] = df['pickups'].fillna(0)
    df['pickups'] = df['pickups'] - df['pallet_count_sendcloud']
    df['pickups'] = df['pickups'].fillna(0)
    df['pickups'] = df['pickups'].astype(int)
    df['tracking_number'] = ''
    df['parcels_to_ship'] = (df['pickups_count'] * MIN_AMOUNT_SHIPMENTS) - df['parcel_count_sendcloud']
    df['parcels_to_ship'] = df['parcels_to_ship'].fillna(0)
    df['parcels_to_ship'] = [int(i) for i in list(df['parcels_to_ship'])]
    df['description'] = df['pickups'].astype(str) + 'x pickup toeslag DHL NL voor {} '.format(month) + df['parcels_to_ship'].astype(str) + ' zendingen te weinig verstuurd'
    df['price'] = df['pickups'] * PICKUP_FEE_DHL
    df['real_price'] = df['price']
    df['type'] = 'surcharge'
    df['from_country'] = 'NL'
    df['carrier'] = 'dhl'
    df['currency'] = 'EUR'
    df['internal_note'] = 'invoice_number: {} surcharge {}'.format(invoice, month)
    df = df[df['pickups'] > 0]
    df = df[(df['price'] > 0) & (df['pickups'] > 0)]
    # df = df[MASK]
    
    return df    