# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import datetime as dt 
import numpy as np
from dateutil.relativedelta import relativedelta


# variables



# functions
def get_invoice_data(i):
    """
    Returns the invoice into a DataFrame.
    """
    df = pd.read_csv(i, 
                     header=None,
                     encoding='latin-1', 
                     low_memory=False, 
                     converters={'Invoice Number': lambda i: str(i)})

    return df
