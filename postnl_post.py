# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import re
from sqlalchemy import create_engine


# functions
def get_price_check_summary(gr, diff, nc, not_created):
    df = pd.DataFrame({'gr': [gr['#shipments * real_price_postnl']['total']], 
                    'diff': [diff.real_price.sum()],
                    'nc': [nc.real_price.sum()],
                    'not_created': [not_created.real_price.sum()]}).transpose()
    df.columns = ['totals']
    df = df.append(df.sum().rename('total')).round(2)
    df['distribution'] = df.iloc[:, 0] / df.iloc[-1, -1]

    return df


def get_code_postnl(df):
    if df['shipping_method_id'] == 504:
        return 'postnl:brievenbus/kg=0-2,segment=C'
    elif df['shipping_method_id'] == 507:
        return 'postnl:brievenbus/kg=0-2,segment=B'
    elif df['shipping_method_id'] == 39:
        return 'postnl:brievenbus/kg=0-2'
    elif df['shipping_method_id'] == 503:
        return 'postnl:brievenbus/kg=0-2,segment=A'
    elif df['shipping_method_id'] == 505:
        return 'postnl:brievenbus/kg=0-2,segment=A+'
    else:
        return 'XXX'



def get_real_price_comparison_dataframe(df):
    df['code_postnl'] = df.apply(get_code_postnl, axis=1)
    df['filter'] = (df['code_postnl'] == df['code'])

    diff = df[df['filter'] == False]
    df = df[df['filter'] == True]

    gr = pd.DataFrame(df.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_postnl', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_postnl']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_postnl'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_postnl'] = gr['shipments'] * gr['real_price_postnl']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('#shipments * dif')
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(4)   
    
    return gr, diff


def get_pricing_overview():
    df = pd.DataFrame({'<500':[410.0], '500 - 1000': [384.7], '1000 - 2500': [377.6], '2500 - 5000': [352.7], '5000 - 10000': [329.9]}).transpose().reset_index()
    df.columns = ['Stuks per jaar', 'prijs']

    return df


def get_price_check_summary(gr, diff, nc, not_created, returns, other, sc):
    gr = pd.DataFrame({'description': ['shipping method sendcloud == shipping method carrier'], 'count': [gr['shipments']['total']], 'value': [gr['#shipments * real_price_postnl']['total']]}).set_index('description')

    diff['description'] = 'shipping method sendcloud != shipping method carrier'
    diff = pd.DataFrame(diff.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    diff.columns = ['count', 'value']

    nc['description'] = 'no collo invoiced'
    nc = pd.DataFrame(nc.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    nc.columns = ['count', 'value']

    not_created['description'] = 'parcel not created at sendcloud'
    not_created = pd.DataFrame(not_created.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    not_created.columns = ['count', 'value']

    returns['description'] = ['returns {}'.format(i) for i in list(returns['direction'])]
    returns = pd.DataFrame(returns.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    returns.columns = ['count', 'value']

    other = pd.DataFrame(other.groupby('description_4').agg({'tracking_number':len, 'real_price':sum}))
    other.columns = ['count', 'value']

    df = pd.concat([gr, diff, nc, not_created, returns, other])
    df = df.sort_values('value', ascending=False)
    df = df.append(df.sum().rename('total')).round(2)
    df['distribution'] = df.iloc[:, -1] / sc.real_price.sum()

    return df  


def get_invoice_price_check(i):
    dis = pd.read_excel(i, sheet_name='summary')
    df = pd.read_excel(i, sheet_name='price-check')
    di = df['#shipments * dif'][len(df) -1]
    ca = df['#shipments * real_price_postnl'][len(df) -1]
    dif = df['dif'].abs().sum()

    return dis, di, ca    


def get_invoice_summary(di, ca, nc, nv, dc, dv, np, na, sc, ts, dn):
    """
    Returns a summary of the checked invoice.
    """
    df = pd.DataFrame({'checked amount of carrier invoice': [ca],
                       'deviation in real_price between carrier and sendcloud: ': [di],
                       'percentage of parcels that are delivered: ': [''],
                       '# parcels not created via sendcloud: ': [nc],
                       'invoice amount parcels not created via sendcloud: ': [nv],
                       '# direct parcels invoiced by carrier: ': [dc],
                       'invoice amount direct parcels invoiced by carrier: ': [dv],
                       'new descriptions added to the invoice: ': np, 
                       '# columns with NaN values: ': [na], 
                       'amount double invoiced: ': [dn],
                       'total invoice value: ': [sc.real_price.sum()]}).transpose()                       

    df.columns = ['metrics']
    df['relative'] = ''
    df['relative']['checked amount of carrier invoice'] = (df['metrics']['checked amount of carrier invoice'] / sc.real_price.sum()).round(4)
    df['relative']['deviation in real_price between carrier and sendcloud: '] = (df['metrics']['deviation in real_price between carrier and sendcloud: '] / ca).round(4)
    df['relative']['invoice amount parcels not created via sendcloud: '] = (df['metrics']['invoice amount parcels not created via sendcloud: '] / sc.real_price.sum()).round(4)
    df['relative']['percentage of parcels that are delivered: '] = ts

    return df     