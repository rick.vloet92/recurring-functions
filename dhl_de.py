# libraries

import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd
import datetime as dt
from sqlalchemy import create_engine
import numpy as np


# variables

CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
DATE_RANGE = str((dt.date.today() - dt.timedelta(290)))


# queries

panel_data_query = """
select
    p.tracking_number, 
    p.weight "weight_panel", 
    p.shipping_method_id, 
    (case when sm."returns" then sc.iso_2
    	  else rc.iso_2 end) "from_country",
    (case when sm."returns" then rc.iso_2
    	  else sc.iso_2 end) "shipping_country", 
    sm.code,
    p.collo_count,
    p.carrier_code,
    p.direction,
    cc."type" "contract_type",
    p.parent_parcel_status_id,
    p.user_id,
    ui.company_name
from 
    parcel p
left join shipping_country sc
	on p.shipping_country_id = sc.shipping_country_id
left join shipping_country rc 
	on p.from_country_id = rc.shipping_country_id
left join shipping_method sm 
	on p.shipping_method_id = sm.shipping_method_id
left join contracts_contract cc 
    on cc.id = p.contract_id
left join users_invoiceaddress ui 
    on p.user_id = ui.user_id
where   
    p.announced_at >= '{}'    
and
    p.tracking_number in {}   
"""


# functions

def get_invoice_data(path):
    df = pd.read_excel(path, 
                       sheet_name='Sheet1', 
                       converters={'Sendungscode': lambda i: str(i), 
                                   'Fakturierungsbeleg': lambda i: str(i)})

    return df


def get_select_columns(df):
    df = df.iloc[:, [5, 3, 4, 1]].copy()
    df.columns = ['tracking_number', 'description', 'date', 'invoice_number']
    df['date'] = pd.to_datetime(df['date'])

    return df


def get_invoice_check_summary(df, np, nc, br, ts):
    df = pd.DataFrame({'Number of unmatched lines: ': [len(nc)],
                       'Number of broker lines invoiced: ': [len(br)],
                       'Parcels delivered: ': [ts[ts['status'].isin(['delivered', 'collected_by_customer'])]['distribution'].sum()],

                       'Number of invoices: ': [len(df.invoice_number.unique())], 
                       'Number of unique tracking numbers invoiced: ': [len(df.tracking_number.unique())],
                       'Number of companies: ': [len(df[~df.company_name.isna()].company_name.unique())],
                       'Min invoice date: ': [min(df['date'])],
                       'Max invoice date: ': [max(df['date'])],                       
                       'Shipping country: ': [df[~df['shipping_country'].isna()].shipping_country.unique()]}).transpose()

    df.columns = ['metric']

    return df 