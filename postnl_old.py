# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import re
from sqlalchemy import create_engine


# variabels
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
PRICING = 'C:\\Users\\Rick Vloet\\Documents\\pricing_sencloud_carriers\\pricing_postnl.csv'


COLUMNS = ['INVOICE_USAGE_AGGREGATION_VALUE_2', 'INVOICE_USAGE_PRICING_COMP_DESCR', 'INVOICE_USAGE_NET_VALUE', 'INVOICE_NUMBER', 
           'INVOICE_ITEM_DESCRIPTION_1', 'INVOICE_USAGE_COLLO_HEIGHT', 'INVOICE_USAGE_COLLO_LENGTH', 'INVOICE_USAGE_COLLO_WIDTH', 
           'INVOICE_USAGE_COLLO_WEIGHT', 'INVOICE_USAGE_DESCRIPTION_4', 'INVOICE_USAGE_DESCRIPTION_2', 'INVOICE_USAGE_UNIT_PRICE', 
           'INVOICE_USAGE_CONSUMPTION_DATE']


NAMES = ['tracking_number', 'description', 'real_price', 'invoice_number', 'item_description', 
         'height', 'length', 'width', 'weight', 'returns', 'zone', 'globalpack_price', 'date']


TYPES = ['Collo', 'Zending', 'Afleveren alleen huisadres', 'Handtekening voor ontvangst', 'Avondbezorging', 'Ophalen bij een PostNL locatie',
         'Handtek. voor ontvangst Alleen Huisadres', 'Handmatige verwerking', 'Niet (correct) voorgemeld', 'Notificatie bezorgstatus (SMS)',
         'Ophalen bij een pakketautomaat', 'Tolheffing', 'Verh. aansprakelijkheid', 'Kg', 'Overschrijding maximum pakket dimensies', 'Maandag bezorging', 
         'Toeslag pakketten >23kg', 'Leeftijdscheck18+', 'Laat Aanleveren']


PRICES = ['Collo', 'Afleveren alleen huisadres', 'Handtekening voor ontvangst', 'Avondbezorging', 'Handtek. voor ontvangst Alleen Huisadres',
          'Verh. aansprakelijkheid', 'Kg', 'Ophalen bij een PostNL locatie', 'Zending', 'Leeftijdscheck18+', 'Ophalen bij een pakketautomaat']   


CLEAN = ['postnl:standard/row', 'postnl:standard/kg=0-31.5,return', 'postnl:brievenbus/kg=0-2,segment=C', 'postnl:brievenbus/kg=0-2', 'postnl:brievenbus/kg=0-2,segment=A+', 
         'postnl:standard/eu,return']                


DIM_MASK = ['tracking_number', 'weight', 'length', 'height', 'width']


PRICING_MASK = ['tracking_number', 'description', 'real_price']


NMG = [293, 291, 294, 282, 272, 283, 274, 284, 275, 286, 277, 285, 276, 287, 278, 288, 279, 289, 280, 290, 281, 298, 296, 
      273, 292, 295, 297, 2242, 2243]


NMG_INCREASE = 3.01


SERVICE = [7, 293]


SERVICE_DECREASE = -0.1


MASK = ['tracking_number', 
        'description', 
        'price', 
        'real_price', 
        'type', 
        'carrier', 
        'from_country',
        'currency', 
        'internal_note']


PRICING_DICT = {
    'Overschrijding maximum pakket dimensies': 76.45, 
    'Handmatige verwerking': 3.05,
    'Toeslag pakketten >23kg': 3.05
}        


# queries
returns_query = """
select 
    reference, 
    price 
from 
    invoice_item
where 
    "type" = 'shipment' 
and 
    "date" > '{}'
and
    carrier_id = 2
and
    reference in {}
"""


nmg_created_query = """
select 
	p.tracking_number, 
    ii.description "description_panel" 
from 
	parcel p
left join invoice_item ii
    on p.parcel_id = ii.parcel_id
where 
    p.cancellation_status = 0
and
    p.shipping_method_id in (84, 273, 291, 292, 293, 294, 295, 297, 2242, 2243)
and
    p.tracking_number in {}
and
    ii."type" = 'shipment'    
"""


# functions
def get_tracking_number(df):
    if pd.isnull(df['INVOICE_USAGE_AGGREGATION_VALUE_1']) == True:
        return df['INVOICE_USAGE_AGGREGATION_VALUE_2']
    else:
        return df['INVOICE_USAGE_AGGREGATION_VALUE_1']


def get_invoice_data_postnl(i):
    """
    Returns the carrier invoice into a DataFrame.
    """
    df = pd.read_csv(i, 
                     encoding='latin-1', 
                     sep=';', 
                     converters={'INVOICE_USAGE_COLLO_HEIGHT': lambda i: i.replace(',', '.'), 
                                 'INVOICE_USAGE_COLLO_LENGTH': lambda i: i.replace(',', '.'), 
                                 'INVOICE_USAGE_COLLO_WIDTH': lambda i: i.replace(',', '.'), 
                                 'INVOICE_USAGE_COLLO_WEIGHT': lambda i: i.replace(',', '.'), 
                                 'INVOICE_USAGE_NET_VALUE': lambda i: i.replace(',', '.'),
                                 'INVOICE_USAGE_UNIT_PRICE': lambda i: i.replace(',', '.'),
                                 'INVOICE_USAGE_CONSUMPTION_DATE': lambda i: str(i)}, 
                     low_memory=False, 
                     error_bad_lines=False)    

    df['INVOICE_USAGE_AGGREGATION_VALUE_1'] = df.apply(get_tracking_number, axis=1)
    
    return df 


# def get_invoice_data_postnl(path, invoice):
#     """
#     Returns the carrier invoice into a DataFrame.
#     """
#     df = pd.read_csv(path + invoice +'.csv', 
#                      encoding='latin-1', 
#                      sep=';', 
#                      converters={'INVOICE_USAGE_COLLO_HEIGHT': lambda i: i.replace(',', '.'), 
#                                  'INVOICE_USAGE_COLLO_LENGTH': lambda i: i.replace(',', '.'), 
#                                  'INVOICE_USAGE_COLLO_WIDTH': lambda i: i.replace(',', '.'), 
#                                  'INVOICE_USAGE_COLLO_WEIGHT': lambda i: i.replace(',', '.'), 
#                                  'INVOICE_USAGE_NET_VALUE': lambda i: i.replace(',', '.'),
#                                  'INVOICE_USAGE_UNIT_PRICE': lambda i: i.replace(',', '.'),
#                                  'INVOICE_USAGE_CONSUMPTION_DATE': lambda i: str(i)}, 
#                      low_memory=False, 
#                      error_bad_lines=False)    

#     df['INVOICE_USAGE_AGGREGATION_VALUE_1'] = df.apply(get_tracking_number, axis=1)
    
#     return df   


def get_select_columns_postnl(df):
    """
    Returns the columns used for further analysis into a DataFrame.
    """
    df = df[COLUMNS]
    df.columns = NAMES   

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df['height'] = [float(i) for i in list(df['height'])]
    df['length'] = [float(i) for i in list(df['length'])]
    df['width'] = [float(i) for i in list(df['width'])]
    df['weight'] = [float(i) for i in list(df['weight'])]
    df['real_price'] = [float(i) for i in list(df['real_price'])]
    df['globalpack_price'] = [float(i) for i in list(df['globalpack_price'])]    
    df['date'] = pd.to_datetime(df['date'])
    
    return df


def get_new_products_of_carrier(df):
    """
    Returns a print statement of products that are added to the invoice.
    """
    lst = []

    for i in list(df['description'].unique()):
        if i not in TYPES:
            lst.append(i)

    if len(lst) > 0:
        print('the carrier added these products: ', lst)
        df = pd.DataFrame({'new products':lst})
    else:
        print('no products added!')
        df = pd.DataFrame({'new products':['no products added!']})
    
    return df


def get_parcels_not_created_at_sendcloud(df, sc):
    """
    Returns a DataFrame of tracking_numbers which are not created via the panel of Sendcloud.
    """    
    sc = sc[sc['description_panel'].isna()]
    df = df[df['INVOICE_USAGE_AGGREGATION_VALUE_1'].isin(list(sc['tracking_number']))]

    return df


def get_dimensions_postnl(df):
    """
    Returns a DataFrame with tracking-number and dimensions.
    """
    di = df[df['weight'] > 0]
    di = di[DIM_MASK]
    di = di.drop_duplicates('tracking_number')

    df = df.drop(['height', 'length', 'width', 'weight'], axis=1)
    df = df.join(di.set_index('tracking_number'), on='tracking_number', how='left')
    df[['weight', 'length', 'width','height']] = df[['weight', 'length', 'width','height']].fillna(1)

    return df


def get_real_price_postnl(df):
    """
    Returns a DataFrame with the PostNL real prices.
    """
    pr = df[(df['description'].isin(PRICES)) & (df['returns'] != 'Retouren')]
    pr = pr[PRICING_MASK]
    pr = pr.dropna(subset=['tracking_number'])
    
    pr['real_price'] = pr.groupby('tracking_number')['real_price'].transform(sum)
    pr = pr[pr['description'] == 'Collo']
    pr = pr.drop(['description'], axis=1)
    df = df[df['description'] == 'Collo']
    df = df.drop(['real_price'], axis=1)
    df = df.join(pr.set_index('tracking_number'), on='tracking_number', how='left')
    
    return df    


def get_weight_class_postnl(df):
    """
    Function that returns the code used withing Sendcloud.
    """
    if df['weight'] <= 23:
        return 'kg=0-23'
    elif df['weight'] <= 31.5:
        return 'kg=23-31.5'
    else:
        return 'max'


def get_code_postnl(df):
    """
    Function that returns the postnl code.
    """
    if df['code'] == 'postnl:pakjegemak/agecheck=18':
        if df['weight'] <= 23:
            return 'postnl:pakjegemak/agecheck=18'
        else:
            return 'postnl:pakjegemak/agecheck=18/max'
    elif df['code'] in ['postnl:standard/eu', 'postnl:standard/eu,non_conveyable']:
        if df['weight'] <= 23:
            return 'postnl:standard/eu'
        else:
            return 'postnl:standard/eu,non_conveyable'
    else:
        if df['weight'] <= 23:
            return re.sub(r'kg=\d+\-\d+', df['weight_class_postnl'], df['code'])
        else:
            return re.sub(r'kg=\d+\-\d+\.\d+', df['weight_class_postnl'], df['code'])


def get_change_real_price_nmg_and_service_point(df):
    """
    Function adjusts the real price for NMG and service point shipments.
    """
    if df['shipping_method_id'] in NMG:
        return df['real_price'] + NMG_INCREASE
    else:
        return df['real_price']


def get_real_price_comparison_dataframe(df):
    """
    Returns a DataFrame where realprice PostNL and realprice sendcloud are compared.
    """
    df = df[(~df['code'].isna()) & (df['collo_count'] == 1)]
    df['weight_class_postnl'] = df.apply(get_weight_class_postnl, axis=1)
    df['code_postnl'] = df.apply(get_code_postnl, axis=1)
    df['filter'] = (df['code_postnl'] == df['code'])
    df['real_price'] = df.apply(get_change_real_price_nmg_and_service_point, axis=1)

    diff = df[df['filter'] == False]
    diff['filter'] = diff['weight'] > diff['weight_panel']
    ws = diff[diff['filter'] == True]
    cr = diff[diff['filter'] == False]
    cl = df[(df['filter'] == True) & (~df['code'].isin(CLEAN))]

    gr = pd.DataFrame(cl.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_postnl', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_postnl']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_postnl'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_postnl'] = gr['shipments'] * gr['real_price_postnl']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('#shipments * dif')
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(4)   
    
    return df, gr, ws, cr  


def get_returns_postnl(df):
    """
    Returns a pivot table of the count, mean and sum of the returns.
    """
    df = df[(df['returns'] == 'Retouren') & (df['real_price'] > 0) & (df['direction'] == 'o')]
    df['zone'].fillna('NL', inplace=True)
    df = df[df['description'] == 'Collo']
    gr = pd.DataFrame(df.groupby(['zone']).agg({'real_price':['count', 'mean', sum]}))
    gr.columns = ['count', 'mean', 'sum']
    gr = gr.sort_values('sum', ascending=False)
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(2)
    
    return gr


def get_globalpack_real_price_overview_postnl(df, pricing):
    """
    Returns a DataFrame that compares the globalpack prices invoiced by PostNL with the DB of Sendcloud.
    """
    df = df[(df['item_description'] == 'Pakketten niet-EU') & (df['description'] == 'Kg')]
    df['description'] = 'PostNL GlobalPack'
    df['id'] = df['code'] + 'NL' + df['shipping_country']

    pa = pd.read_csv(pricing)
    pa = pa[pa['weight_allowance'] > 0]
    pa['id'] = pa['method_code'] + 'NL' + pa['to_country']
    pa = pa[['id', 'real_weight_allowance']]
    
    df = df.join(pa.set_index('id'), on='id', how='left')
    df = df.groupby(['description', 'shipping_country']).agg({'globalpack_price':'mean', 'real_weight_allowance':'mean'})
    df.columns = ['real_price_postnl', 'real_price_panel']
    df['dif'] = df['real_price_panel'] - df['real_price_postnl']
    df = df.sort_values('dif').round(2)
    
    return df


def get_postnl_surcharge_price(i):
    """
    Returns the price of the surcharge based on the description.
    """
    return PRICING_DICT.get(i)


def get_overschrijding_maximale_dimensies_description_postnl(df):
    """
    Function that returns the correct description regarding the overschrijving maximale dimensies surcharge.
    """
    if df['weight'] > 31.5:
        return 'Toeslag overschrijding maximale dimensies - gewogen gewicht: ' + str(df['weight']) + ' (kg)'
    else:
        return 'Toeslag overschrijding maximale dimensies - afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) + ' (cm)'


def get_overschrijding_maximale_dimensies_toeslag_postnl(df):
    """
    Returns a DataFrame of the overschrijving maximale dimensies toeslag.
    """
    df = df[df['description'] == 'Overschrijding maximum pakket dimensies']
    df['price'] = df['description'].apply(get_postnl_surcharge_price)
    df['description'] = df.apply(get_overschrijding_maximale_dimensies_description_postnl, axis=1)
    df = df[(df['length'] >= 176) | (df['width'] >= 70) | (df['height'] >= 58) | (df['weight'] > 31.5)]
    print('# overschrijding maximale dimensies: ', len(df))
    
    return df


def get_niet_machine_geschikt_description_postnl(df):
    """
    Function that returns the correct description regarding the nmg surcharge.
    """
    if df['length'] > 100 and (df['length'] <= 176 or df['width'] > 70 or df['height'] > 58):
        return 'Toeslag niet machine geschikt - afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) +' (cm)'
    elif df['weight'] > 30 and df['weight'] <= 31.5:
        return 'Toeslag niet machine geschikt - gewogen gewicht: ' + str(df['weight']) + ' (kg)'
    else:
        return 'Toeslag wegens aard van verpakking'


def get_handmatige_verwerking_toeslag_postnl(df):
    """
    Returns a DataFrame of the handmatige verwerking surcharge.
    """
    df = df[df['description'] == 'Handmatige verwerking'] 
    pa = df[df['shipping_method_id'].isin(NMG)]
    print('# NMG created: ', len(pa))
    df = df[~df['tracking_number'].isin(list(pa['tracking_number']))].copy()
    df['price'] = df['description'].apply(get_postnl_surcharge_price)
    df['description'] = df.apply(get_niet_machine_geschikt_description_postnl, axis=1)
    print('# handmatige verwerking: ', len(df))
    
    return df


def get_toeslag_pakketten_23kg(df):
    """
    Returns a DataFrame of the toeslag pakketten > 23 kg.
    """
    df = df[df['description'] == 'Toeslag pakketten >23kg'].copy()
    pa = df[df['shipping_method_id'].isin(NMG)]
    print('# NMG created: ', len(pa))
    df = df[~df['tracking_number'].isin(list(pa['tracking_number']))]
    df['price'] = df['description'].apply(get_postnl_surcharge_price)
    df['description'] = 'Toeslag pakket zwaarder dan 23kg - gewogen gewicht: ' + df['weight'].astype(str) + ' (kg)'
    print('# toeslag pakket > 23kg: ', len(df))
    
    return df


def get_retouren_postnl(df):
    """
    Returns a DataFrame of the return surcharges.
    """
    re = df[(df['returns'] == 'Retouren') & (df['real_price'] > 0) & (~df['price_panel'].isna()) & (df['direction'] == 'o')]
    re['price'] = re['price_panel']
    re['description'] = 'Pakket retour afzender'
    print('# returns: ', len(re))
    
    return re


def get_piek_toeslag_postnl(df):
    """
    Returns a DataFrame of the piek surcharge.
    """
    df = df[df['description'] == 'Piek toeslag']
    df['description'] = 'PostNL piektoeslag'
    df['price'] = df['real_price']
    print('# piektoeslag: ', len(df))
    
    return df


def get_surcharges_postnl(df):
    """
    Returns a DataFrame of all surcharges.
    """
    ma = get_overschrijding_maximale_dimensies_toeslag_postnl(df)
    ha = get_handmatige_verwerking_toeslag_postnl(df)
    tp = get_toeslag_pakketten_23kg(df)
    re = get_retouren_postnl(df)
    # pi = get_piek_toeslag_postnl(df)
    df = pd.concat([ma, ha, tp, re])
    df = df.dropna(subset=['description_panel'])
    df['carrier'] = 'postnl'
    df['type'] = 'surcharge'
    df['currency'] = 'EUR'
    df['from_country'] = 'NL'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df = df[MASK]

    return df


def get_globalpack_surcharges_postnl(df):
    """
    Returns a DataFrame of the weight allowances surcharges postnl.
    """
    df = df[(df['item_description'] == 'Pakketten niet-EU') & (df['weight'] > 0) & (df['description'] == 'Collo')]
    df['weight_class'] = df['weight'].apply(np.ceil)
    df['weight_class_panel'] = df['weight_panel'].apply(np.ceil)
    df['filter'] = df['weight_class'] - df['weight_class_panel']
    df = df[df['filter'] > 0]
    df['code'] = df['code'] + 'NL' + df['shipping_country']    

    pa = pd.read_csv(PRICING)
    pa = pa[pa['weight_allowance'] > 0]
    pa['method_code'] = pa['method_code'] + 'NL' + pa['to_country']
    
    pr = pa[['method_code', 'weight_allowance']]
    rp = pa[['method_code', 'real_weight_allowance']]
    
    df = df.join(pr.set_index('method_code'), on='code', how='left')
    df = df.join(rp.set_index('method_code'), on='code', how='left')
    
    df['price'] = df['filter'] * df['weight_allowance']
    df['real_price'] = df['filter'] * df['real_weight_allowance']
    df = df[df['price'] > 0]
    df['description'] = 'Surcharge - selected weight: ' + df['weight_panel'].astype(str) + ' (kg)' + ' measured weight: ' + df['weight'].astype(str) + ' (kg)'    
    df['carrier'] = 'postnl'
    df['type'] = 'surcharge'
    df['currency'] = 'EUR'
    df['from_country'] = 'NL'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df = df[MASK]

    return df                                            