# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import re
import datetime as dt 
from sqlalchemy import create_engine


# notes
"""
1) Brandstoftoeslag: https://www.dhlparcel.nl/nl/zakelijk/support/verzenden/brandstoftoeslag
    * Brandstoftoeslag zit op de zending + de toeslagen op de zending
    ** Op add-ons (niet bij buren etc.) zit geen brandstoftoeslag
    *** Op add-offs (servicepoint, buspakje) zit geen brandstoftoeslag
2) Toltoeslag: https://www.dhlparcel.nl/nl/zakelijk/support/verzenden/toltoeslag
    * Toltoeslag zit op de zending + de toeslagen op de zending
    ** Op add-ons (niet bij buren etc.) zit geen toltoeslag 
    *** Op add-offs (servicepoint, buspakje) zit geen toltoeslag
3) We missen de shipping_method DHL for you drop off, home address only
4) DhlForYou is particulier
5) Dhl Europlus is zakelijk


DHL invoiced de zending plus toeslagen keer de brandstof- en toltoeslag. Over de add-ons-offs worden geen brandstof- en toltoeslagen berekend.
"""


# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
PRICING = r'C:\Users\Rick Vloet\Documents\carrier-pricing\pricing_dhl.csv'
DATE_RANGE = str((dt.date.today() - dt.timedelta(290)))
FUEL_SURCHARGE = 1.1
RETURN_FEE = 1.25
FUEL = 0.07


COLUMNS = ['Invoice Number', 
           'Shipment Number', 
           'Shipment Date', 
           'Shipment Reference 1', 
           'Shipment Reference 2', 
           'Shipment Reference 3', 
           'Product', 
           'Pieces', 
           'Weight (kg)', 
           'Total Amount']


NAMES = ['invoice_number', 
         'shipment_number', 
         'date',
         'shipment_reference_1', 
         'tracking_number', 
         'shipment_reference_3',
         'product', 
         'pieces', 
         'weight', 
         'total_amount',
         'description', 
         'real_price']        


TYPES = ['DOUANE AFHAND', 'Debet', 'GO GREEN', 'Buiten specs', 'Buiten sp. BE', 'XPALLETS',
         'nan', 'Tol DE', 'Tol BE', '0', 'DIESELTOESL.', 'Avondservice', 'Handtekening', 'Niet b. buren', 'Service point', 
         'Buspakje', 'Extra Zeker', 'ZATERDAG', 'Tol HU', 0.0, 'DHL EUROPLUS ZONE 0 COL', 'DHL EUROPLUS ZONE 2 PAL', 
         'DHL EUROPLUS ZONE 1 COL','DHL EUROPLUS ZONE 0 PAL', 'DHL EUROPLUS ZONE 1 PAL','EXPRESSER ZONE 0 COL',
         'EXPRESSER ZONE 1 COL','DHL EUROPLUS ZONE 2 COL', 'DHL FOR YOU ZONE 0', 'DHL FOR YOU ZONE 1', 'DHL FOR YOU ZONE 2', 
         'DHL EUROPLUS ZN 7 PAL INT', 'DHL EUROPLUS ZONE N', 'DHL EUROPLUS ZONE Y', 'DHL PARCEL CONNECT SE', 
         'DHL EUROPLUS ZONE C', 'DHL PARCEL CONNECT DE', 'DHL EUROPLUS ZONE E', 'DHL EUROPLUS ZONE L', 
         'DHL EUROPLUS ZN 4 PAL INT', 'DHL EUROPLUS ZONE B', 'DHL EUROPLUS ZONE Q', 'DHL EUROPLUS ZONE K', 
         'DHL EUROPLUS ZN 6 PAL INT', 'DHL PARCEL CONNECT PL', 'DHL PARCEL CONNECT GB', 'DHL EUROPLUS ZONE H',
         'DHL EUROPLUS ZONE F', 'DHL EUROPLUS ZONE O', 'DHL EUROPLUS ZONE P', 'DHL EUROPLUS ZONE X', 
         'DHL EUROPLUS ZONE T', 'DHL EUROPLUS ZONE U', 'DHL EUROPLUS ZONE I', 'DHL PARCEL CONNECT AT', 
         'DHL PARCEL CONNECT CZ', 'DHL EUROPLUS ZONE V', 'DHL PARCEL CONNECT ES', 'DHL EUROPLUS ZONE R', 
         'DHL PARCEL CONNECT FR', 'DHL EUROPLUS ZONE D', 'DHL EUROPLUS ZONE Z', 'DHL EUROPLUS ZN 9 PAL INT', 
         'DHL EUROPLUS ZONE G', 'DHL EUROPLUS ZONE S', 'DHL EUROPLUS ZN 5 PAL INT', 'DHL EUROPLUS ZN 8 PAL INT', 
         'DHL EUROPLUS ZONE A', 'DHL EUROPLUS ZONE J', 'DHL PARCEL CONNECT SK', 'DHL PARCEL CONNECT EE', 
         'DHL PARCEL CONNECT SI', 'DHL PARCEL CONNECT PT', 'DHL PARCEL CONNECT FI', 'DHL EUROPLUS ZN 3 PAL INT', 
         'DHL PARCEL CONNECT LT', 'DHL EUROPLUS ZONE 5', 'DHL PARCEL CONNECT HU', 'DHL PARCEL CONNECT IE', 
         'DHL PARCEL CONNECT HR', 'DHL PARCEL CONNECT GR', 'DHL PARCEL CONNECT DK', 'DHL PARCEL CONNECT RO', 
         'DHL EUROPLUS ZONE W', 'DHL PARCEL CONNECT BG', 'DHL EUROPLUS ZONE M', 'DHL PARCEL BE', 'WADDENTOESLAG', 
         'DHL EUROPACK ZONE a', 'COLLI', 'DHL PARCEL CONNECT LU', 'WEIGERING RET', 'DHL PARCEL CONNECT LV', 'EXPRESSER ZONE 0 PAL', 
         'DHL PARCEL CONNECT IT', 'EXPRESSER ZONE 2 COL', 'EXTRA ZWAAR', 'XHANDLING', 'Euro Pallets', 'Xhandling doc', 'PAKKET OP PAL', 
         'XRUIMTE -49%', 'XGROOT', 'XRUIMTE', 'XZWAAR', 'Nieuw label', 'AFG. GEBIED', 'ZWAAR PAKKET', 'PALLETS']


PRICES = ['Tol DE', 'Tol BE', 'Avondservice', 'Handtekening', 'Niet b. buren', 'Service point', 'Extra Zeker', 'DIESELTOESL.',
          'ZATERDAG', 'Tol HU', 'DHL EUROPLUS ZONE 0 COL', 'DHL EUROPLUS ZONE 2 PAL', 'DHL EUROPLUS ZONE 1 COL', 'DOUANE AFHAND',
          'DHL EUROPLUS ZONE 0 PAL', 'DHL EUROPLUS ZONE 1 PAL', 'EXPRESSER ZONE 0 COL','EXPRESSER ZONE 1 COL',
          'DHL EUROPLUS ZONE 2 COL', 'DHL FOR YOU ZONE 0', 'DHL FOR YOU ZONE 1', 'DHL FOR YOU ZONE 2', 
          'DHL EUROPLUS ZN 7 PAL INT', 'DHL EUROPLUS ZONE N', 'DHL EUROPLUS ZONE Y', 'DHL PARCEL CONNECT SE', 
          'DHL EUROPLUS ZONE C', 'DHL PARCEL CONNECT DE', 'DHL EUROPLUS ZONE E', 'DHL EUROPLUS ZONE L', 
          'DHL EUROPLUS ZN 4 PAL INT', 'DHL EUROPLUS ZONE B', 'DHL EUROPLUS ZONE Q', 'DHL EUROPLUS ZONE K', 
          'DHL EUROPLUS ZN 6 PAL INT', 'DHL PARCEL CONNECT PL', 'DHL PARCEL CONNECT GB', 'DHL EUROPLUS ZONE H',
          'DHL EUROPLUS ZONE F', 'DHL EUROPLUS ZONE O', 'DHL EUROPLUS ZONE P', 'DHL EUROPLUS ZONE X', 
          'DHL EUROPLUS ZONE T', 'DHL EUROPLUS ZONE U', 'DHL EUROPLUS ZONE I', 'DHL PARCEL CONNECT AT', 
          'DHL PARCEL CONNECT CZ', 'DHL EUROPLUS ZONE V', 'DHL PARCEL CONNECT ES', 'DHL EUROPLUS ZONE R', 
          'DHL PARCEL CONNECT FR', 'DHL EUROPLUS ZONE D', 'DHL EUROPLUS ZONE Z', 'DHL EUROPLUS ZN 9 PAL INT', 
          'DHL EUROPLUS ZONE G', 'DHL EUROPLUS ZONE S', 'DHL EUROPLUS ZN 5 PAL INT', 'DHL EUROPLUS ZN 8 PAL INT', 
          'DHL EUROPLUS ZONE A', 'DHL EUROPLUS ZONE J', 'DHL PARCEL CONNECT SK', 'DHL PARCEL CONNECT EE', 
          'DHL PARCEL CONNECT SI', 'DHL PARCEL CONNECT PT', 'DHL PARCEL CONNECT FI', 'DHL EUROPLUS ZN 3 PAL INT', 
          'DHL PARCEL CONNECT LT', 'DHL EUROPLUS ZONE 5', 'DHL PARCEL CONNECT HU', 'DHL PARCEL CONNECT IE', 
          'DHL PARCEL CONNECT HR', 'DHL PARCEL CONNECT GR', 'DHL PARCEL CONNECT DK', 'DHL PARCEL CONNECT RO', 
          'DHL EUROPLUS ZONE W', 'DHL PARCEL CONNECT BG', 'DHL EUROPLUS ZONE M', 'DHL PARCEL BE', 'Buspakje', 
          'DHL EUROPACK ZONE a', 'DHL PARCEL CONNECT LU', 'DHL PARCEL CONNECT LV', 'EXPRESSER ZONE 2 COL', 'GO GREEN', 
          'EXPRESSER ZONE 0 PAL']



SURCHARGES = ['EXTRA ZWAAR', 
              'XHANDLING', 
              'Euro Pallets', 
              'Xhandling doc', 
              'PAKKET OP PAL', 
              'XRUIMTE -49%', 
              'XGROOT', 
              'XRUIMTE', 
              'XZWAAR', 
              'Nieuw label', 
              'AFG. GEBIED',
              'WADDENTOESLAG',
              'Buiten specs', 
              'Buiten sp. BE', 
              'ZWAAR PAKKET', 
              'PALLETS',
              'XPALLETS']


NO_TOLL = ['DOUANE AFHAND']             


ADD_ONN = ['Avondservice', 'Handtekening', 'Niet b. buren', 'Extra Zeker', 'ZATERDAG']


ADD_OFF = ['Service point', 'Buspakje']


EUROPLUS = ['DHL EUROPLUS ZONE 0 COL',  'DHL EUROPLUS ZONE 1 COL', 'DHL EUROPLUS ZONE 2 COL', 'DHL EUROPLUS ZONE N', 'DHL EUROPLUS ZONE Y', 
            'DHL EUROPLUS ZONE C', 'DHL EUROPLUS ZONE E', 'DHL EUROPLUS ZONE L', 'DHL EUROPLUS ZONE B', 'DHL EUROPLUS ZONE Q', 'DHL EUROPLUS ZONE K', 
            'DHL EUROPLUS ZONE H', 'DHL EUROPLUS ZONE F', 'DHL EUROPLUS ZONE O', 'DHL EUROPLUS ZONE P', 'DHL EUROPLUS ZONE X', 
            'DHL EUROPLUS ZONE T', 'DHL EUROPLUS ZONE U', 'DHL EUROPLUS ZONE I', 'DHL EUROPLUS ZONE V', 'DHL EUROPLUS ZONE R', 
            'DHL EUROPLUS ZONE D', 'DHL EUROPLUS ZONE Z',  'DHL EUROPLUS ZONE G', 'DHL EUROPLUS ZONE S',   'DHL EUROPLUS ZONE A', 'DHL EUROPLUS ZONE J', 
            'DHL EUROPLUS ZONE 5', 'DHL EUROPLUS ZONE W', 'DHL EUROPLUS ZONE M', 'DHL EUROPACK ZONE a']


EXPRESS = ['EXPRESSER ZONE 0 COL','EXPRESSER ZONE 1 COL', 'EXPRESSER ZONE 2 COL']


PARCEL_CONNECT = ['DHL PARCEL CONNECT DE', 'DHL PARCEL CONNECT PL', 'DHL PARCEL CONNECT GB', 'DHL PARCEL CONNECT AT', 
                  'DHL PARCEL CONNECT CZ', 'DHL PARCEL CONNECT ES', 'DHL PARCEL CONNECT FR', 'DHL PARCEL CONNECT SK', 
                  'DHL PARCEL CONNECT EE', 'DHL PARCEL CONNECT SI', 'DHL PARCEL CONNECT PT', 'DHL PARCEL CONNECT FI', 
                  'DHL PARCEL CONNECT LT', 'DHL PARCEL CONNECT HU', 'DHL PARCEL CONNECT IE', 'DHL PARCEL CONNECT HR', 
                  'DHL PARCEL CONNECT GR', 'DHL PARCEL CONNECT DK', 'DHL PARCEL CONNECT RO', 'DHL PARCEL CONNECT BG', 
                  'DHL EUROPLUS ZONE M',  'DHL PARCEL CONNECT LU', 'DHL PARCEL CONNECT LV', 'DHL PARCEL CONNECT SE', 'DHL PARCEL CONNECT IT']


DHLFORYOU = ['DHL FOR YOU ZONE 0', 'DHL FOR YOU ZONE 1', 'DHL FOR YOU ZONE 2']


PALLET = ['DHL EUROPLUS ZONE 2 PAL', 'DHL EUROPLUS ZONE 0 PAL', 'DHL EUROPLUS ZONE 1 PAL', 'DHL EUROPLUS ZN 7 PAL INT',
          'DHL EUROPLUS ZN 4 PAL INT', 'DHL EUROPLUS ZN 6 PAL INT', 'DHL EUROPLUS ZN 9 PAL INT', 'DHL EUROPLUS ZN 5 PAL INT', 
          'DHL EUROPLUS ZN 8 PAL INT', 'DHL EUROPLUS ZN 3 PAL INT', 'EXPRESSER ZONE 0 PAL']


METHODS = EUROPLUS + EXPRESS + PARCEL_CONNECT + DHLFORYOU + PALLET


TOLL_BE = ['BE']
TOLL_THROUGH_BE = ['FR', 'IE', 'LU', 'PT', 'ES', 'GB']
TOLL_THROUGH_DE = ['BG', 'DK', 'DE', 'EE', 'FI', 'GR', 'HU', 'IT', 'HR', 'LV', 'LT', 'NO', 'AT', 'PL', 'RO', 'SI', 'SK', 'CZ', 'SE', 'CH']
TOLL_THROUGH_HG = ['BG', 'HU', 'RO']


DESCRIPTION = ['Product Name' if i == 0 else 'XC' + str(i) + ' Name' for i in range(10)]
CHARGE = ['Weight Charge' if i == 0 else 'XC' + str(i) + ' Charge' for i in range(10)]


PIECES_DICT = {
    'EXTRA ZWAAR': 2.90,
    'ZWAAR PAKKET': 2.90,
    'Euro Pallets': 2.95,
    'Nieuw label': 0.95, 
    'WADDENTOESLAG': 5,
    'XGROOT': 45,
    'XHANDLING': 4.90,
    'XRUIMTE': 35.93,
    'XRUIMTE -49%': 35.93,
    'PAKKET OP PAL': 35.93,
    'XZWAAR': 45, 
    'Xhandling doc': 3.2,
    'Buiten specs': 4.9,
    'Buiten sp. BE': 45,
    'XPALLETS': 75
}


PRICING_DICT = {
    'ZWAAR PAKKET': 3.20,
    'Euro Pallets': 3.25,
    'Nieuw label' : 1.05,
    'XGROOT' : 49.5,
    'XHANDLING' : 5.40,
    'XRUIMTE' : 0,
    'XRUIMTE -49%': 0,
    'XZWAAR' : 49.5, 
    'Xhandling doc' : 3.5,
    'Buiten specs': 5.4,
    'Buiten sp. BE': 49.5,
    'XPALLETS' : 82.5   
}

PRICING_AFG_GEBIED = 0.38


SURCHARGES_NAMES_DICT = {
    'ZWAAR PAKKET': 'Toeslag zwaar pakket',
    'Buiten specs': 'Toeslag buiten specificaties Nederland',
    'Buiten sp. BE': 'Toeslag buiten specificaties naar Belgie',
    'XHANDLING': 'Toeslag aard van verpakking',
    'Xhandling doc': 'Toeslag buiten specificaties klein pakket',
    'WADDENTOESLAG' : 'Waddentoeslag',
    'AFG. GEBIED': 'Toeslag afgelegen gebied',
    'Nieuw label' : 'Toeslag nieuw label',
    'Euro Pallets' : 'Toeslag pallet omruilservice',
    'XGROOT': 'Toeslag extra groot',
    'XRUIMTE': 'XRUIMTE',
    'XRUIMTE -49%': 'XRUIMTE -49%',
    'XZWAAR': 'Toeslag extra zwaar', 
    'DHL Europlus Pallet': 'DHL Europlus Pallet',
    'XPALLETS': 'Toeslag extra pallet'
}


MASK = ['tracking_number', 
        'description',
        'price', 
        'real_price',
        'type',
        'from_country',
        'carrier', 
        'currency',
        'internal_note']


# queries
panel_data_query = """
select
    p.tracking_number, 
    ii.description "description_panel", 
    ii.price "price_panel",
    ii.real_price "real_price_panel", 
    p.weight "weight_panel", 
    p.shipping_method_id, 
    (case when sm."returns" then rc.iso_2
    	  else sc.iso_2 end) "shipping_country", 
    sm.code,
    p.collo_count,
    p.direction,
    cc."type" "contract_type",
    cast(p.parcel_id as varchar) "parcel_id_sc",
    p.parent_parcel_status_id,
    json(p.extra_data -> 'measured_width') "measured_width",
	json(p.extra_data -> 'measured_height') "measured_height",
	json(p.extra_data -> 'measured_length') "measured_length",
	json(p.extra_data -> 'measured_weight') "measured_weight"
from 
    parcel p
left join invoice_item ii
	on p.parcel_id = ii.parcel_id
left join shipping_country  sc
	on p.shipping_country_id = sc.shipping_country_id
left join shipping_country rc 
	on p.from_country_id = rc.shipping_country_id
left join shipping_method sm 
	on p.shipping_method_id = sm.shipping_method_id
left join contracts_contract cc 
    on cc.id = p.contract_id
where
    ii."type" = 'shipment'
and
    ii.description != 'Reversed cancellation'    
and
    p.announced_at >= '{}'    
and
    p.{} in {}     
and
    p.carrier_code = 'dhl'   
"""


real_price_query = """
select
    p.colli_uuid, 
    sum(ii.real_price) "real_price_multi_collo"
from 
    parcel p 
    left join invoice_item ii 
        on p.parcel_id = ii.parcel_id
where 
    p.colli_uuid in {}
and 
    ii."type" = 'shipment'    
group by p.colli_uuid
"""


letterbox_details_query = """
select
	p.tracking_number, 
	ii.description "description_panel" 
from 
    invoice_item ii 
left join parcel p 
	on p.parcel_id = ii.parcel_id
where 
	ii."type" = 'shipment'
and 
	p.shipping_method_id in (340, 492, 1543, 2620)
and 
	p.cancellation_status = 0
and 
	p.tracking_number in {}
and 
	ii.date > '{}'
"""


return_price_query = """
select
    p.tracking_number, 
    cast(p.parcel_id as varchar), 
    ii.price
from
    parcel p
left join invoice_item ii
    on p.parcel_id = ii.parcel_id
where 
    ii."type" = 'shipment'    
and
    ii.description != 'Reversed cancellation'
and
    p.cancellation_status = 0
and
    p.parcel_id {} {}
and
    ii."date" >= '{}'
"""


weigering_retouren_query = """
select
    p.tracking_number, 
    ii.price
from
    parcel p
left join invoice_item ii
    on p.parcel_id = ii.parcel_id
where
    ii."type" = 'shipment'    
and
    ii.description != 'Reversed cancellation'
and
    p.cancellation_status = 0
and
    p.tracking_number in {}    
and 
    ii."date" >= '{}'
"""


# functions
def get_invoice_data(i):
    df = pd.read_excel(i, 
                       encoding='latin-1',
                       sep=';',
                       converters={'Shipment Date': lambda i: str(i), 
                                   'Invoice Number': lambda i: str(i),
                                   'Weight (kg)': lambda i: i.replace(',', '')})
        
    df = df[df['Line Type'] == 'S']
    
    return df             


def get_parcel_id(df):
    if 'Zendingnr' in str(df['tracking_number']):
        if len(re.findall(pattern='\d+', string=df['tracking_number'])) != 0:
            return int(re.findall(pattern='\d+', string=df['tracking_number'])[1])
        else:
            return 0
    else:
        return 0        


def get_dhl_method(df):
    if df['description'] in EUROPLUS or df['description'] in EXPRESS or df['description'] in PALLET:
        return 'europlus'
    elif df['description'] in PARCEL_CONNECT:
        return 'parcel_connect'
    else:
        return 'dhlforyou'


def get_select_columns(df):
    lst = []

    for i in range(len(DESCRIPTION)):
        c = COLUMNS
        c.extend([DESCRIPTION[i], CHARGE[i]])
        x = df.loc[:, c]
        x.columns = NAMES
        c.remove(DESCRIPTION[i])
        c.remove(CHARGE[i])
        lst.append(x)

    df = pd.concat(lst)
    df = df.dropna(subset=['description', 'tracking_number'])
    df = df.sort_values('tracking_number').reset_index().drop(['index'], axis=1)
    df['date'] = pd.to_datetime(df['date'])
    df['parcel_id'] = df.apply(get_parcel_id, axis=1)
    df['parcel_id'] = df['parcel_id'].astype(int)    
    df['weight'] = df['weight'].astype(float)
    df = df[df['real_price'] != 0].reset_index().drop(['index'], axis=1)
    methods = df[df['description'].isin(METHODS)].copy()
    if len(methods) > 0:
        methods['service'] = methods.apply(get_dhl_method, axis=1)
        methods = methods[['tracking_number', 'service']]
        df = df.join(methods.set_index('tracking_number'), on='tracking_number', how='left')
    else:
        df['service'] = ''

    dic = {
        'Dieseltoeslag': 'DIESELTOESL.',
        'Waddentoeslag': 'WADDENTOESLAG',
        'Go Green': 'GO GREEN',
        'Service Point': 'Service point',
        'Xzwaar': 'XZWAAR',
        'Zwaar Pakket': 'ZWAAR PAKKET',
        'Xgroot': 'XGROOT',
        'Afg. Gebied': 'AFG. GEBIED',
        'Xhandling': 'XHANDLING',
        'Weigering Ret': 'WEIGERING RET',
        'Pakket op Pal': 'PAKKET OP PAL', 
        'Xruimte': 'XRUIMTE', 
        'Xruimte -49%': 'XRUIMTE -49%', 
        'Zaterdag': 'ZATERDAG',
        'Douane Afhand': 'DOUANE AFHAND',
        'Pallets': 'PALLETS',
        'Xpallets': 'XPALLETS'
        }

    df['description'] = [dic.get(i) if i in dic.keys() else i for i in list(df['description'])]

    return df        


def get_new_products(df):
    lst = []

    for i in list(df['description'].unique()):
        if i not in TYPES:
            lst.append(i)

    if len(lst) > 0:
        print('the carrier added these products: ', lst)
        df = pd.DataFrame({'new products':[lst]})
    else:
        print('no products added!')
        df = pd.DataFrame({'new products':[['no products added!']]})
    
    return df    


def get_panel_details(df):
    lst = []
    df1 = df[~df['tracking_number'].str.contains('Zendingnr')]
    tn = df1[['tracking_number']]
    tn = tn.dropna().drop_duplicates('tracking_number')
    tn = list(tn['tracking_number'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(panel_data_query.format(DATE_RANGE, 'tracking_number', tu), con=ENGINE)
    df1 = df1.join(pa.set_index('tracking_number'), on='tracking_number', how='left').drop(['parcel_id_sc'], axis=1)
    lst.append(df1)

    df2 = df[df['tracking_number'].str.contains('Zendingnr')].copy()
    if len(df2) > 0:
        tn = df2[['parcel_id']]
        tn = list(tn['parcel_id'])
        tu = tuple(tn[i] for i in range(len(tn)))
        pa = pd.read_sql_query(panel_data_query.format(DATE_RANGE, 'parcel_id', tu), con=ENGINE)
        df2.loc[:, 'parcel_id'] = df2['parcel_id'].astype(str)
        pa = pa.drop(['tracking_number'], axis=1)
        df2 = df2.join(pa.set_index('parcel_id_sc'), on='parcel_id', how='left')
        lst.append(df2)

        df = pd.concat(lst)
        
        return df
    else:
        return df1


def get_parcels_not_created_at_sendcloud(df, sc):  
    sc = sc[sc['description_panel'].isna()]
    df = df[df['Shipment Reference 2'].isin(list(sc['tracking_number']))]

    return df    


def get_direct_parcels_invoiced_by_the_carrier(df):
    df = df[df['contract_type'] == 'direct']
    
    return df


def get_check_valid_surcharge(df):
    if df['description'] == 'Buiten specs':
        if df['measured_length'] > 80 or df['measured_width']  > 60 or df['measured_height'] > 50:
            return True
        else:
            return False
    elif df['description'] == 'XGROOT':
        if df['measured_length'] > 180 or df['measured_width']  > 100 or df['measured_height'] > 50:
            return True
        else:
            return False
    elif df['description'] == 'Buiten sp. BE':
        if df['measured_length'] > 120 or df['measured_width']  > 60 or df['measured_height'] > 50:
            return True
        else:
            return False        
    elif df['description'] == 'ZWAAR PAKKET':
            if df['measured_weight'] > 23:
                return True
            else:
                return False
    else:
        return True    


def get_divide_types(df):
    """
    DHL NL invoice can be separated into four different parts:
        1) shipments
        2) returns
        3) surcharges
        4) parcels not created via sendcloud
    """    
    df[['measured_width', 'measured_height', 'measured_length', 'measured_weight']] = df[['measured_width', 'measured_height', 'measured_length', 'measured_weight']].fillna(0)
    not_created = df[df['description_panel'].isna()]
    df = df[~df['description_panel'].isna()]
    shipments = df[df['description'].isin(PRICES)]
    returns = df[df['description'] == 'Debet']
    surcharges = df[(~df['description'].isin(PRICES)) & (df['description'] != 'Debet') & (df['real_price'] > 0)]

    return shipments, returns, surcharges, not_created   


def get_prices_dhlnl(df):
    buspakje = df[df['description'] == 'Buspakje'].copy()
    df = df[df['description'].isin(PRICES)]
    df['real_price'] = df.groupby('tracking_number')['real_price'].transform(sum)
    df = df[df['description'].isin(METHODS)]
    buspakje = df[df['tracking_number'].isin(list(buspakje['tracking_number']))].copy()
    buspakje['description'] = 'buspakje'
    pa = df[~df['tracking_number'].isin(list(buspakje['tracking_number']))]
    df = pd.concat([buspakje, pa])

    return df    


def get_multi_collo_real_price_dhlnl(df):
    """
    Returns the real_price for multi_collo shipments.
    """
    if df['collo_count'] == 1:
        return df['real_price_panel']
    else:
        return df['real_price_multi_collo']


def get_weight_dhl(df):
    """
    Returns the measured weight by dhl.
    """
    if np.isnan(df.measured_weight_api) == True:
        return df.weight
    else:
        return df.measured_weight_api


def get_description_dhlnl(df):
    """
    Returns the description used by Sendcloud.
    """
    if df['collo_count'] > 1:
        return 'DHL Europlus multi_collo'
    else:
        if df['description'] in (PALLET) and df['shipping_method_id'] == 53:
            return 'DHL Europlus Pallet'
        elif df['description'] == 'buspakje' and df['shipping_method_id'] in (492, 2620, 340, 1543):
            return df['description_panel']
        elif df['description'] in (EUROPLUS) and df['shipping_method_id'] in (16, 1311):
            return df['description_panel']
        elif df['description'] in (PARCEL_CONNECT): 
            if df['shipping_method_id'] in (356, 357, 358, 359):
                if df['weight'] <= 2:
                    return 'DHL Parcel Connect 0-2kg'
                elif df['weight'] <= 5:
                    return 'DHL Parcel Connect 2-5kg'
                elif df['weight'] <= 15:
                    return 'DHL Parcel Connect 5-15kg'
                else:
                    return 'DHL Parcel Connect 15-31.5kg'
            elif df['shipping_method_id'] in (1315, 1316, 1317, 1318):
                if df['weight'] <= 2:
                    return 'DHL Parcel Connect 0-2kg to ParcelShop'
                elif df['weight'] <= 5:
                    return 'DHL Parcel Connect 2-5kg to ParcelShop'
                elif df['weight'] <= 15:
                    return 'DHL Parcel Connect 5-15kg to ParcelShop'
                else:
                    return 'DHL Parcel Connect 15-31.5kg to ParcelShop'       
        elif df['description'] in (DHLFORYOU) and df['shipping_method_id'] in (1602,9,117,115,492,340,1344,81,14,12,15,11,18):
            return df['description_panel']
        elif df['description'] in (EXPRESS) and df['shipping_method_id'] == 38:
            return df['description_panel']
        else:
            return np.nan         


def get_real_price_comparison_dataframe(df):
    """
    Returns a DataFrame where realprice dhl and realprice sendcloud are compared.
    """
    df['description_dhl'] = df.apply(get_description_dhlnl, axis=1)
    df['filter'] = (df['description_dhl'] == df['description_panel'])
    diff = df[df['filter'] == False]
    df = df[df['filter'] == True].copy()
    df['real_price_panel'] = df['real_price_panel'].astype(float)

    gr = pd.DataFrame(df.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_dhl', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_dhl']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_dhl'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_dhl'] = gr['shipments'] * gr['real_price_dhl']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('dif (%)')
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(2)
    
    return gr, diff   


def get_price_check_summary(gr, diff, not_created, returns, surcharges):
    # su_v = surcharges[surcharges['surcharge_valid'] == True]
    # su_iv = surcharges[surcharges['surcharge_valid'] == False]

    df = pd.DataFrame({'gr': [gr['#shipments * real_price_dhl']['total']], 
                       'diff': [diff.real_price.sum()],
                       'not_created': [not_created.real_price.sum()],
                       'returns': [returns.real_price.sum()],
                    #    'surcharges_valid': [su_v.real_price.sum()],
                    #    'surcharges_not_valid': [su_iv.real_price.sum()]}).transpose()
                       'surcharges': [surcharges['real_price'].sum()]}).transpose()
    df.columns = ['totals']
    df = df.append(df.sum().rename('total')).round(2)
    df['distribution'] = df.iloc[:, 0] / df.iloc[-1, -1]

    return df    


def get_invoice_parcel_check(i):
    """
    nc: count of parcels not created at sendcloud 
    cv: sum real price of parcels not created at sendcloud
    dc: count of direct parcels invoiced to sendcloud
    dv: sum real price of direct parcels invoiced to sendcloud
    """
    ts = pd.read_excel(i, sheet_name='ts')
    ts.columns = ['description', 'count', 'value', 'distribution']
    ts = ts[ts['description'].isin(['delivered', 'collected_by_customer'])].distribution.sum().round(4)

    not_created = pd.read_excel(i, sheet_name='nc')
    if len(not_created) == 0:
        nc = len(not_created)
        nv = 0
    else:
        nc = len(not_created['Shipment Reference 2'].unique())
        nv = not_created['Total Amount'].sum()

    dp = pd.read_excel(i, sheet_name='dp')
    if len(dp) == 0:
        dc = len(dp)
        dv = 0
    else:
        dc = len(dp.tracking_number.unique())
        dv = dp.real_price.sum()

    np = pd.read_excel(i, sheet_name='np')
    np = np.iloc[-1,-1]

    na = pd.read_excel(i, sheet_name='nan')
    lst = []

    for i in range(len(na)):
        if na['nan_value'][i] != 0:
            lst.append(na['column'][i])

    if len(lst) == 0:
        na = 'no NaN values'
    else:
        na = lst

    return nc, nv, dc, dv, np, na, ts


def get_invoice_price_check(i):
    df = pd.read_excel(i, sheet_name='price-check')
    pc = df['#shipments * dif'][len(df) -1]
    ca = df['#shipments * real_price_dhl'][len(df) -1]
    su = pd.read_excel(i, sheet_name='surcharges')
    # su_iv = su[su['surcharge_valid'] == False]
    # su_iv =su_iv.drop(['price_panel', 'real_price_panel'], axis=1)

    return pc, su, ca


def get_invoice_summary(pc, nc, nv, dc, dv, np, na, sc, ts, ca):
    """
    Returns a summary of the checked invoice.
    """
    df = pd.DataFrame({'checked amount of carrier invoice': [ca],
                       'diviation in real_price between dhl and sendcloud: ': [pc],
                       'percentage of parcels that are delivered: ': [''],
                       '# parcels not created via sendcloud: ': [nc],
                       'invoice amount parcels not created via sendcloud: ': [nv],
                       '# direct parcels invoiced by carrier: ': [dc],
                       'invoice amount direct parcels invoiced by carrier: ': [dv],
                    #    '# unvalid surcharged parcels: ': [len(su_iv.tracking_number.unique())],
                    #    'invoice amount unvalid surcharges: ': [su_iv.real_price.sum()],
                       'new descriptions added to the invoice: ': np, 
                       '# columns with NaN values: ': [na], 
                       'total invoice value: ': [sc.real_price.sum()]}).transpose()

    df.columns = ['metrics']
    df['relative'] = ''
    df['relative']['checked amount of carrier invoice'] = (df['metrics']['checked amount of carrier invoice'] / sc.real_price.sum()).round(4)
    df['relative']['diviation in real_price between dhl and sendcloud: '] = (df['metrics']['diviation in real_price between dhl and sendcloud: '] / sc.real_price.sum()).round(4)
    df['relative']['invoice amount parcels not created via sendcloud: '] = (df['metrics']['invoice amount parcels not created via sendcloud: '] / sc.real_price.sum()).round(4)
    # df['relative']['invoice amount unvalid surcharges: '] = (df['metrics']['invoice amount unvalid surcharges: '] / su.real_price.sum()).round(4)
    df['relative']['percentage of parcels that are delivered: '] = ts

    return df              


def get_read_in_pallet_prices(pricing):
    df = pd.read_csv(pricing)
    df = df[df['method_friendly_name'] == 'DHL Europlus Pallet']
    df['id'] = df['method_friendly_name'] + df['from_country'] + df['to_country']
    df = df[['id', 'price']]    

    return df


def get_change_description_zwaar_pakket_dhl(df):
    if df['description'] == 'Toeslag gewicht buiten specificaties' and df['service'] == 'europlus':
        return 'Toeslag gewicht buiten specificaties'
    else:
        return df['description']


def get_change_price_toeslag_extra_groot_dhl(df):
    if df['description'] == 'Toeslag extra groot' and df['service'] == 'parcel_connect':
        return 16.5
    else:
        return df['price']


def get_XZWAAR_parcel_connect(df):
    if df['description'] == 'XZWAAR' and df['service'] == 'parcel_connect':
        return 'DHL Europlus Pallet'
    else:
        return df['description']


def get_regular_surcharges_dhlnl(df):
    """
    Returns a DataFrame of the regular dhl nl surcharges.
    """
    df = df[(df['real_price'] > 0) & (df['description'] != 'WEIGERING RET')].copy()
    df['pieces_1'] = [PIECES_DICT.get(i) for i in list(df['description'])]
    df['pieces_1'] = (df['real_price'] / df['pieces_1'])
    df['description'] = df.apply(get_XZWAAR_parcel_connect, axis=1)
    df['description'] = df['description'].replace('PAKKET OP PAL', 'DHL Europlus Pallet')
    df['description'] = df['description'].replace('XRUIMTE', 'DHL Europlus Pallet')
    df['description'] = df['description'].replace('XRUIMTE -49%', 'DHL Europlus Pallet')
    df['description'] = df['description'].replace('PALLETS', 'DHL Europlus Pallet')
    df = df[~df['description_panel'].isna()]
    df['id'] = df['description'] + 'NL' + df['shipping_country']

    pallet_pricing = get_read_in_pallet_prices(PRICING)

    lst = []

    for description in df.description.unique():
        x = df[df['description'] == description].copy()
        if description == 'WADDENTOESLAG':
            x['price'] = x['real_price'] * 1.1
        elif description == 'DHL Europlus Pallet':
            x = x.join(pallet_pricing.set_index('id'), on='id', how='left')
            x['price'] = x['price'] * x['pieces']
        elif description == 'AFG. GEBIED':
            x['price'] = [19 if i <= 50 else i * PRICING_AFG_GEBIED for i in list(x['weight'])]
        else:
            x['price'] = [PRICING_DICT.get(i) for i in list(x['description'])]
            x['price'] = x['price'] * x['pieces_1']

        lst.append(x)

    df = pd.concat(lst)    
 
    df['description'] = [SURCHARGES_NAMES_DICT.get(i) for i in list(df['description'])]
    df['description'] = df.apply(get_change_description_zwaar_pakket_dhl, axis=1)
    df['price'] = df.apply(get_change_price_toeslag_extra_groot_dhl, axis=1)
    df['real_price'] = (df['real_price'] * (1 + FUEL)).round(2)
    df['from_country'] = 'NL'
    df['type'] = 'surcharge'
    df['carrier'] = 'dhl'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['currency'] = 'EUR'
    df = df[MASK]
    
    return df   


def get_surcharge_letterbox(df):
    """
    Returns a DataFrame of the regular dhl nl surcharges.
    """    
    bus = df[df['description'] == 'Buspakje'].copy()
    df = df[(df['description'].isin(METHODS)) & (~df['tracking_number'].isin(list(bus['tracking_number']))) & (df['shipping_method_id'].isin([340, 492, 1543, 2620]))].copy()

    if len(df) > 0:
        df['price'] = 1
        df['real_price'] = 1
        df['carrier'] = 'dhl'
        df['from_country'] = 'NL'
        df['type'] = 'surcharge'
        df['description'] = 'DHLForYou Letterbox buiten specificaties'
        df['currency'] = 'EUR'
        df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
        df = df[MASK]

    return df


def get_vrachtprijs(i):
    """
    Returns the real price invoiced by the carrier.
    """
    return re.findall(pattern='vrachtprijs.*', string=i)


def get_return_surcharges(df):
    """
    Return a DataFrame with the return surcharges of dhl nl.
    """
    df = df[df['tracking_number'].str.contains('Zendingnr')].copy()
    if len(df) > 0:
        df = df.drop(['tracking_number'], axis=1) 
        tn = list(df['parcel_id'])
        tu = tuple(tn[i] for i in range(len(tn)))
        if len(tn) == 1:
            pa = pd.read_sql_query(return_price_query.format('=', tn[0], DATE_RANGE), con=ENGINE)
        else:    
            pa = pd.read_sql_query(return_price_query.format('in', tu, DATE_RANGE), con=ENGINE)
        pa['parcel_id'] =  pa['parcel_id'].astype(str)
        # df = df.join(pa.set_index('parcel_id'), on='parcel_id', how='left')
        df = df.merge(pa, left_on='parcel_id', right_on='parcel_id', how='left')
        df = df.dropna(subset=['tracking_number'])
        df['price'] = df['price'] + RETURN_FEE  
        df['description'] = 'Pakket retour afzender'
        df['carrier'] = 'dhl'
        df['from_country'] = 'NL'
        df['type'] = 'surcharge'
        df['currency'] = 'EUR'
        df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
        df = df[MASK]

    return df

def get_weigering_retour_surcharges(df):
    """
    Returns a DataFrame with the weigering retour surcharges.
    """
    df = df[df['description'] == 'WEIGERING RET'].copy()
    if len(df) > 0:
    #     tn = list(df['tracking_number'])
    #     tu = tuple(tn[i] for i in range(len(tn)))
    #     pa = pd.read_sql_query(weigering_retouren_query.format(tu, DATE_RANGE), con=ENGINE)
    #     # df = df.join(pa.set_index('tracking_number'), on='tracking_number', how='left')
    #     df = df.merge(pa, left_on='tracking_number', right_on='tracking_number', how='left')
    #     df = df.dropna(subset=['tracking_number', 'price'])
        df['price'] = df['price_panel'] + RETURN_FEE  
        df['description'] = 'Pakket retour afzender'
        df['carrier'] = 'dhl'
        df['from_country'] = 'NL'
        df['type'] = 'surcharge'
        df['currency'] = 'EUR'
        df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
        df = df[MASK]

    return df


def get_pivot_table(df):
    df = df.groupby(['description']).agg({'real_price':['count', 'mean', 'sum']})
    df.columns = [ 'count', 'mean', 'sum']
    df = df.sort_values('sum', ascending=False)
    df = df.append(df.sum(numeric_only=True).rename('total')).round(2)
    
    return df     


   