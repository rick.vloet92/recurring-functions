# libraries
import pandas as pd 
import datetime as dt 
from sqlalchemy import create_engine
import numpy as np
from dateutil.relativedelta import relativedelta
from decimal import Decimal


# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
DATE_RANGE = str((dt.date.today() - dt.timedelta(290)))


shipping_status_dic = {
    1000 : 'ready to send',
    1 : 'announced',
    3 : 'to_sorting',
    4 : 'delayed',
    5 : 'sorted',
    6 : 'unsorted',
    7 : 'sorting',
    8 : 'delivery_failed',
    10 : 'delivery_forced',
    11 : 'delivered',
    12 : 'awaiting_customer_pickup',
    13 : 'announced_uncollected',
    15 : 'collect_error',
    18 : 'unsorted2',
    80 : 'undeliverable',
    91 : 'shipment_on_route',
    92 : 'driver_on_route',
    22 : 'picked_up_by_driver',
    93 : 'collected_by_customer',
    999 : 'no_label',
    2000 : 'cancelled',
    1998 : 'cancellation_pending',
    1337 : 'cancelled_in_time',
    2001 : 'submitting_cancellation_request',
    1002 : 'announcedment_failed',
    62990 : 'at_sorting_center',
    1999 : 'cancellation_requested',
    62989 : 'released_by_customs'
}  


# queries
parcels_created_at_sendcloud_query = """
select 
    tracking_number
from
    parcel
where
    tracking_number in {}
and
    announced_at >= '{}'
"""


parcel_data_query = """
select
    p.tracking_number, 
    p.weight "weight_panel", 
    p.shipping_method_id, 
    (case when sm."returns" then sc.iso_2
    	  else rc.iso_2 end) "from_country",
    (case when sm."returns" then rc.iso_2
    	  else sc.iso_2 end) "shipping_country", 
    sm.code,
    p.collo_count,
    p.direction,
    cc."type" "contract_type",
    p.parent_parcel_status_id,
    p.colli_uuid,
    sm."returns"
from 
    parcel p
left join shipping_country sc
	on p.shipping_country_id = sc.shipping_country_id
left join shipping_country rc 
	on p.from_country_id = rc.shipping_country_id
left join shipping_method sm 
	on p.shipping_method_id = sm.shipping_method_id
left join contracts_contract cc 
    on cc.id = p.contract_id
where   
    p.announced_at >= '{}'    
and
    p.tracking_number in {}       
"""


invoice_item_data_query = """
select
    p.tracking_number, 
    ii.description "description_panel", 
    ii.price "price_panel",
    ii.real_price "real_price_panel"
from 
    parcel p
left join invoice_item ii
	on p.parcel_id = ii.parcel_id
where
    ii."type" = 'shipment'
and
    ii.description != 'Reversed cancellation'    
and
    p.announced_at >= '{}'    
and
    p.tracking_number in {} 
"""


# functions
def get_first_day_of_month(i):
    return dt.datetime(i.year, i.month, 1)


def get_nan_values_in_invoice(df):
    df = pd.DataFrame(df.isna().sum()).reset_index()
    df.columns = ['column', 'nan_value']    

    return df     


def get_panel_details(df):
    tn = df[['tracking_number']]
    tn = tn.dropna().drop_duplicates('tracking_number')
    tn = list(df['tracking_number'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(parcel_data_query.format(DATE_RANGE, tu), con=ENGINE)
    df = df.join(pa.set_index('tracking_number'), on='tracking_number', how='left')
    pa = pd.read_sql_query(invoice_item_data_query.format(DATE_RANGE, tu), con=ENGINE)
    df = df.join(pa.set_index('tracking_number'), on='tracking_number', how='left')
    
    return df      


def get_new_products_of_carrier(df, types):
    lst = []

    for i in list(df['description'].unique()):
        if i not in types:
            lst.append(i)

    if len(lst) > 0:
        print('the carrier added these products: ', lst)
        df = pd.DataFrame({'new products':lst})
        return df
    else:
        print('no products added!')
        df = pd.DataFrame({'new products':['no products added!']})
        return df


def get_pivot_table(df, column, agg_column):
    df = pd.DataFrame(df.groupby(column).agg({agg_column:['count', 'mean', sum]}))
    df.columns = ['count', 'mean', 'sum']
    df = df.sort_values('sum', ascending=False)
    df = df.append(df.sum(numeric_only=True).rename('total'))
    
    return df  


def get_internal_note(df):
    return 'invoice_number: ' + df['invoice_number'] + ' surcharge {}'.format(dt.datetime.strftime(df['date'], "%B").lower())


def get_user_segment(i):
    if pd.isna(i) == True:
        return np.nan
    elif i <= 30:
        return 'E'
    elif i <= 80:
        return 'D'
    elif i <= 300:
        return 'C'
    elif i <= 1000:
        return 'B'
    else:
        return 'A'


def split_up_csv_upload_in_parts(df):
    start = 0
    end = 10000

    parts = round(len(df)/ end)

    for i in range(12):
        x = df.iloc[start:end, :]
        x.to_csv(PATH + 'surcharges\\{}-dpd-de-regular-surcharges_part_{}.csv'.format(INVOICE, str(i)), index=False, encoding='utf-8')
        start += 10000
        end += 10000
        print('part {} done!'.format(str(i)))    


def get_parcels_not_created_at_sendcloud(df, pa, column): 
    pa = pa[pa['description_panel'].isna()]
    df = df[df[column].isin(list(pa['tracking_number']))]

    return df        


def get_direct_parcels_invoiced_by_the_carrier(df):
    df = df[df['contract_type'] == 'direct']
    
    return df


def get_nan_values_in_invoice_data(df):
    df = pd.DataFrame(df.isna().sum()).reset_index()
    df.columns = ['column', 'nan_value']    

    return df        


def get_tracking_status_pivot(df):
    df = df[~df['parent_parcel_status_id'].isna()]
    df = pd.DataFrame(df.groupby('parent_parcel_status_id').agg({'tracking_number':pd.Series.nunique, 'real_price':sum}))
    df.columns = ['count', 'value']
    df['distribution'] = df.iloc[:, -1] / df.iloc[:, -1].sum()
    df = df.sort_values('distribution', ascending=False)
    df.index = [shipping_status_dic.get(i) if i in shipping_status_dic.keys() else i for i in df.index]
    df = df.append(df.sum().rename('total'))

    return df    


def get_invoice_distribution_carrier(ts, dis):
    pa = pd.DataFrame({'description': [], 'count': [], 'value': [], 'distribution': []})
    tracking = pd.DataFrame({'description': ['tracking status distribution'], 'count': [np.nan], 'value': [np.nan], 'distribution': [np.nan]})
    pa = pa.append(tracking)
    tra = pd.DataFrame({'description': ts.iloc[:, 0], 'count': ts.iloc[:, 1], 'value': ts.iloc[:, 2], 'distribution': ts.iloc[:, 3]})
    pa = pa.append(tra)
    tracking = pd.DataFrame({'description': ['tracking status distribution'], 'count': [np.nan], 'value': [np.nan], 'distribution': [np.nan]})
    pa = pa.append(tracking)
    tra = pd.DataFrame({'description': dis.iloc[:, 0], 'count': dis.iloc[:, 1], 'value': dis.iloc[:, 2], 'distribution': dis.iloc[:, 3]})
    pa = pa.append(tra).round(2)

    return pa    


def get_change_date(df):
    if (df['date'] - pd.to_datetime(dt.date.today())).days < -30:
        return pd.to_datetime(dt.date.today())
    else:
        return df['date'] 


def get_double_items(df, column):
    lst = []

    for i in df[column].unique():
        dff = df[df[column] == i].copy()
        dff['count_item_multiple_times_invoiced'] = dff.groupby('tracking_number')['tracking_number'].transform(len)
        dff = dff[(dff['count_item_multiple_times_invoiced'] > 1) & (dff['real_price'] > 0)]
        lst.append(dff)

    dd = pd.concat(lst)
    dd = dd.sort_values('tracking_number')

    return dd


def get_pricing_in_right_format(path):
    df = pd.read_csv(path, converters={'to_country': lambda i: str(i), 'from_country': lambda i: str(i)})
    # df = df.iloc[:, :-2]
    df['price'] = [Decimal(i) if pd.isnull(i) == False else i for i in list(df['price'])]
    df['price'] = [round(i, 2) if pd.isnull(i) == False else i for i in list(df['price'])]
    df['real_price'] = [Decimal(i) if pd.isnull(i) == False else i for i in list(df['real_price'])]
    df['real_price'] = [round(i, 2) if pd.isnull(i) == False else i for i in list(df['real_price'])]
    df['weight_allowance'] = [Decimal(i) if pd.isnull(i) == False else i for i in list(df['weight_allowance'])]
    df['real_weight_allowance'] = [Decimal(i) if pd.isnull(i) == False else i for i in list(df['real_weight_allowance'])]

    return df    


def get_testing(path1, start_date, path2, name):
    staff = pd.read_csv(path1).set_index('Row Labels')
    staff.columns = pd.date_range(start_date, periods=12, freq='MS')
    staff = staff.fillna(0)

    fte = pd.read_csv(path2).set_index('Row Labels')
    fte.columns = pd.date_range(start_date, periods=12, freq='MS')
    fte = fte.fillna(0)    

    staff_by_fte = staff.divide(fte)

    des = staff.transpose().describe()

    pa = pd.DataFrame({'index' : staff.index}).set_index('index')

    for i in range(len(staff.columns)):
        if i == 11:
            break
        else:
            pa[staff.columns[i + 1]] = ((staff.iloc[:, i + 1] / staff.iloc[:, i]) - 1).values

    dep = staff[staff.index.isin(DEPARTMENTS)].copy()
    dep = dep.append(dep.sum(numeric_only=True).rename('Total'))

    dis = pd.DataFrame({'index':dep.index})

    for i in range(len(dep.columns)):
        dis[dep.columns[i]] = (dep.iloc[:, i] / dep.iloc[-1, i]).values


    writer = pd.ExcelWriter('C:\\Users\\Rick Vloet\\Downloads\\{}.xlsx'.format(name), engine='xlsxwriter')
    staff_by_fte.to_excel(writer, 'staff_by_fte')
    des.to_excel(writer, 'descriptives')
    pa.to_excel(writer, 'MoM growth')
    dis.to_excel(writer, 'department distribution')
    writer.save() 

    return staff, fte, staff_by_fte, des, pa, dis


def get_invoice_distribution(df):
    df = df.groupby(['invoice_number', 'description']).agg({'real_price': [len, 'mean', sum]}).reset_index()
    df.columns = ['invoice_number', 'description', 'count', 'mean', 'sum']
    df['sum_invoice_number'] = df.groupby('invoice_number')['sum'].transform(sum)
    df['sum_distribution_invoice_number'] = (df.iloc[:, -2] / df.iloc[:, -1])

    return df