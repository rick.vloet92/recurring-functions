# libraries
import pandas as pd 
import datetime as dt 
from sqlalchemy import create_engine
import numpy as np
from dateutil.relativedelta import relativedelta
from decimal import Decimal


# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
DATE_RANGE = str((dt.date.today() - dt.timedelta(180)))



# functions
def get_invoice_data(i):
    df = pd.read_csv(i, 
                     encoding='Latin-1', 
                     delimiter=';', 
                     converters={'LDV': lambda i: str(i[1:]), 
                                 'TARIFFA_CONTRATTO': lambda i: i.replace(',', '.'), 
                                 'IMPORTO_TOTALE': lambda i: i.replace(',', '.'), 
                                 'PESO_REALE': lambda i: i.replace(',', '.'), 
                                 'PESO_VOLUMETRICO': lambda i: i.replace(',', '.'), 
                                 'ALTEZZA_TOT': lambda i: i.replace(',', '.'), 
                                 'LARGHEZZA_TOT': lambda i: i.replace(',', '.'), 
                                 'PROFONDITA_TOT': lambda i: i.replace(',', '.'), 
                                 'DATA_PARTENZA': lambda i: str(i), 
                                 'NUMERO_FATTURA': lambda i: str(i)},
                      low_memory=False)
        
    return df