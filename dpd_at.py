# libraries

import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd
import datetime as dt
from sqlalchemy import create_engine
import numpy as np


# variabels

CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
DATE_RANGE = str((dt.date.today() - dt.timedelta(290)))

NAMES = ['invoice_number', 
         'date', 
         'tracking_number', 
         'description_1', 
         'description_2', 
         'description_3', 
         'description_4', 
         'description_5', 
         'description_6', 
         'description_7', 
         'real_price', 
         'weight', 
         'client_id']


TYPES = ['LMAT', 'SERV+I', 'LMATEU', 'SERV+E', 'IZ-EU', 'ÜB-EU', 'CH-ZLL', 'BE-RE', 'BG', 'CH-RE', 'ÜB-DL', 'NODADL',
         'nan', 'NCZRI', 'EPA-I', 'NCZINL', 'NODAI', 'LQ-I', 'LMEU', 'NCZEU', 'SP',
         'LMATDL', 'NCZREU', 'LMDL', 'NODAEU', 'AT-RE', 'DE-RE', 'AT', 'DE-RÜ', 'SE-RE', 'BG-RE', 'ZPA-I', 'ZPA-EU', 'GB-ABF',
         'B2CR', 'DE', 'FR-RE', 'FR', 'NL', 'SI', 'GB', 'SE', 'LT', 'BE',
         'LU', 'HU', 'IT', 'ES', 'RO', 'PL', 'SK', 'CZ', 'LU-RE', 'LV', 'HR',
         'PT', 'HU-RE', 'DK', 'IE', 'EE', 'CH', 'HF-INL', 'IT-RE', 'ÜB-INL', 'B2CREU', 'NCZRDL', 'ZPA-DL',
         'ES-RE', 'SPZI', 'SERV+D', 'FI', 'SPZEU', 'HF-EU', 'SK-RE', 'EPA-EU', 'NL-RE', 'PL-RE', 'GB-RE', 'NCZDL', 'AT-N9']     


SURCHARGES = ['IZ-EU', 'ZPA-I', 'ZPA-EU', 'GB-ABF', 'ZPA-DL',
              'IZ-DL',
              'ÜB-EU', 
              'ÜB-INL', 'ÜB-DL', 
              'ÜB-DL',
              'NCZINL', 
              'NCZEU',
              'NCZDL',
              'SPZI',
              'SPZEU', 
              'LQ-I', 
              'HF-INL', 
              'HF-EU', 
              'EPA-EU' , 
              'EPA-DL',
              'EPA-I']        


SURCHARGE_NAME_DICT = {
    'ÜB-INL':'Zuschlag Überschreitung der Größen (Inland)',
    'IZ-EU':'Inselzuschlag',
    'IZ-DL':'Inselzuschlag',
    'ÜB-EU':'Zuschlag Überschreitung der Größen (International)',
    'ÜB-INL':'Zuschlag Überschreitung der Größen (AT)',
    'ÜB-DL':'Zuschlag Überschreitung der Größen (Drittland)',
    'NCZINL':'NC- Zuschlag (Nicht bandfähig)',
    'NCZEU':'NC- Zuschlag (Nicht bandfähig)',
    'LQ-I':'Zuschlag Paket Gefahrgut in begrenzter Menge',
    'NCZDL':'NC- Zuschlag (Nicht bandfähig)',
    'SPZI':'Zuschlag Speditionsz',
    'SPZEU':'Zuschlag Speditionsz', 
    'HF-INL':'Handlingfee AT',
    'HF-EU':'Handlingfee AT', 
    'EPA-EU':'Einzel paket Abh.',
    'EPA-DL':'EinzelPkt.Abh. AT',
    'EPA-I':'EinzelPkt.Abh. AT',
    'AT-N9':'Rückholpaket',
    'ZPA-I': 'ZweiPkt.Abh. AT', 
    'ZPA-EU': 'ZweiPkt.Abh. EU', 
    'ZPA-DL': 'ZweiPkt.Abh. DE',
    'GB-ABF': 'GB Abfertigung'
}         


SURCHARGE_PRICE_DICT = {
    'ÜB-INL':17.9,
    'IZ-EU':12.5,
    'IZ-DL':12.5,
    'ÜB-EU':25,
    'ÜB-DL':25,
    'ÜB-DL':25,
    'NCZINL':3,
    'NCZEU':3,
    'LQ-I':3,
    'NCZDL':3,
    'SPZI':25,
    'SPZEU':25, 
    'HF-INL':0.4, 
    'HF-EU':0.4, 
    'EPA-EU':1.75,
    'EPA-DL':1.75,
    'EPA-I':1.75,
    'AT-N9':10,
    'ZPA-I': 3, 
    'ZPA-EU': 3, 
    'GB-ABF': 6.45
}


MASK = ['tracking_number',
        'description',
        'price',
        'real_price', 
        'type', 
        'carrier',
        'from_country', 
        'currency',
        'internal_note']


MASK1 = ['tracking_number',
         'description',
         'price',
         'real_price', 
         'type', 
         'carrier',
         'from_country', 
         'currency',
         'internal_note', 
         'user_id']        


# queries
query_price_return = """
select
    p.tracking_number, 
    price
from 
    parcel p
left join invoice_item ii
    on p.parcel_id = ii.parcel_id
where  
    p.announced_at > '{}' 
and 
    p.tracking_number in {}
and
    ii."type" = 'shipment'
and 
    ii.description != 'Reversed cancellation'    
"""      


get_user_id_based_on_cliend_id_query = """
select
    client_id, 
    user_id
from
    contracts_contract
where
    client_id in {}
and
    is_active    
"""


# functions

def get_invoice_data(path, invoice):  
    df = pd.read_csv(path + invoice, 
                     encoding='Latin-1', 
                     sep=';',
                     low_memory=False).reset_index()

    df['SubDepot'] = [i[1:-1] for i in list(df['SubDepot'])] 
    df['SubDepot'] = [i.strip() for i in list(df['SubDepot'])]
    df['Offertvariante 6'] = df['Offertvariante 6'].fillna('0')
    df['Offertvariante 6'] = [i.replace(',', '.') for i in list(df['Offertvariante 6'])]   
    df['Offertvariante 6'] = df['Offertvariante 6'].astype(float)       

    return df


def get_select_columns(df):
    df = df.iloc[:, [0, 1, 7, 21, 22, 23, 24, 26, 27, 28, 29, 30, 5]].copy()
    df.columns = NAMES
    df['date'] = pd.to_datetime(df['date'].astype(str))
    df['weight'] = df['weight'].fillna('0')
    df['weight'] = [i.replace(',', '.') for i in list(df['weight'])]
    df['weight'] = df['weight'].astype(float)
    df['invoice_number'] = df['invoice_number'].astype(str)

    lst = []

    for c in ['description_{}'.format(str(i)) for i in range(1, 8)]:
        y = df[['tracking_number', c, 'real_price', 'weight', 'date', 'invoice_number', 'client_id']].copy()
        y.columns = ['tracking_number', 'description', 'real_price', 'weight', 'date', 'invoice_number', 'client_id']
        lst.append(y)

    df = pd.concat(lst)
    df = df[~df['description'].isna()].reset_index().drop(['index'], axis=1)
    df['description'] = [i.strip() for i in list(df['description'])]
    
    return df 


def get_new_products(df):
    lst = []

    for i in df.description.unique():
        if i not in (TYPES):
            lst.append(i)

    if lst:
        print('these are the new products: ', lst)
        df = pd.DataFrame({'new products':lst})
    else:
        print('no new products!')
        df = pd.DataFrame({'new products':['no products added!']})

    return df


def get_regular_surcharges(df):
    df = df[(df['description'].isin(SURCHARGES)) & (~df['description_panel'].isna())].copy()
    df['price'] = [SURCHARGE_PRICE_DICT.get(i) for i in list(df['description'])]
    df['real_price'] = df['price']  
    df['description'] = [SURCHARGE_NAME_DICT.get(i) for i in list(df['description'])]
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['type'] = 'surcharge'
    df['from_country'] = 'AT'
    df['carrier'] = 'dpd'   
    df['currency'] = 'EUR'   
    df = df[MASK]
    df = df.sort_values('tracking_number')
    
    return df


def get_return_surcharges(df):
    df = df[(df['description'].str.contains('-RE')) | (df['description'] == 'B2CREU') | (df['description'] == 'DE-RÜ')].copy()
    
    tn = list(df['tracking_number'])
    tu = tuple(tn[i] for i in range(len(tn)))
    
    pa = pd.read_sql_query(query_price_return.format(DATE_RANGE, tu), con=ENGINE)
    df = df.join(pa.set_index('tracking_number'), on='tracking_number', how='left')
    df = df[df['direction'] == 'o']
    
    df['description'] = 'Paket Retoure'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['type'] = 'surcharge'
    df['from_country'] = 'AT'
    df['carrier'] = 'dpd'      
    df['currency'] = 'EUR'
    df = df[MASK]
    df = df.sort_values('tracking_number')
        
    return df    


def get_service_pauschale_surcharges(df):
    df = df[df['description'] == 'SP'].copy()
    df['client_id'] = df['client_id'].astype(int).astype(str)
    tn = list(df['client_id'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(get_user_id_based_on_cliend_id_query.format(tu), con=ENGINE)
    df = df.join(pa.set_index('client_id'), on='client_id', how='left')
    df = df[df['user_id'] != 55839]
    df = df.drop_duplicates('user_id')
    df['price'] = df['real_price']
    df['description'] = 'Servicepauschale'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['type'] = 'surcharge'
    df['from_country'] = 'AT'
    df['carrier'] = 'dpd'   
    df['currency'] = 'EUR'  
    df = df.dropna(subset=['user_id']) 
    df['user_id'] = [int(i) for i in list(df['user_id'])]
    df = df[MASK1]
    
    return df


def get_invoice_check_summary(df, sc, nc, dp, ts, di, np):
    df = pd.DataFrame({'invoice_value': [df.iloc[:, 29].sum()], 
                       'number_of_lines': [len(df)],
                       'number_of_unique_tracking_numbers': [len(sc.tracking_number.unique())],
                       'number_of_new_product_on_invoice': [np['new products']],
                       'parcels_delivered (0-1 %)': [ts[ts.index.isin(['delivered', 'collected_by_customer'])]['distribution'].sum()],
                       'number_of_unmatched_items': [len(nc)],
                       'number_of_direct_shipments_invoiced': [len(dp)],
                       'number_of_shipments_invoiced_multiple_times_unvalid': [len(di.tracking_number.unique())]}).transpose()

    df.columns = ['metric']

    return df    