# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import glob
import datetime as dt 
from sqlalchemy import create_engine


# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
DATE_RANGE = (dt.date.today() - dt.timedelta(120)) 


MASK = ['tracking_number', 
        'description',
        'price', 
        'real_price',
        'type',
        'from_country',
        'carrier', 
        'currency',
        'internal_note']


# functions
def get_invoice_data(path, invoice):
    df = pd.read_csv(path + invoice, 
                     sep=';',
                     converters={'Parcelnumber': lambda i: str(i)},
                     low_memory=False,
                     encoding='Latin-1')

    df['invoice_number'] = invoice[:-4]
    if 'Total amount EUR' in df.columns:
        df['Total amount EUR'] = [i.replace(',', '.') for i in list(df['Total amount EUR'])]
    else:
        df['Total Amount'] = [i.replace(',', '.') for i in list(df['Total Amount'])]

    return df         


def get_select_columns(df):
    if 'Total amount EUR' in df.columns:
        df = df[['Declaration date',
                 'Parcelnumber',
                 'Total amount EUR', 
                 'invoice_number']].copy()
    else:
        df = df[['Declaration date',
                 'Parcelnumber',
                 'Total Amount', 
                 'invoice_number']].copy()
    
    df.columns = ['date', 'tracking_number', 'real_price', 'invoice_number']
    df['real_price'] = df['real_price'].astype(float)
    df['date'] = pd.to_datetime(df['date'])

    return df      


def get_import_costs(df):
    df['date'] = df.apply(general_carrier.get_change_date, axis=1)
    df['price'] = df['real_price']
    df['description'] = 'Import kosten ' + df['shipping_country']
    df['type'] = 'surcharge'
    df['from_country'] = 'NL'
    df['carrier'] = 'dpd'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['currency'] = 'EUR'
    df = df[MASK]
    
    return df    