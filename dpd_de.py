# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import re
import datetime as dt 
from sqlalchemy import create_engine


# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
DATE_RANGE = (dt.date.today() - dt.timedelta(180))


TYPES = ['DPD CLASSIC NP (national)', 'Maut', 'Dieselzuschlag', 'Fehlende oder unzureichende Avisdaten', 'Sicherheitsgebühr', 'DPD CLASSIC KP (national)', 
         'Adresskorrektur Inland', 'systemgeneriertes Paket', 'Retoure (EU)', 'Währungszuschlag', 'DPD CLASSIC NP (Predict NAT)', 'DPD CLASSIC KP (Predict NAT)',
         'DPD CLASSIC KP (Predict EUR)', 'Unzustellbar / AV', 'Fehlerh./ nicht gerout. Pakete(unlesbar)', 'DPD CLASSIC NP (Europa)', 'Adresskorrektur Inland (UNRET)',
         'Retoure (national)', 'PLZ-Fehler Inland', 'DPD PARCELLetter', 'DPD CLASSIC NP (Predict EUR)', 'Unzustellbar / AV (UNRET)', 'DPD CLASSIC KP (Europa)',
         'Adresskorrektur Ausland (UNRET)', '(NC) Non-Conveyable-Maß', 'Fehlende o. unvolls. Exportpapiere/Daten', '(NC) Non-Conveyable-Volumen', 'Adresskorrektur Ausland',
         'Übergröße (Länge)', 'Verzollungsabwicklung', '(NC) Non-Conveyable', 'PLZ-Fehler Ausland', 'DPD Reifen', 'Saisonzuschlag Reifen', 'erneute Zustellversuche', 
         'DPD CLASSIC NP (SHOPZUSTELLUNG NAT)','DPD Reifen (predict)', 'DPD CLASSIC NP (SHOPZUSTELLUNG EU)', 'Speditionszuschlag', 'Inselzuschlag', 'Übergröße (Gurtmaß)',
         'DPD CLASSIC KP (SHOPZUSTELLUNG NAT)', 'Gefahrgut LQ', 'Übergewicht', 'Übergewicht Übergrösse', 'Fehlerh./ nicht gerout. Pakete', 'DPD CLASSIC KP (SHOPZUSTELLUNG EU)', 'DPD 12:00 Paket', 
         'Zuschlag Sondergebiete', 'Rückholpaket Classic', 'vergeblicher Abholversuch Rückholpaket', 'Inselzuschlag Parcelletter', 'Verzollungsabwicklung DAP unverzollt',
         'DPD 10:00 Paket', 'Diesel- und eMobilitätszuschlag', 'DPD EXPRESS PAKET (DPD 18:00)', 'Diesel- und E-Mobilitätszuschlag', 'Kerosinzuschlag', 'DPD EXPRESS PAKET International']


SHIPMENTS = ['DPD CLASSIC KP (Predict EUR)', 'DPD CLASSIC KP (Europa)', 'DPD CLASSIC NP (Predict EUR)', 'DPD CLASSIC NP (national)', 'DPD CLASSIC KP (Predict NAT)', 
             'DPD CLASSIC KP (national)', 'DPD CLASSIC NP (Predict NAT)', 'DPD CLASSIC KP (SHOPZUSTELLUNG EU)', 'DPD PARCELLetter', 'DPD CLASSIC NP (Europa)', 
             'DPD CLASSIC NP (SHOPZUSTELLUNG NAT)','DPD CLASSIC NP (SHOPZUSTELLUNG EU)', 'DPD CLASSIC KP (SHOPZUSTELLUNG NAT)', 'DPD Reifen (predict)',
             'DPD EXPRESS PAKET (DPD 18:00)','DPD CLASSIC PAKET (Guarantee)','DPD Reifen', 'Maut', 'Dieselzuschlag', 'Verzollungsabwicklung', 
             'DPD 12:00 Paket', 'DPD 10:00 Paket', 'DPD EXPRESS PAKET (DPD 18:00)', 'DPD EXPRESS PAKET International', 'Diesel- und E-Mobilitätszuschlag', 'Kerosinzuschlag', 'Währungszuschlag']



SURCHARGES = ['Übergröße (Gurtmaß)', 'Adresskorrektur Inland (UNRET)', 'Adresskorrektur Inland', 'Adresskorrektur Ausland', 'Unzustellbar / AV (UNRET)', 
              '(NC) Non-Conveyable-Maß', '(NC) Non-Conveyable', 'Übergewicht', 'Inselzuschlag', 'Übergröße (Länge)', 'Saisonzuschlag', 'Speditionszuschlag', 
              '(NC) Non-Conveyable-Volumen', 'Übergewicht Übergrösse', 'Adresskorrektur Ausland (UNRET)', 'Unzustellbar / AV', 'Fehlende o. unvolls. Exportpapiere/Daten', 
              'Inselzuschlag Parcelletter', 'erneute Zustellversuche', 'Fehlende oder unzureichende Avisdaten', 'Fehlerh./ nicht gerout. Pakete', 
              'Fehlerh./ nicht gerout. Pakete(unlesbar)', 'Gefahrgut LQ', 'PLZ-Fehler Inland', 'Rückholpaket Classic', 'Sicherheitsgebühr', 'systemgeneriertes Paket', 
              'Zuschlag Sondergebiete', 'Inselzuschlag Parcelletter', 'PLZ-Fehler Ausland', 'Saisonzuschlag Reifen', 'Emergency Situation Zuschlag']


OTHER = ['Maut', 'Dieselzuschlag', 'Verzollungsabwicklung', 'Diesel- und E-Mobilitätszuschlag', 'Kerosinzuschlag', 'Währungszuschlag', 'Verzollungsabwicklung DAP unverzollt']       


KP_PAKKET = ['DPD CLASSIC KP (Predict EUR)', 'DPD CLASSIC KP (Europa)', 'DPD CLASSIC KP (Predict NAT)', 'DPD CLASSIC KP (national)',
             'DPD CLASSIC KP (SHOPZUSTELLUNG EU)', 'DPD CLASSIC KP (SHOPZUSTELLUNG NAT)']


RETURNS = ['Retoure (national)', 'Retoure (EU)']             


COLUMNS = ['INVOICENO', 
           'CONTRACT',
           'PARCELNO', 
           'AMOUNT',  
           'QUANTITY',
           'PRODUCTTEXT',
           'WEIGHT', 
           'ISCANDATE', 
           'DIMHEIGHT', 
           'DIMWIDTH',
           'DIMLENGTH']


NAMES = ['invoice_number', 
         'contract', 
         'tracking_number', 
         'real_price',
         'quantity', 
         'description', 
         'weight', 
         'date', 
         'height', 
         'width', 
         'length']


MASK = ['tracking_number', 
        'description', 
        'price', 
        'real_price', 
        'type', 
        'from_country', 
        'carrier', 
        'currency',
        'internal_note']


NP_PARCEL = ['DPD CLASSIC NP (Predict EUR)',
             'DPD CLASSIC NP (Predict NAT)',
             'DPD CLASSIC NP (national)',
             'DPD CLASSIC NP (Europa)', 
             'DPD CLASSIC KP (national)',
             'DPD CLASSIC KP (Europa)',
             'DPD CLASSIC KP (Predict NAT)',
             'DPD CLASSIC KP (Predict EUR)']


NP_SPARCEL = ['DPD CLASSIC NP (SHOPZUSTELLUNG NAT)',
              'DPD CLASSIC NP (SHOPZUSTELLUNG EU)', 
              'DPD CLASSIC KP (SHOPZUSTELLUNG EU)',
              'DPD CLASSIC KP (SHOPZUSTELLUNG NAT)']        


DESCRIPTION_DICT = {
    'Übergröße (Gurtmaß)':'Zuschlag Übergröße (Gurtmaß)', 
    'Adresskorrektur Inland (UNRET)':'Zuschlag Adresskorrektur Inland (UNRET)', 
    'Adresskorrektur Inland':'Zuschlag Adresskorrektur Inland', 
    'Adresskorrektur Ausland':'Zuschlag Adresskorrektur Ausland', 
    'Unzustellbar / AV (UNRET)':'Zuschlag Unzustellbar / AV (UNRET)', 
    '(NC) Non-Conveyable-Maß':'Zuschlag (NC) Non-Conveyable-Maß', 
    '(NC) Non-Conveyable':'Zuschlag (NC) Non-Conveyable', 
    'Übergewicht':'Zuschlag Übergewicht', 
    'Inselzuschlag':'Inselzuschlag', 
    'Übergröße (Länge)':'Zuschlag Übergröße (Länge)', 
    'Saisonzuschlag':'Saisonzuschlag', 
    'Speditionszuschlag':'Speditionszuschlag', 
    '(NC) Non-Conveyable-Volumen':'Zuschlag (NC) Non-Conveyable-Volumen', 
    'Übergewicht Übergrösse':'Zuschlag Übergewicht Übergrösse', 
    'Adresskorrektur Ausland (UNRET)':'Zuschlag Adresskorrektur Ausland (UNRET)', 
    'Unzustellbar / AV':'Zuschlag Unzustellbar / AV', 
    'Fehlende o. unvolls. Exportpapiere/Daten':'Zuschlag Fehlende o. unvolls. Exportpapiere/Daten', 
    'Inselzuschlag Parcelletter':'Inselzuschlag Parcelletter', 
    'erneute Zustellversuche':'Zuschlag erneute Zustellversuche', 
    'Fehlende oder unzureichende Avisdaten':'Zuschlag Fehlende oder unzureichende Avisdaten', 
    'Fehlerh./ nicht gerout. Pakete':'Zuschalg Fehlerh./ nicht gerout. Pakete', 
    'Fehlerh./ nicht gerout. Pakete(unlesbar)':'Zuschlag Fehlerh./ nicht gerout. Pakete(unlesbar)', 
    'Gefahrgut LQ':'Zuschlag Gefahrgut LQ', 
    'PLZ-Fehler Inland':'Zuschlag PLZ-Fehler Inland', 
    'Rückholpaket Classic':'Zuschlag Rückholpaket Classic', 
    'Sicherheitsgebühr':'Zuschlag Sicherheitsgebühr', 
    'systemgeneriertes Paket':'Zuschlag systemgeneriertes Paket', 
    'Zuschlag Sondergebiete':'Zuschlag Sondergebiete',
    'Inselzuschlag Parcelletter':'Inselzuschlag',
    'Emergency Situation Zuschlag': 'Emergency Situation Zuschlag',
    'PLZ-Fehler Ausland': 'PLZ-Fehler Ausland',
    'Saisonzuschlag Reifen': 'Saisonzuschlag'
}       


# queries
parcels_created_at_sendcloud_query = """
select 
    tracking_number
from 
    parcel
where
    tracking_number in {}
and 
    announced_at >= '{}'
"""


direct_parcels_created_at_sendcloud_query = """
select 
    p.tracking_number,
    p.announced_at
from 
    parcel p
    left join contracts_contract cc 
        on cc.id = p.contract_id
where 
    p.tracking_number in {}
and 
    cc."type" = 'direct'
and
    p.cancellation_status = 0
"""


# functions
def get_invoice_data(path, invoice):
    df = pd.read_csv(path + invoice + '.csv', 
                     converters={'PARCELNO': lambda i: str(i), 
                                 'AMOUNT': lambda i: i.replace(',', '.'), 
                                 'DIMHEIGHT': lambda i: i.replace(',', '.'), 
                                 'DIMWIDTH': lambda i: i.replace(',', '.'), 
                                 'DIMLENGTH': lambda i: i.replace(',', '.'), 
                                 'WEIGHT': lambda i: i.replace(',', '.'), 
                                 'INVOICENO': lambda i: str(i), 
                                 'CONTRACT': lambda i: str(i)}, 
                     encoding='Latin-1', 
                     sep=';', 
                     index_col=False, 
                     low_memory=False)
        
    return df   


def get_invoice_data(i):
    df = pd.read_csv(i, 
                     converters={'PARCELNO': lambda i: str(i), 
                                 'AMOUNT': lambda i: i.replace(',', '.'), 
                                 'DIMHEIGHT': lambda i: i.replace(',', '.'), 
                                 'DIMWIDTH': lambda i: i.replace(',', '.'), 
                                 'DIMLENGTH': lambda i: i.replace(',', '.'), 
                                 'WEIGHT': lambda i: i.replace(',', '.'), 
                                 'INVOICENO': lambda i: str(i), 
                                 'CONTRACT': lambda i: str(i)}, 
                     encoding='Latin-1', 
                     sep=';', 
                     index_col=False, 
                     low_memory=False)
        
    return df      


def get_select_columns(df): 
    df = df[COLUMNS].copy()
    df.columns = NAMES    
    df['real_price'] = [float(i) for i in list(df.real_price)]
    df['height'] = [round(float(i)) for i in list(df.height)]
    df['width'] = [round(float(i)) for i in list(df.width)]
    df['length'] = [round(float(i)) for i in list (df.length)]
    df['weight'] = [float(i) for i in list (df.weight)]  
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)
    df = df[df['real_price'] != 0].reset_index().drop(['index'], axis=1)
    
    return df   


def get_new_products_of_carrier(df):
    lst = []

    for i in list(df['description'].unique()):
        if i not in TYPES:
            lst.append(i)

    if len(lst) > 0:
        print('the carrier added these products: ', lst)
        df = pd.DataFrame({'new products':lst})
    else:
        print('no products added!')
        df = pd.DataFrame({'new products':['no products added!']})

    return df     


def get_items_double_invoiced(df):
    lst = []
    x = 0

    for i in range(len(df)):
        if i == 0:
            lst.append(x)
        elif (df['tracking_number'][i] == df['tracking_number'][i - 1]):
            if (df['description'][i] == df['description'][i - 1]):
                x = x + 1
                lst.append(x)
                lst[i-1] = x
            else:
                lst.append(x)
        else:
            x = 0
            lst.append(x)

    return lst


def get_double_items(df):
    df = df.sort_values(['tracking_number', 'description']).reset_index().drop(['index'], axis=1)
    df['count'] = get_items_double_invoiced(df)
    df = df[df['count'] > 0]

    return df


def get_divide_types(df):
    not_created = df[df['description_panel'].isna()].copy()
    df = df[~df['description_panel'].isna()]
    returns = df[df['description'].isin(RETURNS)].copy()
    surcharges = df[df['description'].isin(SURCHARGES)].copy()
    surcharges['description'] = [DESCRIPTION_DICT.get(i) for i in list(surcharges['description'])]
    shipments = df[(~df['description'].isin(RETURNS)) & (~df['description'].isin(SURCHARGES))].copy()
    
    return shipments, returns, surcharges, not_created           


def get_real_price(shipments, returns):
    df = pd.concat([shipments, returns])
    df['real_price'] = df.groupby('tracking_number')['real_price'].transform(sum)
    pr = df[~df['description'].isin(OTHER)].drop_duplicates(['tracking_number'])

    extra = df[~df['tracking_number'].isin(list(pr['tracking_number']))]
    
    return pr, extra


def get_change_description_dpdde(df):
    if df['shipping_method_id'] in (111, 112, 113, 114) and df['description'] not in KP_PAKKET:
        if df['weight'] < 5:
            return 'DPD Classic 0-5kg'
        elif df['weight'] < 10:
            return 'DPD Classic 5-10kg'
        elif df['weight'] < 20:
            return 'DPD Classic 10-20kg'
        else:
            return 'DPD Classic 20-30kg'
    elif df['shipping_method_id'] == 85 and df['description'] in KP_PAKKET:
        return 'DPD Classic KP'
    elif df['shipping_method_id'] == 78:
        return 'DPD Parcelletter'

    elif df['shipping_method_id'] in (302, 303, 304):
        if df['weight'] < 3:
            return 'DPD Classic KP (Service point)'
        elif df['weight'] < 5:
            return 'DPD Classic 0-5kg (Service point)'
        elif df['weight'] < 10:
            return 'DPD Classic 5-10kg (Service point)'
        else:
            return 'DPD Classic 10-20kg (Service point)'
    elif df['shipping_method_id'] == 305:
        if df['weight'] < 3:
            return 'DPD Classic KP (Service point)'
        elif df['weight'] < 5:
            return 'DPD Classic 0-5kg (Service point)'
        elif df['weight'] < 10:
            return 'DPD Classic 5-10kg (Service point)'
        else:
            return 'DPD Classic 10-20kg (Service point)'
    elif df['shipping_method_id'] == 353:
        return 'shipment: DPD return 0-10Kg'
    elif df['shipping_method_id'] == 355:
        return 'shipment: DPD Return 0-20kg' 
    elif df['shipping_method_id'] == 1519:
        return 'shipment: DPD Classic 10-20kg'
    elif df['shipping_method_id'] == 1556:
        return 'shipment: DPD Classic 0-5kg'
    elif df['shipping_method_id'] == 2567:
        return 'shipment: DPD Return 10-20kg'    
    elif df['shipping_method_id'] in (2562, 2563, 2564, 2565, 2566):
        if df['weight'] < 2:
            return 'shipment: DPD Return 0-2kg'
        elif df['weight'] < 4:
            return 'shipment: DPD Return 2-4kg'
        elif df['weight'] < 6:
            return 'shipment: DPD Return 4-6kg'
        elif df['weight'] < 8:
            return 'shipment: DPD Return 6-8kg'       
        else:
            return 'shipment: DPD Return 8-10kg'
    else:
        return np.nan


def get_real_price_comparison_df(df):
    df = df.copy()
    df['description_dpd'] = df.apply(get_change_description_dpdde, axis=1)
    df['filter'] = (df['description_dpd'] == df['description_panel'])
    diff = df[df['filter'] == False]
    sa = df[df['filter'] == True]
    gr = pd.DataFrame(sa.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_dpdde', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_dpdde']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_dpdde'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_dpdde'] = gr['shipments'] * gr['real_price_dpdde']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('#shipments * dif', ascending=False)
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(2)
    
    return gr, diff


def get_returns_dpdde(df):
    df = pd.DataFrame(df.groupby('direction').agg({'tracking_number':len, 'real_price': ['mean', sum]})).reset_index()
    # df.columns = ['count', 'mean', 'sum']
    # df = df.append(df.sum(numeric_only=True).rename('total'))
    
    return df                


def get_price_check_summary(gr, diff, not_created, surcharges, extra, sc):
    gr = pd.DataFrame({'description': ['shipping method sendcloud == shipping method carrier'], 'count': [gr.iloc[:-1, 0].sum()], 'value': [gr.iloc[:-1, 6].sum()]}).set_index('description')

    diff = diff.copy()
    diff['description'] = 'shipping method sendcloud != shipping method carrier'
    diff = pd.DataFrame(diff.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    diff.columns = ['count', 'value']

    not_created = not_created.copy()
    not_created['description'] = 'parcel not created at sendcloud'
    not_created = pd.DataFrame(not_created.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    not_created.columns = ['count', 'value']

    surcharges = pd.DataFrame(surcharges.groupby('description').agg({'tracking_number':len, 'real_price':sum})).copy()
    surcharges.columns = ['count', 'value']

    extra = extra.copy()
    extra['description'] = 'no shipment invoiced'
    extra = pd.DataFrame(extra.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    extra.columns = ['count', 'value']

    df = pd.concat([gr, diff, not_created, extra, surcharges])
    df = df.sort_values('value', ascending=False)
    df = df.append(df.sum().rename('total')).round(2)
    df['distribution'] = df.iloc[:, -1] / sc.real_price.sum()

    return df


def get_invoice_parcel_check(i):
    ts = pd.read_excel(i, sheet_name='ts')
    ts.columns = ['description', 'count', 'value', 'distribution']
    ts = ts[ts['description'].isin(['delivered', 'collected_by_customer'])].distribution.sum().round(4)

    dn = pd.read_excel(i, sheet_name='di').real_price.sum()

    not_created = pd.read_excel(i, sheet_name='nc')

    if len(not_created) == 0:
        nc = len(not_created)
        nv = 0
    else:
        nc = len(not_created['Pakketnummer'].unique())
        nv = not_created.copy()
        nv['Totaal'] = [i.replace(',', '.') for i in list(nv['Totaal'])]
        nv = nv['Totaal'].astype(float).sum()

    dp = pd.read_excel(i, sheet_name='dp')
    if len(dp) == 0:
        dc = len(dp)
        dv = 0
    else:
        dc = len(dp.tracking_number.unique())
        dv = dp.real_price.sum()

    np = pd.read_excel(i, sheet_name='np')
    np = np.iloc[-1,-1]

    na = pd.read_excel(i, sheet_name='nan')
    lst = []

    for i in range(len(na)):
        if na['nan_value'][i] != 0:
            lst.append(na['column'][i])

    if len(lst) == 0:
        na = 'no NaN values'
    else:
        na = lst

    return ts, nc, nv, dc, dv, np, na, dn


def get_invoice_price_check(i):
    dis = pd.read_excel(i, sheet_name='summary')
    df = pd.read_excel(i, sheet_name='price-check')
    di = df['#shipments * dif'][len(df) -1]
    ca = df['#shipments * real_price_dpdde'][len(df) -1]

    return dis, di, ca


def get_invoice_summary(di, ca, nc, nv, dc, dv, np, na, sc, ts, dn):
    df = pd.DataFrame({'checked amount of carrier invoice': [ca],
                       'deviation in real_price between carrier and sendcloud: ': [di],
                       'percentage of parcels that are delivered: ': [''],
                       '# parcels not created via sendcloud: ': [nc],
                       'invoice amount parcels not created via sendcloud: ': [nv],
                       '# direct parcels invoiced by carrier: ': [dc],
                       'invoice amount direct parcels invoiced by carrier: ': [dv],
                       'new descriptions added to the invoice: ': np, 
                       '# columns with NaN values: ': [na], 
                       'amount double invoiced: ': [dn],
                       'total invoice value: ': [sc.real_price.sum()]}).transpose()

    df.columns = ['metrics']
    df['relative'] = ''
    df['relative']['checked amount of carrier invoice'] = (df['metrics']['checked amount of carrier invoice'] / sc.real_price.sum()).round(4)
    df['relative']['deviation in real_price between carrier and sendcloud: '] = (df['metrics']['deviation in real_price between carrier and sendcloud: '] / ca).round(4)
    df['relative']['invoice amount parcels not created via sendcloud: '] = (df['metrics']['invoice amount parcels not created via sendcloud: '] / sc.real_price.sum()).round(4)
    df['relative']['percentage of parcels that are delivered: '] = ts

    return df


def get_regular_surcharges(df):
    df = df[~df['description_panel'].isna()].copy()
    df['currency'] = 'EUR'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['description'] = [DESCRIPTION_DICT.get(i) if i in DESCRIPTION_DICT.keys() else i for i in list(df['description'])]
    df['price'] = df['real_price']
    df['type'] = 'surcharge'
    df['carrier'] = 'dpd'
    df = df[df['price'] > 0]
    df = df[MASK]
        
    return df    