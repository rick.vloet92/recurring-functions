# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import re
import datetime as dt 
from sqlalchemy import create_engine
import glob
from dateutil.relativedelta import relativedelta


# notes
"""

"""


# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
DATE_RANGE = str((dt.date.today() - dt.timedelta(290)))
MONTH = dt.datetime.strftime((dt.date.today().replace(day=1) - dt.timedelta(1)), "%B")
PRICING_FILE = 'C:\\Users\\Rick Vloet\\Documents\\carrier-pricing\\pricing_bpost.csv'


TYPES = ['Automatic 2nd Presentation', 
         'BPACK 24/7', 
         'BPACK 24H PRO', 
         'BPACK EASY RETOUR',
         'BPACK WORLD BUSINESS',
         'BPACK WORLD RETURN',
         'BPACK@BPOST PRO',
         'Collect on Demand Flex Collect ',
         'Fuel Tax',
         'Insurance',
         'Oversize',
         'Overweight',
         'Saturday Delivery',
         'Signature',
         'Supplement for drop in retail netwo', 
         'Supplement illegible Shipping Label', 
         'Supplement Back To Sender', 
         'Supplement for Announcement Data Mi', 
         'Supplement Manual Handling', 
         'Supplement Parcel Density', 
         'Supplement Manual Handling (cylindr', 
         'Supplement Manual Handling (no', 
         'Temporarily crisis contribution', 
         'Supplement Manual Handling (dimensi', 
         'Supplement for drop in retail network', 
         'Supplement Manual Handling (dimension > 100 cm)', 
         'Supplement Manual Handling (cylindrical shape)',
         'Supplement for Announcement Data Missing', 
         'Supplement Manual Handling (non-conform packaging)']


PRICES = ['BPACK 24H PRO',
          'BPACK 24/7', 
          'Signature', 
          'BPACK@BPOST PRO', 
          'BPACK EASY RETOUR',
          'Automatic 2nd Presentation',
          'BPACK WORLD BUSINESS',
          'Fuel Tax',
          'BPACK WORLD RETURN', 
          'Saturday Delivery']         


METHODS = ['BPACK 24H PRO',
           'BPACK@BPOST PRO',
           'BPACK 24/7',
           'BPACK WORLD BUSINESS',
           'BPACK EASY RETOUR', 
           'BPACK WORLD RETURN']


COLUMNS = ['BARCODE',
           'PRODUCT',
           'DESCRIPTION',
           'QUANTITY',
           'SALES EXCL. VAT',
           'WEIGHT',
           'DATE', 
           'INVOICE_NUMBER']


NAMES = ['tracking_number',
         'product', 
         'description', 
         'count',
         'real_price', 
         'weight',
         'date',
         'invoice_number']


MASK = ['tracking_number', 
        'description',
        'price',
        'real_price',
        'type', 
        'from_country', 
        'carrier', 
        'currency',
        'internal_note']         


DESCRIPTION_DICT = {
    'BPACK EASY RETOUR':'BPACK RETOUR',
    'BPACK WORLD RETURN':'BPACK RETOUR',
    'BPACK 24H PRO':'BPACK 24H PRO',
    'BPACK 24/7':'BPACK@BPOST PRO',
    'BPACK@BPOST PRO':'BPACK@BPOST PRO',
    'BPACK WORLD BUSINESS':'BPACK WORLD BUSINESS'
}


SURCHARGES_NAMES_DICT = {
    'Supplement illegible Shipping Label': 'Toeslag onleesbaar verzendetiket', 
    'Supplement for Announcement Data Missing': 'Toeslag onleesbaar verzendetiket', 
    'Supplement Back To Sender': 'Toeslag terug naar afzender', 
    'Supplement Manual Handling (dimension > 100 cm)': 'Toeslag manuele verwerking (dimension > 100 cm)',
    'Supplement Manual Handling (cylindrical shape)': 'Toeslag manuele verwerking (cylindrical shape)',
    'Supplement Manual Handling (non-conform packaging)': 'Toeslag manuele verwerking (non-conform packaging)',
    'Supplement Parcel Density': 'Denstiteitstoeslag',
    'Supplement for Peak Period': 'Piektoeslag', 
    'Overweight': 'Toeslag overgrootte gewicht',
    'Oversize': 'Toeslag overgrootte lengte'
}


DESCRIPTION_DICT = {
    'bpost:athome-bpack24hpro/kg=0-10':'0-10kg',
    'bpost:athome-bpack24hpro/kg=10-30':'10-30kg',
    'bpost:international-bpackworldbusiness/kg=0-2':'0-2kg',
    'bpost:international-bpackworldbusiness/kg=2-10':'2-10kg',
    'bpost:international-bpackworldbusiness/kg=10-20':'10-20kg',
    'bpost:international-bpackworldbusiness/kg=20-30':'20-30kg',
    'bpost:atbpost-bpack/kg=0-10':'0-10kg',
    'bpost:atbpost-bpack/kg=10-30':'10-30kg',
    'bpost:athome-bpack24hpro/signature,kg=0-10':'0-10kg',
    'bpost:athome-bpack24hpro/signature,kg=10-30':'10-30kg',
    'bpost:bpackeasyretour/kg=0-10,return':'0-10kg',
    'bpost:bpackeasyretour/kg=10-30,return':'10-30kg',
    'bpost:athome-bpack24hpro/kg=0-10,saturday':'0-10kg',
    'bpost:athome-bpack24hpro/kg=10-30,saturday':'10-30kg',
    'bpost:athome-bpack24hpro/2ndpresentation,kg=0-10':'0-10kg',
    'bpost:athome-bpack24hpro/2ndpresentation,kg=10-30':'10-30kg'
}


# queries

panel_data_query = """
select
    p.tracking_number, 
    ii.description "description_panel", 
    ii.real_price "real_price_panel", 
    p.weight "weight_panel", 
    p.shipping_method_id, 
    (case when direction = 'i' then sc.iso_2
          else fc.iso_2 end) "from_country",
    (case when direction = 'i' then fc.iso_2
          else sc.iso_2 end) "shipping_country", 
    p.direction,
    sm.code
from 
    parcel p
left join invoice_item ii
    on p.parcel_id = ii.parcel_id
left join contracts_contract cc 
    on cc.id = p.contract_id
left join shipping_country sc
    on p.shipping_country_id = sc.shipping_country_id
left join shipping_country fc
    on p.from_country_id = fc.shipping_country_id
left join shipping_method sm
    on sm.shipping_method_id = p.shipping_method_id
where
    ii.carrier_id = 4
and
    ii."type" = 'shipment'
and
    ii.description != 'Reversed cancellation'
and    
   cc."type" != 'direct' 
and
    p.tracking_number in {}
and
    p.announced_at >= '{}'
"""


parcels_created_at_sendcloud_query = """
select 
	p.tracking_number
from 
	parcel p
where  
	p.cancellation_status = 0
and
    p.tracking_number in {} 
and
    p.announced_at >= '{}'    
"""


bpost_retail_surcharge_query = """
select
    *
from
    invoice_item
where
    carrier_id = 4
and 
    "date" >= '{}'
and 
    "date" < '{}'
and
    description = 'bpost retail surcharge'
"""


# functions
def get_invoice_data(i):
    df = pd.read_csv(i,
                     encoding='Latin-1',
                     converters={'BARCODE': lambda i: str(i[1:]), 
                                 'WEIGHT': lambda i: i.replace(',', '.'),
                                 'INVOICE_NUMBER': lambda i: str(i)}, 
                     low_memory=False)

    df = df[df['CONTRACT'] == 115885].reset_index().drop(['index'], axis=1)
    # df.columns = ['DATE', 'CONTRACT', 'BARCODE', 'PRODUCT', 'QUANTITY',
    #    'SALES EXCL. VAT', 'DESCRIPTION', 'CUSTOMER REF', 'WEIGHT', 'ADDRESSEE',
    #    'ADDRESS', 'ZIPCODE', 'COUNTRY', 'INVOICE_NUMBER']

    return df  


def get_select_columns(df): 
    df = df.reset_index()
    df = df[COLUMNS]
    df.columns = NAMES 
    df['product'] = [df['description'][i] if df['product'][i] not in PRICES else df['product'][i] for i in range(len(df))]
    df[['real_price', 'weight']] = df[['real_price', 'weight']].astype(float)
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)   
    df = df[df['real_price'] != 0]
    
    return df   


def get_new_products_of_carrier(df):
    lst = []

    for i in list(df['product'].unique()):
        if i not in TYPES:
            lst.append(i)

    if len(lst) > 0:
        print('the carrier added these products: ', lst)
        df = pd.DataFrame({'new products':[lst]})
    else:
        print('no products added!')
        df = pd.DataFrame({'new products':[['no products added!']]})
    
    return df    


def get_divide_types_bpost(df):  
    not_created = df[df['description_panel'].isna()]
    df = df[~df['description_panel'].isna()]
    shipments = df[df['product'].isin(PRICES)]
    surcharges = df[(~df['product'].isin(PRICES)) & (df['description'] != 'Supplement for drop in retail network')]
    other = df[df['description'] == 'Supplement for drop in retail network']

    return shipments, surcharges, not_created, other    


def get_calculate_shipping_prices(df):
    df['real_price'] = df.groupby('tracking_number')['real_price'].transform(sum)
    df = df[df['product'].isin(METHODS)]
    df = df.drop_duplicates('tracking_number')
    
    return df      


def get_change_description_bpost(df):
    """
    Function that returns the description used by Sendcloud.
    """
    if df['shipping_method_id'] in (54, 59):
        if df['weight'] <= 10:
            return 'bpost @home 0-10kg'
        else:
            return 'bpost @home 10-30kg'
    elif df['shipping_method_id'] in (360, 361, 362, 363):
        if df['weight'] <= 2:
            return 'bpack World Business 0-2kg'
        elif df['weight'] <= 10:
            return 'bpack World Business 2-10kg'
        elif df['weight'] <= 20:
            return 'bpack World Business 10-20kg'
        else:
            return 'bpack World Business 20-30kg'
    elif df['shipping_method_id'] in (95, 96):
        if df['weight'] <= 10:
            return 'bpost @bpack 0-10kg'
        else:
            return 'bpost @bpack 10-30kg'
    elif df['shipping_method_id'] in (56, 61):
        if df['weight'] <= 10:
            return 'bpost @home 0-10kg Signature on delivery'
        else:
            return 'bpost @home 10-30kg Signature on delivery'
    elif df['shipping_method_id'] in (1520, 1559):
        if df['weight'] <= 10:
            return 'shipment: bpack Easy Retour 0-10kg'
        else:
            return 'shipment: bpack Easy Retour 10-30kg'
    elif df['shipping_method_id'] in (83, 82):
        if df['weight'] <= 10:
            return 'bpost @home 24h 0-10kg Saturday delivery'
        else:
            return 'bpost @home 24h 10-30kg Saturday delivery'
    elif df['shipping_method_id'] in (57, 62):
        if df['weight'] <= 10:
            return 'bpost @home 0-10kg Signature on delivery + 2nd attempt'
        else:
            return 'bpost @home 10-30kg signature on delivery + 2nd attempt'
    elif df['shipping_method_id'] in (1590, 1567, 1573, 1596):
        if df['weight'] <= 2:
            return 'shipment: bpack World Easy Retour 0-2kg'
        elif df['weight'] <= 10:
            return 'shipment: bpack World Easy Retour 2-10kg'
        elif df['weight'] <= 20:
            return 'shipment: bpack World Easy Retour 10-20kg'
        else:
            return 'shipment: bpack World Easy Retour 20-30kg'
    else:
        return 'XXX'    


def get_change_description_code_bpost(df):
    """
    Function that returns the description used by Sendcloud.
    """
    if df['shipping_method_id'] in (54, 59):
        if df['weight'] <= 10:
            return 'bpost:athome-bpack24hpro/kg=0-10'
        else:
            return 'bpost:athome-bpack24hpro/kg=10-30'
    elif df['shipping_method_id'] in (360, 361, 362, 363):
        if df['weight'] <= 2:
            return 'bpost:international-bpackworldbusiness/kg=0-2'
        elif df['weight'] <= 10:
            return 'bpost:international-bpackworldbusiness/kg=2-10'
        elif df['weight'] <= 20:
            return 'bpost:international-bpackworldbusiness/kg=10-20'
        else:
            return 'bpost:international-bpackworldbusiness/kg=20-30'
    elif df['shipping_method_id'] in (95, 96):
        if df['weight'] <= 10:
            return 'bpost:atbpost-bpack/kg=0-10'
        else:
            return 'bpost:atbpost-bpack/kg=10-30'
    elif df['shipping_method_id'] in (56, 61):
        if df['weight'] <= 10:
            return 'bpost:athome-bpack24hpro/signature,kg=0-10'
        else:
            return 'bpost:athome-bpack24hpro/signature,kg=10-30'
    elif df['shipping_method_id'] in (1520, 1559):
        if df['weight'] <= 10:
            return 'bpost:bpackeasyretour/kg=0-10,return'
        else:
            return 'bpost:bpackeasyretour/kg=10-30,return'
    elif df['shipping_method_id'] in (83, 82):
        if df['weight'] <= 10:
            return 'bpost:athome-bpack24hpro/kg=0-10,saturday'
        else:
            return 'bpost:athome-bpack24hpro/kg=10-30,saturday'
    elif df['shipping_method_id'] in (57, 62):
        if df['weight'] <= 10:
            return 'bpost:athome-bpack24hpro/2ndpresentation,kg=0-10'
        else:
            return 'bpost:athome-bpack24hpro/2ndpresentation,kg=10-30'
    elif df['shipping_method_id'] in (1590, 1567, 1573, 1596):
        if df['weight'] <= 2:
            return 'bpost:international-bpackworldeasyretour/kg=0-2,return'
        elif df['weight'] <= 10:
            return '	bpost:international-bpackworldeasyretour/kg=2-10,return'
        elif df['weight'] <= 20:
            return 'bpost:international-bpackworldeasyretour/kg=10-20,return'
        else:
            return 'bpost:international-bpackworldeasyretour/kg=20-30,return'
    else:
        return 'XXX'         


def get_real_price_comparison_df(df):
    df['description'] = df.apply(get_change_description_bpost, axis=1)
    df['filter'] = (df['description'] == df['description_panel'])

    diff = df[df['filter'] == False]
    df = df[df['filter'] == True]

    gr = pd.DataFrame(df.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_bpost', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_bpost']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_bpost'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_bpost'] = gr['shipments'] * gr['real_price_bpost']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('#shipments * dif')
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(2)
    
    return gr, diff    


def get_price_check_summary(gr, diff, not_created, surcharges, other):
    df = pd.DataFrame({'gr': [gr['#shipments * real_price_bpost']['total']], 
                       'diff': [diff.real_price.sum()],
                       'not_created': [not_created.real_price.sum()],
                       'other': [other.real_price.sum()],
                       'surcharges': [surcharges.real_price.sum()]}).transpose()
    df.index = ['shipping method sendcloud == shipping method carrier', 
                'shipping method sendcloud != shipping method carrier',
                'parcel not created at sendcloud',
                'other',
                'surcharges']
    df.columns = ['totals']
    df = df.append(df.sum().rename('total')).round(2)
    df['distribution'] = df.iloc[:, 0] / df.iloc[-1, -1]

    return df       


def get_price_check_summary(gr, diff, not_created, surcharges, other, sc):
    gr = pd.DataFrame({'description': ['shipping method sendcloud == shipping method carrier'], 'count': [gr['shipments']['total']], 'value': [gr['#shipments * real_price_bpost']['total']]}).set_index('description')

    diff['description'] = 'shipping method sendcloud != shipping method carrier'
    diff = pd.DataFrame(diff.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    diff.columns = ['count', 'value']

    not_created['description'] = 'parcel not created at sendcloud'
    not_created = pd.DataFrame(not_created.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    not_created.columns = ['count', 'value']

    surcharges['description'] = 'surcharges'
    surcharges = pd.DataFrame(surcharges.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    surcharges.columns = ['count', 'value']

    other = pd.DataFrame(other.groupby('description').agg({'tracking_number':len, 'real_price':sum}))
    other.columns = ['count', 'value']

    df = pd.concat([gr, diff, not_created, surcharges, other])
    df = df.sort_values('value', ascending=False)
    df = df.append(df.sum().rename('total')).round(2)
    df['distribution'] = df.iloc[:, -1] / sc.real_price.sum()

    return df    


def get_break_down_one_bpost(df):
    pr = df[df['product'].isin(METHODS)]
    np = df[~df['product'].isin(METHODS)]
    np['product'] = 'other'
    df = pd.concat([pr, np]) 
    df = pd.DataFrame(df.groupby('product').agg({'real_price':sum}).sort_values('real_price', ascending=False)).reset_index()    
    df = df.append(df.sum(numeric_only=True).rename('total')).round(2)

    return df    


def get_invoice_parcel_check(i):
    ts = pd.read_excel(i, sheet_name='ts')
    ts.columns = ['description', 'count', 'value', 'distribution']
    ts = ts[ts['description'].isin(['delivered', 'collected_by_customer'])].distribution.sum().round(4)

    not_created = pd.read_excel(i, sheet_name='nc')

    if len(not_created) == 0:
        nc = len(not_created)
        nv = 0
    else:
        nc = len(not_created['BARCODE'].unique())
        nv = not_created['SALES EXCL. VAT'].astype(float).sum()

    dp = pd.read_excel(i, sheet_name='dp')
    if len(dp) == 0:
        dc = len(dp)
        dv = 0
    else:
        dc = len(dp.tracking_number.unique())
        dv = dp.real_price.sum()

    np = pd.read_excel(i, sheet_name='np')
    np = np.iloc[-1,-1]

    na = pd.read_excel(i, sheet_name='nan')
    lst = []

    for i in range(len(na)):
        if na['nan_value'][i] != 0:
            lst.append(na['column'][i])

    if len(lst) == 0:
        na = 'no NaN values'
    else:
        na = lst

    return ts, nc, nv, dc, dv, np, na    


def get_invoice_price_check(i):
    dis = pd.read_excel(i, sheet_name='summary')
    df = pd.read_excel(i, sheet_name='price-check')
    di = df['#shipments * dif'][len(df) -1]
    ca = df['#shipments * real_price_bpost'][len(df) -1]

    return dis, di, ca


def get_invoice_summary(di, ca, nc, nv, dc, dv, np, na, sc, ts):
    """
    Returns a summary of the checked invoice.
    """
    df = pd.DataFrame({'deviation in real_price between carrier and sendcloud: ': [di],
                       'checked amount of carrier invoice': [ca],
                       '# parcels not created via sendcloud: ': [nc],
                       'percentage of parcels that are delivered: ': [''],
                       'invoice amount parcels not created via sendcloud: ': [nv],
                       '# direct parcels invoiced by carrier: ': [dc],
                       'invoice amount direct parcels invoiced by carrier: ': [dv],
                       'new descriptions added to the invoice: ': np, 
                       '# columns with NaN values: ': [na], 
                       'total invoice value: ': [sc.real_price.sum()]}).transpose()

    df.columns = ['metrics']
    df['relative'] = ''
    df['relative']['checked amount of carrier invoice'] = (df['metrics']['checked amount of carrier invoice'] / sc.real_price.sum()).round(4)
    df['relative']['deviation in real_price between carrier and sendcloud: '] = (df['metrics']['deviation in real_price between carrier and sendcloud: '] / ca).round(4)
    df['relative']['invoice amount parcels not created via sendcloud: '] = (df['metrics']['invoice amount parcels not created via sendcloud: '] / sc.real_price.sum()).round(4)
    df['relative']['percentage of parcels that are delivered: '] = ts

    return df     


def get_description_bpost(i):
    return SURCHARGES_NAMES_DICT.get(i)    


def get_regular_surcharges(df):
    df = df[~df['description_panel'].isna()]
    df = df[(df['description'].isin(['Oversize', 'Overweight'])) | (df['description'].str.contains('Supplement')) & (df['description'] != 'Supplement for drop in retail network')]
    df['price'] = df['real_price']
    df['description'] = df['description'].apply(get_description_bpost)
    df['internal_note'] = 'invoice_number: ' + df['invoice_number'] + ' surcharge {}'.format(dt.datetime.strftime(dt.date.today() - relativedelta(months=1), "%B").lower())
    df['currency'] = 'EUR'
    df['type'] = 'surcharge'
    df['carrier'] = 'bpost'
    df['from_country'] = 'BE'
    df = df[MASK]
    
    return df    


def get_weight_class(i):
    return re.findall(pattern='\=(.*)', string=i)


def get_weight_surcharges_bpost(df):
    df = df[df['product'].isin(METHODS)]
    df = df.drop(['real_price'], axis=1)
    df = df.dropna(subset=['code'])
    df['filter'] = (df['description'] != df['code'])
    df = df[(df['filter'] == True) & (df['description'] != 'XXX')]    
    df['description'] = df.apply(get_change_description_code_bpost, axis=1)
    df['id1'] = df['description'] + df['from_country'] + df['shipping_country']
    df['id2'] = df['code'] + df['from_country'] + df['shipping_country']
    
    pt = pd.read_csv(PRICING_FILE)
    pt['id'] = pt['method_code'] + pt['from_country'] + pt['to_country']
    pr = pt[['id', 'price']]
    rp = pt[['id', 'real_price']]  
    
    df = df.join(pr.set_index('id'), on='id1', how='left')
    df = df.join(pr.set_index('id'), on='id2', how='left', lsuffix='bpost', rsuffix='panel')
    df = df.join(rp.set_index('id'), on='id1', how='left')
    df = df.join(rp.set_index('id'), on='id2', how='left', lsuffix='bpost', rsuffix='panel')     
    
    df['price'] = df['pricebpost'] - df['pricepanel']
    df['real_price'] = df['real_pricebpost'] - df['real_pricepanel']
    
    df['des1'] = df['description'].apply(get_weight_class)
    df['des1'] = df['des1'].str[0] + 'kg'
    df['des2'] = df['code'].apply(get_weight_class)
    df['des2'] = df['des2'].str[0] + 'kg'
    df['description'] = 'Toeslag - aangegeven gewichtsklasse: ' + df['des2'] + ' juiste gewichtsklasse: ' + df['des1']
    df = df[df['price'] > 0]
    df['carrier'] = 'bpost'
    df['type'] = 'surcharge'
    df['currency'] = 'EUR'
    df['internal_note'] = 'invoice_number: ' + df['invoice_number'] + ' surcharge {}'.format(dt.datetime.strftime(dt.date.today() - relativedelta(months=1), "%B").lower())
    df = df[MASK]     
    df['description'] = [i.replace(',return', '') for i in list(df['description'])]
    df['description'] = [i.replace(',saturday', '') for i in list(df['description'])]
    print('Total bpost weight surcharges: ', df['price'].sum())
    
    return df    


def get_double_items(df, column):
    lst = []

    for i in df[column].unique():
        dff = df[df[column] == i].copy()
        dff['count_item_multiple_times_invoiced'] = dff.groupby('tracking_number')['tracking_number'].transform(len)
        dff = dff[(dff['count_item_multiple_times_invoiced'] > 1) & (dff['real_price'] > 0)]
        lst.append(dff)

    dd = pd.concat(lst)
    dd = dd.sort_values('tracking_number')

    return dd 