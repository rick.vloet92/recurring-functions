# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import numpy as np 
import glob
import datetime as dt 
from sqlalchemy import create_engine


# variables
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
DATE_RANGE = (dt.date.today() - dt.timedelta(120))


TYPES = ['A', np.nan, 'DPD HOME', 'DPD CLASSIC', 'Shop return', 'DPD CLASSIC KP', '2Shop', 'DPD HOME KP', 'DPD 12:00', 'Saturday delivery', 'DPD GUARANTEE', 'DPD 8:30',
         'Customs clearance', 'mautpreis', 'Higher Insurance', 'Saturday', 'Tyre surcharge', 'Toeslag', 'Express', 'nachnahme', 'Porti & Paper', 'COD Toeslag', 'Customs documents', 
         'Fuel surcharge', 'verzoll_gebuehr', 'Discount', 'Customs clearance', 'mautpreis', 'Higher Insurance', 'Saturday', 'Tyre surcharge', 'Toeslag', 'Express', 'nachnahme', 
         'Porti & Paper', 'COD Toeslag', 'Customs documents', 'Fuel surcharge', 'verzoll_gebuehr', 'Discount', 'Saturday delivery KP']


DESCRIPTIONS = ['Tarief',
                'Toeslag', 
                'nachnahme',
                'mautpreis', 
                'verzoll_gebuehr', 
                'COD Toeslag', 
                'Higher Insurance', 
                'Saturday',
                'Express', 
                'Tyre surcharge', 
                'Customs clearance', 
                'Fuel surcharge',
                'Customs documents', 
                'Discount', 
                'Porti & Paper']


DESCRIPTIONS_1 = ['Product' if i == 'Tarief' else i + '_1' for i in DESCRIPTIONS]                


COLUMNS = ['Pakketnummer', 
           'Verzenddatum', 
           'Gewicht', 
           'Product', 
           'Tarief',
           'Factuur', 
           'Toeslag', 
           'nachnahme',
           'mautpreis', 
           'verzoll_gebuehr', 
           'COD Toeslag', 
           'Higher Insurance', 
           'Saturday',
           'Express', 
           'Tyre surcharge', 
           'Customs clearance', 
           'Fuel surcharge',
           'Customs documents', 
           'Discount', 
           'Porti & Paper']           


MASK = ['tracking_number', 
        'description',
        'price', 
        'real_price',
        'type',
        'from_country',
        'carrier', 
        'currency',
        'internal_note']


SHIPMENTS = ['DPD HOME', 
             'DPD CLASSIC', 
             'Saturday delivery',
             '2Shop', 
             'DPD HOME KP',
             'DPD 12:00', 
             'DPD GUARANTEE', 
             'Saturday delivery KP']


RETURNS = ['Shop return']


SURCHARGES = ['Toeslag']


CUSTOMS = ['verzoll_gebuehr']


COLUMNS_SURCHARGES = ['Parcel', 
                      #    'Measurement Date', 
                      'Date', 
                      'Description Systemretour',
                      'AccountNumber', 
                      'Weight (g)', 
                      'Length (mm)', 
                      'Width (mm)', 
                      'Height (mm)', 
                      'Girth (mm)',
                      'Amount (EUR)',
                      'Factuur']


NAMES_SURCHARGES = ['tracking_number', 
                    'date', 
                    'description',
                    'account_number',
                    'weight', 
                    'length', 
                    'width', 
                    'height', 
                    'girth', 
                    'real_price', 
                    'invoice_number']


DIM_D = 'Toeslag overschrijden afmetingen en gewicht NP-pakket - '  


RETURN_FEE =  2.5


# queries
already_invoiced_surcharges_query = """
select 
	*
from 
	invoice_item
where 
	"type" = 'surcharge'
and 
	carrier_id = 3
and 
	"date" >= '{}'
and 
	reference in {}
"""

# functions
def get_invoice_data(path, invoice):
    df = pd.read_csv(path + invoice, 
                     sep=';',
                     converters={'Pakketnummer': lambda i: str(i), 
                                 'Gewicht': lambda i: i.replace(',', '.'), 
                                 'Factuur': lambda i: str(i)},
                     low_memory=False,
                     encoding='Latin-1')

    for i in DESCRIPTIONS:
        df[i] = df[i].replace(',', '.')
    
    return df


def get_invoice_data_import_duties(path, invoice):
    df = pd.read_csv(path + invoice, 
                     sep=';',
                     converters={'Parcelnumber': lambda i: str(i),
                                 'Total Amount EUR': lambda i: i.replace(',', '.')}, 
                     low_memory=False,
                     encoding='Latin-1')

    # df = df.iloc[:, :13].copy()
    # df['invoice_number'] = invoice[:-4]

    return df         


def get_all_invoices_data(path):
    invoices = glob.glob(path + '/*.csv')
    lst = []

    for inv in invoices:
        df = pd.read_csv(inv, 
                         sep=';',
                         converters={'Pakketnummer': lambda i: str(i), 
                                     'Gewicht': lambda i: i.replace(',', '.'), 
                                     'Factuur': lambda i: str(i)},
                         low_memory=False,
                         encoding='Latin-1')
        lst.append(df)

    df = pd.concat(lst)

    for i in DESCRIPTIONS:
        df[i] = df[i].replace(',', '.')
    
    return df        


def get_select_columns(df):
    df = df[COLUMNS].copy()

    for i in DESCRIPTIONS:
        df[i + '_1'] = i

    COL = ['Factuur', 'Pakketnummer', 'Verzenddatum', 'Gewicht']

    lst = []    

    for i in range(len(DESCRIPTIONS)):
        c = COL
        c.extend([DESCRIPTIONS_1[i], DESCRIPTIONS[i]])
        x = df.loc[:, c]
        x.columns = ['invoice_number', 'tracking_number', 'date', 'weight', 'description', 'real_price']
        c.remove(DESCRIPTIONS[i])
        c.remove(DESCRIPTIONS_1[i])
        lst.append(x)

    df = pd.concat(lst)
    df = df.sort_values('tracking_number').reset_index().drop(['index'], axis=1)
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)
    df['real_price'] = df['real_price'].fillna('0')
    df['real_price'] = df['real_price'].astype(str)
    df['real_price'] = [i.replace(',', '.') for i in list(df['real_price'])]
    df['real_price'] = df['real_price'].astype(float)

    return df    


def get_select_columns_import_duties(df):
    df = df[['Declaration date',
                   'Parcelnumber',
                   'VAT amount EUR', 'invoice_number']].copy()
    
    df.columns = ['date', 'tracking_number', 'real_price', 'invoice_number']
    df['real_price'] = df['real_price'].astype(float)
    df['date'] = pd.to_datetime(df['date'])

    return df      


def get_new_products_of_carrier(df):
    lst = []

    for i in list(df['description'].unique()):
        if i not in TYPES:
            lst.append(i)

    if len(lst) > 0:
        print('the carrier added these products: ', lst)
        df = pd.DataFrame({'new products':[lst]})
    else:
        print('no products added!')
        df = pd.DataFrame({'new products':[['no products added!']]})
    
    return df  


def get_calculate_shipping_prices_dpdnl(df):
    """
    Function that adds customs and subtracts remote area surcharge.
    """
    if df['extra'] == 'A' and df['customs_clearance'] == 27:
        return df['real_price'] - df['surcharge'] + df['customs_clearance']
    elif df['extra'] == 'A':
        return df['real_price'] - df['surcharge']
    elif df['customs_clearance'] == 27:
        return df['real_price'] + df['customs_clearance']
    else:
        return df['real_price']    


def get_toll_price_dpdnl(df):
    """
    Function that include toll costs to the shipping costs.
    """
    if df['country'] == 'NL':
        return round(TOLL_NL * df['tarife'], 2)
    elif df['country'] == 'BE':
        return round(TOLL_BE * df['tarife'], 2)
    elif df['country'] in ('LU', 'FR', 'IE', 'IT', 'MC', 'PT', 'ES', 'GB', 'CH'):
        return round(TOLL_BE * 0.5 * df['tarife'], 2)
    elif df['country'] == 'DE':
        return round(TOLL_DE * df['tarife'], 2)
    else:
        return round(TOLL_DE * 0.5 * df['tarife'], 2)        


def get_fuel_surcharge_dpdnl(df):
    """
    Function that include fuel costs to the shipping costs.
    """    
    if df['description'] in ('DPD CLASSIC', 'DPD CLASSIC KP', 'DPD HOME', 'DPD HOME KP', 'Saturday delivery', 'DPD 8:30'):
        return round(df['real_price'] * FUEL_SURCHARGE, 2)
    elif df['description'] in ('DPD GUARANTEE', 'DPD 12:00') and df['country'] == 'NL':
        return round(df['real_price'] * FUEL_SURCHARGE, 2)    
    elif df['description'] in ('DPD GUARANTEE', 'DPD 12:00') and df['country'] != 'NL':
        return round(df['real_price'] * KEROSINE_SURCHARGE, 2)
    else:
        return 0  


def get_prices_dpdnl(df):
    """
    Returns a DataFrame with the invoiced prices by the carrier.
    """
    df['real_price'] = df.apply(get_calculate_shipping_prices_dpdnl, axis=1)
    df['toll'] = df.apply(get_toll_price_dpdnl, axis=1)
    df['fuel'] = df.apply(get_fuel_surcharge_dpdnl, axis=1)
    df['real_price'] = df['real_price'] + df['toll'] + df['fuel']
    
    return df    


def get_change_shipping_country_dpdnl(df):
    """
    Function that changes to shipping country for returns.
    """
    if df['direction'] == 'i':
        return df['from_country']
    else:
        return df['shipping_country']    


def get_parcel_details_panel(df):
    """
    Returns a DataFrame where the Sendcloud details are added.
    """
    df = df.dropna(subset=['tracking_number'])    
    tn = list(df['tracking_number'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(panel_data_query.format(tu, DATE_RANGE), con=ENGINE)
    df = df.join(pa.set_index('tracking_number'), on='tracking_number', how='left') 
    df['shipping_country'] = df.apply(get_change_shipping_country_dpdnl, axis=1)
    
    return df     


def get_divide_invoice_types(df):   
    df = df[df['real_price'] != 0]
    not_created = df[df['description_panel'].isna()]
    df = df[~df['description_panel'].isna()]
    shipments = df[df['description'].isin(SHIPMENTS)]
    returns = df[df['description'].isin(RETURNS)]
    surcharges = df[df['description'].isin(SURCHARGES)]
    customs = df[df['description'].isin(CUSTOMS)]

    return shipments, returns, surcharges, not_created, customs


def get_invoice_distribution(shipments, returns, surcharges, not_created, customs):
    df = pd.DataFrame({'description': ['shipments', 'returns', 'surcharges', 'not_created', 'customs'],
                       'amount': [shipments.real_price.sum(), returns.real_price.sum(), surcharges.real_price.sum(), not_created.real_price.sum(), customs.real_price.sum()]})    
    df = df.append(df.sum(numeric_only=True).rename('total'))
    df['distribution'] = df.iloc[:, -1] / df['amount']['total']

    return df


def get_real_price_comparison_dataframe(shipments, returns):
    df = pd.concat([shipments, returns]).copy()
    gr = pd.DataFrame(df.groupby(['description_panel', 'shipping_country']).agg({'real_price':['count', 'mean'], 'real_price_panel':'mean'}))
    gr.columns = ['shipments', 'real_price_dpdnl', 'real_price_panel']
    gr['dif (%)'] = round(((gr['real_price_panel'] - gr['real_price_dpdnl']) / gr['real_price_panel']), 4) * 100
    gr['dif'] = (gr['real_price_panel'] - gr['real_price_dpdnl'])
    gr['#shipments * dif'] = gr['shipments'] * gr['dif']
    gr['#shipments * real_price_dpdnl'] = gr['shipments'] * gr['real_price_dpdnl']
    gr['#shipments * real_price_panel'] = gr['shipments'] * gr['real_price_panel']
    gr = gr.sort_values('#shipments * dif')
    gr = gr.append(gr.sum(numeric_only=True).rename('total')).round(2)
    
    return gr


def get_change_date(df):
    if (df['date'] - pd.to_datetime(dt.date.today())).days < -30:
        return pd.to_datetime(dt.date.today())
    else:
        return df['date']


def get_surcharges_dpdnl(df):
    df['date'] = df.apply(general_carrier.get_change_date, axis=1)
    df['price'] = df['real_price']
    df['description'] = 'Toeslag afgelegen gebied: - ' + df['shipping_country']
    df['type'] = 'surcharge'
    df['from_country'] = 'NL'
    df['carrier'] = 'dpd'
    df['date'] = pd.to_datetime(df['date'])
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['currency'] = 'EUR'
    df = df[MASK]
    
    return df    


def get_invoice_parcel_check(i):
    ts = pd.read_excel(i, sheet_name='ts')
    ts.columns = ['description', 'count', 'value', 'distribution']
    ts = ts[ts['description'].isin(['delivered', 'collected_by_customer'])].distribution.sum().round(4)

    dn = pd.read_excel(i, sheet_name='di').real_price.sum()

    not_created = pd.read_excel(i, sheet_name='nc')

    if len(not_created) == 0:
        nc = len(not_created)
        nv = 0
    else:
        nc = len(not_created['Pakketnummer'].unique())
        nv = not_created.copy()
        nv['Totaal'] = [i.replace(',', '.') for i in list(nv['Totaal'])]
        nv = nv['Totaal'].astype(float).sum()

    dp = pd.read_excel(i, sheet_name='dp')
    if len(dp) == 0:
        dc = len(dp)
        dv = 0
    else:
        dc = len(dp.tracking_number.unique())
        dv = dp.real_price.sum()

    np = pd.read_excel(i, sheet_name='np')
    np = np.iloc[-1,-1]

    na = pd.read_excel(i, sheet_name='nan')
    lst = []

    for i in range(len(na)):
        if na['nan_value'][i] != 0:
            lst.append(na['column'][i])

    if len(lst) == 0:
        na = 'no NaN values'
    else:
        na = lst

    return ts, nc, nv, dc, dv, np, na, dn


def get_invoice_price_check(i):
    dis = pd.read_excel(i, sheet_name='summary')
    df = pd.read_excel(i, sheet_name='price-check')
    di = df['#shipments * dif'][len(df) -1]
    ca = df['#shipments * real_price_dpdnl'][len(df) -1]

    return dis, di, ca


def get_invoice_summary(di, ca, nc, nv, dc, dv, np, na, sc, ts, dn):
    df = pd.DataFrame({'checked amount of carrier invoice': [ca],
                       'deviation in real_price between carrier and sendcloud: ': [di],
                       'percentage of parcels that are delivered: ': [''],
                       '# parcels not created via sendcloud: ': [nc],
                       'invoice amount parcels not created via sendcloud: ': [nv],
                       '# direct parcels invoiced by carrier: ': [dc],
                       'invoice amount direct parcels invoiced by carrier: ': [dv],
                       'new descriptions added to the invoice: ': np, 
                       '# columns with NaN values: ': [na], 
                       'amount double invoiced: ': [dn],
                       'total invoice value: ': [sc.real_price.sum()]}).transpose()

    df.columns = ['metrics']
    df['relative'] = ''
    df['relative']['checked amount of carrier invoice'] = (df['metrics']['checked amount of carrier invoice'] / sc.real_price.sum()).round(4)
    df['relative']['deviation in real_price between carrier and sendcloud: '] = (df['metrics']['deviation in real_price between carrier and sendcloud: '] / ca).round(4)
    df['relative']['invoice amount parcels not created via sendcloud: '] = (df['metrics']['invoice amount parcels not created via sendcloud: '] / sc.real_price.sum()).round(4)
    df['relative']['percentage of parcels that are delivered: '] = ts

    return df   


def get_invoice_data_surcharges(path, invoice):
    df = pd.read_csv(path + invoice + '.csv', 
                     sep=';', 
                     converters={'Parcel': lambda i: str(i), 
                                 'Toeslag': lambda i: i.replace(',', '.'), 
                                 'Weight (g)': lambda i: i.replace(',', '.'), 
                                 'Length (mm)': lambda i: i.replace(',', '.'), 
                                 'Width (mm)': lambda i: i.replace(',', '.'), 
                                 'Height (mm)': lambda i: i.replace(',', '.'), 
                                 'Girth (mm)': lambda i: i.replace(',', '.'), 
                                 'Amount (EUR)': lambda i: i.replace(',', '.'), 
                                 'Date': lambda i: str(i)},
                     encoding='Latin-1')

    if 'TYPE' in df.columns:
        df = df[df['TYPE'] == 'P'].copy()

    return df    


def get_select_columns_surcharges(df, invoice_number):
    if 'Factuur' not in df.columns:
        df['Factuur'] = invoice_number
    df = df[COLUMNS_SURCHARGES].copy()
    df.columns = NAMES_SURCHARGES

    lst = []

    for i in range(2):
        if i == 0:
            x = df[df['description'].isna()].reset_index().drop(['index'], axis=1)
            x[['real_price', 'weight', 'length', 'width', 'height', 'girth']] = x[['real_price', 'weight', 'length', 'width', 'height', 'girth']].astype(float)
            x['weight'] = [round(i/1000,1) for i in list(x['weight'])]
            x['length'] = [round(i/10,1) for i in list(x['length'])]
            x['width'] = [round(i/10,1) for i in list(x['width'])]
            x['height'] = [round(i/10,1) for i in list(x['height'])]
            x['girth'] = [round(i/10,1) for i in list(x['girth'])]
            x['date'] = pd.to_datetime(x['date'], dayfirst=True)
            lst.append(x)
        else:
            x = df[~df['description'].isna()].reset_index().drop(['index'], axis=1)
            x['real_price'] = x['real_price'].astype(float)
            x['date'] = pd.to_datetime(x['date'], dayfirst=True)
            lst.append(x)
    
    df = pd.concat(lst)
    
    return df    


def get_nsp_surcharge(df):
    df = df[df['description'].isna()].copy()
    df = df[df['real_price'].isin([1.5, 4.5])]
    df['description'] = 'NSP toeslag'    
    
    return df


def get_description_dimension_surcharge(df):
    if df['girth'] >= 300:
        return DIM_D + 'omvang: ' + str(df['girth']) + ' (cm)'
    elif df['length'] >= 175:
        return DIM_D + 'afmetingen: ' + str(df['length']) + 'x' + str(df['width']) + 'x' + str(df['height']) + ' (cm)'
    else:
        return DIM_D + 'gewicht: ' + str(df['weight']) + '(kg)'


def get_dimension_surcharge(df):
    df = df[df['description'].isna()].copy()
    df = df[df['real_price'].isin([37.45, 39.45])]
    df['description'] = df.apply(get_description_dimension_surcharge, axis=1)
    
    return df            


def get_overgrootte_gewicht_surcharge(df):
    df = df[df['description'].isna()].copy()
    df = df[df['real_price'] == 7.50]
    df['description'] = 'Overgrootte gewicht'
    
    return df    


def get_dimension_surcharge_dpdnl(nsp, dim, ove):
    df = pd.concat([nsp, dim, ove])
    df = df[~df['description_panel'].isna()]

    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['type'] = 'surcharge'
    df['from_country'] = 'NL'
    df['carrier'] = 'dpd'
    df['price'] = df['real_price']
    df['currency'] = 'EUR'
    df = df[MASK]    
    
    return df    


def get_returns_surcharges(df):
    df = df[~df['description'].isna()].copy()
    df = df[df['direction'] == 'o']
    df['price'] = df['price_panel'] + RETURN_FEE
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['description'] = 'Pakket retour afzender'
    df['from_country'] = 'NL'
    df['carrier'] = 'dpd'
    df['type'] = 'surcharge'
    df['currency'] = 'EUR'
    df = df[MASK]
    df.dropna(subset=['price'],inplace=True)
    
    return df


def get_already_invoiced_surcharges(df):
    tn = list(df['tracking_number'])
    tu = tuple(tn[i] for i in range(len(tn)))
    df = pd.read_sql_query(already_invoiced_surcharges_query.format(DATE_RANGE, tu), con=ENGINE)
    df['date'] = df['date'].astype(str)

    return df