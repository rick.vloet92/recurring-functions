# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import pandas as pd 
import datetime as dt 
import numpy as np
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine


# variables

CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})

TYPES = ['Address Correction Dom. Standard', 'Peak Surcharge - Over Max', 'UK Border Fee', 'Extended Area Collection Surcharge',
         '19.000 % Tax', 'Not Previously Billed WW Standard', 'Not Previously Billed UK Border Fee',
         'Address Correction Dom. Express Saver', 'Address Correction WW Standard',
         'Address Correction Dom. Express', 
         'Address Correction TB Standard', 
         'Dom. Standard Undeliverable Return', 
         'Fuel Surcharge', 
         'Not Previously Billed Dom. Standard', 
         'Not Previously Billed Residential',
         'Not Previously Billed Fuel Surcharge',
         'Not Previously Billed TB Standard', 
         'Not Previously Billed Additional Handling', 
         np.nan, 
         'Dom. Standard Adjustment', 
         'TB Standard Adjustment',
         'Dom. Express Adjustment', 
         'Dom. Express Saver Adjustment', 
         'TB Express Saver Adjustment',
         'Transportation', 
         'Alternate Address- Future Day- Web', 
         'Residential Collection Service', 
         'Future Day Collection- Web', 
         'Alternate Address- Future Day-Phone', 
         'Alternate Address- Same Day- Web', 
         'Same Day Collection- Web', 
         'Alternate Address- Same Day- Phone', 
         'Daily Service Charge', 
         'Address Correction WW Express Saver', 
         'GEB\\HR - AUSFUHRANMELDUNG (EDS)', 
         'WW Standard Adjustment', 
         'WW Express Saver Adjustment',
         'Dom. Standard', 
         'Dom. Express Saver', 
         'Large Package Surcharge',
         'Peak Surcharge - Large Package', 
         'Residential',
         'Dom. Express', 
         'Additional Handling', 
         'Peak Surcharge- Additional Handling', 
         'Sat.Delivery', 
         'Extended Area Surcharge-Destination', 
         'UPS Access Point Hold Service', 
         'Adult Signature Required', 
         'TB Standard', 
         'TB Express Saver', 
         'Remote Area Surcharge', 
         'TB Express', 
         'WW Express Saver', 
         'Delivery Area Surcharge - Extended', 
         'WW Standard', 
         'Not Previously Billed Dom. Express', 
         'Not Previously Billed Large Package Surcharge', 
         'Not Previously Billed Peak Surcharge - Large Package', 
         'Overmax Length', 
         'Overmax Size', 
         'Extended Area P/U Surcharge-Origin', 
         'Address Correction TB Express Saver', 
         'Not Previously Billed Peak Surcharge- Additional Handling', 
         'Not Previously Billed Dom. Express Saver', 
         'Peak Surcharge - Commercial', 
         'Peak Surcharge - Residential', 
         '16.000 % Tax', 
         'Billable Usage', 
         'Prohibited Item Fee',
         'WW Expedited', 'Not Previously Billed Overmax Size', 'Not Previously Billed Adult Signature Required', 'Same Day Collection- Phone']


RESIDENTIAL = ['Not Previously Billed Residential', 
               'Residential', 
               'Dom. Express Adjustment', 
               'Dom. Express Saver Adjustment', 
               'Dom. Standard Adjustment', 
               'TB Express Saver Adjustment', 
               'TB Standard Adjustment', 
               'WW Express Saver Adjustment', 
               'WW Standard Adjustment']


SURCHARGES = ['Address Correction Dom. Standard', 
              'Peak Surcharge - Over Max', 
              'Address Correction TB Standard', 
              'Same Day Collection- Phone', 
              'Residential Collection Service', 
              'Future Day Collection- Web', 
              'Same Day Collection- Web',
              'Address Correction WW Express Saver', 
              'Address Correction WW Standard', 
              'Additional Handling', 
              'Large Package Surcharge', 
              'Extended Area Surcharge-Destination', 
              'Overmax Size', 
              'Remote Area Surcharge', 
              'Address Correction TB Express Saver', 
              'Address Correction Dom. Express', 
              'Overmax Length', 
              'Overmax Weight'
              'Delivery Area Surcharge - Extended', 
              'Delivery Area Surcharge - Extended',
              'Address Correction Dom. Express Saver', 
              'Extended Area Collection Surcharge', 
              'Peak Surcharge - Over Max',         
              'Not Previously Billed Large Package Surcharge', 
              'Not Previously Billed Peak Surcharge - Large Package', 
              'Extended Area P/U Surcharge-Origin', 
              'Alternate Address- Future Day- Web', 
              'Alternate Address- Future Day-Phone', 
              'Alternate Address- Same Day- Phone', 
              'Alternate Address- Same Day- Web', 
              'Not Previously Billed Peak Surcharge- Additional Handling',               
              'Peak Surcharge - Commercial', 
              'Peak Surcharge - Residential', 
              'Prohibited Item Fee']


RETURNS = ['Dom. Standard Undeliverable Return']


DEFAULT = ['Alternate Address- Future Day- Web', 'Alternate Address- Future Day-Phone', 'Alternate Address- Same Day- Phone', 
           'Alternate Address- Same Day- Web', 'Same Day Collection- Phone', 'Same Day Collection- Web', 'Future Day Collection- Web']


SHIPPING_METHODS = ['Dom. Express Saver', 
                    'Dom. Express',
                    'Dom. Standard', 
                    'TB Express Saver', 
                    'TB Standard', 
                    'WW Express Saver', 
                    'WW Standard']           


MASK = ['tracking_number', 
        'description', 
        'price', 
        'real_price', 
        'type', 
        'from_country',
        'carrier', 
        'currency',
        'internal_note']


DESCRIPTION_DICT = {
    'Additional Handling' :                                         'Zuschlag Zusätzliche Handhabung',
    'Address Correction WW Express Saver' :                         'Zuschlag Adresskorrektur',
    'Address Correction Dom. Express Saver' :                       'Zuschlag Adresskorrektur',
    'Address Correction WW Standard' :                              'Zuschlag Adresskorrektur',
    'Address Correction TB Express Saver' :                         'Zuschlag Adresskorrektur',
    'Address Correction TB Standard' :                              'Zuschlag Adresskorrektur',
    'Address Correction Dom. Express' :                             'Zuschlag Adresskorrektur',
    'Address Correction Dom. Standard' :                            'Zuschlag Adresskorrektur',
    'Extended Area Service Delivery' :                              'Zuschlag Service Für Den Erweiterten Bereich',
    'Delivery Area Surcharge - Extended' :                          'Zuschlag Service Für Den Erweiterten Bereich',
    'Extended Area P/U Surcharge-Origin' :                          'Zuschlag Service Für Den Erweiterten Bereich',
    'Extended Area Surcharge-Destination' :                         'Zuschlag Service Für Den Erweiterten Bereich',
    'Remote Area Surcharge' :                                       'Zuschlag Abgelegenes Abholgebiet',   
    'Large Package Surcharge' :                                     'Zuschlag Aufpreis Für Großes Paket',
    'Not Previously Billed Large Package Surcharge' :               'Zuschlag Aufpreis Für Großes Paket',
    'Overmax Size' :                                                'Zuschlag Über Der Maximalen Größe',
    'Not Previously Billed Overmax Size' :                          'Zuschlag Über Der Maximalen Größe',
    'Overmax Length' :                                              'Zuschlag Über Der Maximalen Größe',
    'Overmax Weight' :                                              'Zuschlag Über Der Maximalen Größe',
    'Extended Area Collection Surcharge':                           'Extended Area Collection Surcharge',    
    'Peak Surcharge additional Handling' :                          'Saisonalzuschlag',
    'Peak Surcharge Over Maximum Length' :                          'Saisonalzuschlag',
    'Peak Surcharge Over Maximum Size' :                            'Saisonalzuschlag',
    'Peak Surcharge - Over Max' :                                   'Saisonalzuschlag',
    'Peak Surcharge - Over Max' :                                   'Saisonalzuschlag',
    'Not Previously Billed Peak Surcharge - Large Package' :        'Saisonalzuschlag',
    'Not Previously Billed Peak Surcharge- Additional Handling' :   'Saisonalzuschlag',
    'Peak Surcharge - Commercial' :                                 'Saisonalzuschlag', 
    'Peak Surcharge - Residential' :                                'Saisonalzuschlag',
    'Alternate Address- Future Day- Web' :                          'Zuschlag Alternative Adresse', 
    'Alternate Address- Future Day-Phone' :                         'Zuschlag Alternative Adresse', 
    'Alternate Address- Same Day- Phone' :                          'Zuschlag Alternative Adresse', 
    'Alternate Address- Same Day- Web' :                            'Zuschlag Alternative Adresse',
    'Not Previously Billed Residential' :                           'Zuschlag für Privatzustellung', 
    'Residential' :                                                 'Zuschlag für Privatzustellung', 
    'Dom. Express Adjustment' :                                     'Zuschlag für Privatzustellung', 
    'Dom. Express Saver Adjustment' :                               'Zuschlag für Privatzustellung', 
    'Dom. Standard Adjustment' :                                    'Zuschlag für Privatzustellung', 
    'TB Express Saver Adjustment' :                                 'Zuschlag für Privatzustellung', 
    'TB Standard Adjustment' :                                      'Zuschlag für Privatzustellung', 
    'WW Express Saver Adjustment' :                                 'Zuschlag für Privatzustellung', 
    'WW Standard Adjustment' :                                      'Zuschlag für Privatzustellung',   
    'Dom. Standard Undeliverable Return' :                          'Zuschlag Retoure',  
    'Prohibited Item Fee' :                                         'Zuschlag Verbotener Artikel' 
}


STANDARD_PRICE_DICT = {
    'Zuschlag Aufpreis Für Großes Paket' :  80.45,
    'Zuschlag Über Der Maximalen Größe' :  123.75,
    'Zuschlag Adresskorrektur' :            7.10,
    'Zuschlag Abgelegenes Abholgebiet' :    31
}


EXPRESS_PRICE_DICT = {
    'Zuschlag Aufpreis Für Großes Paket' :  84.75,
    'Zuschlag Über Der Maximalen Größe' :  130.35,
    'Zuschlag Adresskorrektur' :            7.10,
    'Zuschlag Abgelegenes Abholgebiet' :    31
}


DEFAULT_PRICE_DICT = {
    'Alternate Address- Future Day- Web' :  3.5, 
    'Alternate Address- Future Day-Phone' : 4.4, 
    'Alternate Address- Same Day- Phone' :  7.90, 
    'Alternate Address- Same Day- Web' :    7,
    'Same Day Collection- Phone' :          7, 
    'Same Day Collection- Web' :            7,
    'Future Day Collection- Web' :          3.5 
}


DEFAULT_DESCRIPTION_DICT = {
    'Alternate Address- Future Day- Web' :                          'Zuschlag Alternative Adresse', 
    'Alternate Address- Future Day-Phone' :                         'Zuschlag Alternative Adresse', 
    'Alternate Address- Same Day- Phone' :                          'Zuschlag Alternative Adresse', 
    'Alternate Address- Same Day- Web' :                            'Zuschlag Alternative Adresse',
    'Same Day Collection- Phone' :                                  'Express Abholing', 
    'Same Day Collection- Web' :                                    'Express Abholing',
    'Future Day Collection- Web' :                                  'Standard abholing' 
}


DESCRIPTION_WEIGHT_SURCHARGES_DICT = {
    'ups:standard/service=11,kg=0-30':'0-30kg',
    'ups:standard/service=11,kg=30-40':'30-40kg',
    'ups:standard/service=11,kg=40-50':'40-50kg',
    'ups:standard/service=11,kg=50-60':'50-60kg',
    'ups:standard/service=11,kg=60-70':'60-70kg',
    'ups:standard/service=11,kg=0-30,signature,adult':'0-30kg',
    'ups:standard/service=11,kg=30-40,signature,adult':'30-40kg',
    'ups:standard/service=11,kg=40-50,signature,adult':'40-50kg',
    'ups:standard/service=11,kg=50-60,signature,adult':'50-60kg',
    'ups:standard/service=11,kg=60-70,signature,adult':'60-70kg',
    'ups:express/service=07,kg=0-3':'0-3kg',
    'ups:express/service=07,kg=3-6':'3-6kg',
    'ups:express/service=07,kg=6-10':'6-10kg',
    'ups:express/service=07,kg=10-15':'10-15kg',
    'ups:express/service=07,kg=15-20':'15-20kg',
    'ups:express/service=07,kg=20-30':'20-30kg',
    'ups:express/service=07,kg=30-40':'30-40kg',
    'ups:express/service=07,kg=40-50':'40-50kg',
    'ups:express/service=07,kg=50-60':'50-60kg',
    'UPS Express 60-70kg':'60-70kg',
    'ups:express/service=07,kg=0-3,saturday':'0-3kg',
    'ups:express/service=07,kg=3-6,saturday':'3-6kg',
    'ups:express/service=07,kg=6-10,saturday':'6-10kg',
    'ups:express/service=07,kg=10-15,saturday':'10-15kg',
    'ups:express/service=07,kg=15-20,saturday':'15-20kg',
    'ups:express/service=07,kg=20-30,saturday':'20-30kg',
    'ups:express/service=07,kg=30-40,saturday':'30-40kg',
    'ups:express/service=07,kg=40-50,saturday':'40-50kg',
    'ups:express/service=07,kg=50-60,saturday':'50-60kg',
    'ups:express/service=07,kg=60-70,saturday':'60-70kg',
    'ups:standard/service=65,kg=0-3':'0-3kg',
    'ups:standard/service=65,kg=3-6':'3-6kg',
    'ups:standard/service=65,kg=6-10':'6-10kg',
    'ups:standard/service=65,kg=10-15':'10-15kg',
    'ups:standard/service=65,kg=15-20':'15-20kg',
    'ups:standard/service=65,kg=20-30':'20-30kg',
    'ups:standard/service=65,kg=30-40':'30-40kg',
    'ups:standard/service=65,kg=40-50':'40-50kg',
    'ups:standard/service=65,kg=50-60':'50-60kg',
    'ups:standard/service=65,kg=60-70':'60-70kg',
    'ups:standard/service=65,kg=0-3,service_point':'0-3kg',
    'ups:standard/service=65,kg=3-6,service_point':'3-6kg',
    'ups:standard/service=65,kg=6-10,service_point':'6-10kg',
    'ups:standard/service=65,kg=10-15,service_point':'10-15kg',
    'ups:standard/service=65,kg=15-20,service_point':'15-20kg'
}


# functions
def get_invoice_data(path, invoice):
    df = pd.read_csv(path + invoice +'.csv', 
                     encoding='latin-1', 
                     low_memory=False, 
                     header=None)

    return df


def get_count_lead_tracking_number_multi_collo(df):
    lst = []

    for i in df.description.unique():
        x = df[df['description'] == i].copy()
        x['count_2'] = x.groupby('lead_tracking_number')['lead_tracking_number'].transform(len)
        lst.append(x)

    df = pd.concat(lst)

    return df


def get_count_appearances_lead_in_tt(df):
    lst = []
    x = 0

    for i in range(len(df)):
        if (df['lead_tracking_number'][i] == df['tracking_number'][i]):
            x += 1
            lst.append(x)
            if x == 2:
                lst[i-1] = 2
        else:
            x = 0
            lst.append(x)
    
    return lst


def get_rp_distribution(df):    
    lst = []

    for i in df.lead_tracking_number.unique():
        x = df[df['lead_tracking_number'] == i].reset_index().drop(['index'], axis=1)
        count = x['count_3'][0]
        rp = x['real_price'][0]
        if count == 2:
            x = x[x['real_price'] == 0].copy()
            rp = rp / (len(x))
            x['real_price'] = rp
            lst.append(x)
        else:
            rp = rp / (len(x))
            x['real_price'] = rp
            lst.append(x)

    df = pd.concat(lst)

    return df        


def get_select_columns(df):
    df = df[[13,
             20, 
             45, 
             46,
             32,
             5, 
             52, 
             28, 
             11]].copy()
    
    df.columns = ['lead_tracking_number',
                  'tracking_number', 
                  'description', 
                  'count',
                  'dimensions',
                  'invoice_number', 
                  'real_price',  
                  'weight',
                  'date']

    df = df[(df['real_price'] != 0) & (~df['tracking_number'].isna())].reset_index().drop(['index'], axis=1)
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)
    df['invoice_number'] = [str(i) for i in list(df['invoice_number'])]
    df['tracking_number'] = df['tracking_number'].astype(str)
    df['lead_tracking_number'] = df['lead_tracking_number'].astype(str)
    
    return df

def get_data_manipulations_multi_collo(df):
    surcharges = df[df['description'].isin(SURCHARGES)].copy()
    df = df[~df['description'].isin(SURCHARGES)].copy()
    surcharges_multi = surcharges[surcharges['count'] > 1]
    surcharges_multi = surcharges[surcharges['lead_tracking_number'].isin(list(surcharges_multi['lead_tracking_number']))].copy()

    surcharges_multi = get_count_lead_tracking_number_multi_collo(surcharges_multi)
    
    surcharges = surcharges[~surcharges.index.isin(list(surcharges_multi.index))]

    surcharges_multi_1 = surcharges_multi[surcharges_multi['count_2'] == 1]
    surcharges_multi_2 = surcharges_multi[surcharges_multi['count_2'] > 1].reset_index().drop(['index'], axis=1)

    surcharges_multi_2['count_3'] = get_count_appearances_lead_in_tt(surcharges_multi_2)
    surcharges_multi_2 = get_rp_distribution(surcharges_multi_2)

    surcharges = pd.concat([surcharges_multi_1, surcharges_multi_2, surcharges])

    df = pd.concat([surcharges, df])

    df = df[df['real_price'] != 0].reset_index().drop(['index'], axis=1)
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)
    df['invoice_number'] = [str(i) for i in list(df['invoice_number'])]

    return df


def get_new_products_of_carrier(df):
    lst = []

    for i in df.description.unique():
        if i not in TYPES:
            lst.append(i)
            
    if lst:
        print('these are the new upsde products: ',lst)
        df = pd.DataFrame({'new products':[lst]})
    else:
        print('there are no new upsde products!')  
        df = pd.DataFrame({'new products':[['no products added!']]})

    return df


def get_parcels_not_created_at_sendcloud(df, pa):
    nc = df[~df[20].isin(list(pa['tracking_number']))]
    
    return nc     


def get_surcharges_overview(df):
    df = df.dropna(subset=['description_panel']).copy()
    df = df[df['real_price'] > 0]
    su = df[df['description'].isin(SURCHARGES)].copy()
    re = df[df['description'].isin(RESIDENTIAL)].copy()
    re['description'] = 'Zuschlag für Privatzustellung'
    rt = df[df['description'].isin(RETURNS)].copy()
    rt = rt[rt['direction'] != 'i']
    rt['description'] = 'Zuschlag Retoure'

    df = pd.concat([su, re, rt])
    df = df.dropna(subset=['description_panel'])
    df['description'] = [DESCRIPTION_DICT.get(i) if i in DESCRIPTION_DICT.keys() else i for i in list(df['description'])]
    df['description'] = [DEFAULT_DESCRIPTION_DICT.get(i) if i in DEFAULT_DESCRIPTION_DICT.keys() else i for i in list(df['description'])]

    df = pd.DataFrame(df.groupby('description').agg({'tracking_number' : len, 'real_price': ['mean', sum]})).reset_index()
    df.columns = ['description', 'count', 'mean', 'sum']
    df = df.sort_values('sum', ascending=False)
    df = df.append(df.sum(numeric_only=True).rename('total'))

    return df


def get_double_items(df, column):
    lst = []

    df = df.dropna(subset=['tracking_number']).copy()

    for i in df[column].unique():
        dff = df[df[column] == i].copy()
        dff['count_item_multiple_times_invoiced'] = dff.groupby('tracking_number')['tracking_number'].transform(len)
        dff = dff[(dff['count_item_multiple_times_invoiced'] > 1) & (dff['real_price'] > 0)]
        lst.append(dff)

    dd = pd.concat(lst)
    dd = dd.sort_values('tracking_number')

    return dd     


def get_invoice_check_summary(df, sc, nc, dp, ts, di, np):
    df = pd.DataFrame({'invoice_value': [sc.real_price.sum()], 
                       'number_of_lines': [len(df)],
                       'number_of_unique_tracking_numbers': [len(sc.tracking_number.unique())],
                       'number_of_new_product_on_invoice': np['new products'],
                       'parcels_delivered (0-1 %)': [ts[ts.index.isin(['delivered', 'collected_by_customer'])]['distribution'].sum()],
                       'number_of_unmatched_items': [len(nc)],
                       'number_of_direct_shipments_invoiced': [len(dp)],
                       'number_of_shipments_invoiced_multiple_times_unvalid': [len(di.tracking_number.unique())]}).transpose()

    df.columns = ['metric']

    return df


def get_country_dependent_surcharges_standard(df):
    if df['description'] == 'Zuschlag Zusätzliche Handhabung':
        if df['shipping_country'] == 'DE':
            return 9.3
        else:
            return 21.05
    elif df['description'] == 'Zuschlag Service Für Den Erweiterten Bereich':
        if df['shipping_country'] == 'DE':
            return 11.5
        else:
            return 31
    elif df['description'] == 'Saisonalzuschlag':
        if df['tarife'] == 0.22:
            return (0.25 * df['weight'])
        elif df['tarife'] == 0.35:
            return (0.40 * df['weight'])
        elif df['tarife'] == 0.44:
            return (0.50 * df['weight'])
        elif df['tarife'] == 0.88:
            return (1 * df['weight'])
        else:
            return np.nan
    else:
        return df['price']


def get_country_dependent_surcharges_express(df):
    if df['description'] == 'Zuschlag Zusätzliche Handhabung':
        if df['shipping_country'] == 'DE':
            return 9.8
        else:
            return 22.15
    elif df['description'] == 'Zuschlag Service Für Den Erweiterten Bereich':
        if df['shipping_country'] == 'DE':
            return 11.5
        else:
            return 31
    elif df['description'] == 'Saisonalzuschlag':
        if df['tarife'] == 0.22:
            return (0.25 * df['weight'])
        elif df['tarife'] == 0.35:
            return (0.45 * df['weight'])
        elif df['tarife'] == 0.44:
            return (0.55 * df['weight'])
        elif df['tarife'] == 0.88:
            return (1.05 * df['weight'])
        else:
            return np.nan
    else:
        return df['price']


def get_dimensions(df):
    dim = df[['tracking_number', 'weight']].copy()
    dim = dim[dim['weight'] > 0]
    dim = dim.drop_duplicates('tracking_number')

    df = df.drop(['weight'], axis=1)
    df = df.merge(dim, left_on='tracking_number', right_on='tracking_number', how='left')

    return df


def get_regular_surcharges(df, fuel_standard, fuel_express):
    """
    currenty out or order.
    """
    df = df[df['description'].isin(SURCHARGES) & (~df['description_panel'].isna())].copy()
    df = df[df['real_price'] > 0]
    df['type'] = 'surcharge'
    df['from_country'] = 'DE'
    df['carrier'] = 'ups'
    df['currency'] = 'EUR'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    default = df[df['description'].isin(DEFAULT)].copy()
    default['price'] = [DEFAULT_PRICE_DICT.get(i) for i in list(default['description'])]
    default['description'] = [DEFAULT_DESCRIPTION_DICT.get(i) for i in list(default['description'])]
    df = df[~df['description'].isin(DEFAULT)].copy()

    df['description'] = [DESCRIPTION_DICT.get(i) for i in list(df['description'])]
    express = df[df['description_panel'].str.contains('Express')].copy()
    express['price'] = [EXPRESS_PRICE_DICT.get(i) for i in list(express['description'])]
    express['price'] = express.apply(get_country_dependent_surcharges_express, axis=1)

    standard = df[~df['description_panel'].str.contains('Express')].copy()
    standard['price'] = [STANDARD_PRICE_DICT.get(i) for i in list(standard['description'])]
    standard['price'] = standard.apply(get_country_dependent_surcharges_standard, axis=1)

    df = pd.concat([default, express, standard])

    lst = []

    for description in df.description.unique():
        x = df[df['description'] == description].copy()
        if description not in ('Zuschlag Service Für Den Erweiterten Bereich', 'Zuschlag Abgelegenes Abholgebiet', 'Zuschlag Zusätzliche Handhabung'):
            lst.append(x)
        elif description == 'Zuschlag Zusätzliche Handhabung':
            # x['price'] = (x['price'] * x['collo_count'])
            x['price'] = x['price'] 
            lst.append(x)
        else:
            y = x[x['shipping_country'] == 'DE'].copy()
            y['price'] = [11.50 if i <= 46 else i * 0.25 for i in list(y['weight'])]
            z = x[x['shipping_country'] != 'DE'].copy()
            z['price'] = [31 if i <= 52 else i * 0.6 for i in list(z['weight'])]
            lst.append(y)
            lst.append(z)

    df = pd.concat(lst)

    # express = df[df['description_panel'].str.contains('Express')].copy()
    # express['real_price'] = (express['real_price'] * (1 + fuel_express)).round(2)

    # standard = df[~df['description_panel'].str.contains('Express')].copy()
    # standard['real_price'] = (standard['real_price'] * (1 + fuel_standard)).round(2)

    # df = pd.concat([express, standard])

    # df = df[MASK].copy()
    
    return df


def get_regular_surcharges(df, fuel_standard, fuel_express):
    df = df[df['description'].isin(SURCHARGES) & (~df['description_panel'].isna() & (df['real_price'] > 0))].reset_index().drop(['index'], axis=1).copy()
    df['type'] = 'surcharge'
    df['from_country'] = 'DE'
    df['carrier'] = 'ups'
    df['currency'] = 'EUR'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['tarife'] = (df['real_price'] / df['weight']).round(2)
    default = df[df['description'].isin(DEFAULT)].copy()
    default['price'] = [DEFAULT_PRICE_DICT.get(i) for i in list(default['description'])]
    default['description'] = [DEFAULT_DESCRIPTION_DICT.get(i) for i in list(default['description'])]
    df = df[~df['description'].isin(DEFAULT)].copy()

    df['description'] = [DESCRIPTION_DICT.get(i) for i in list(df['description'])]
    express = df[df['description_panel'].str.contains('Express')].copy()
    express['price'] = [EXPRESS_PRICE_DICT.get(i) for i in list(express['description'])]
    express['price'] = express.apply(get_country_dependent_surcharges_express, axis=1)

    standard = df[~df['description_panel'].str.contains('Express')].copy()
    standard['price'] = [STANDARD_PRICE_DICT.get(i) for i in list(standard['description'])]
    standard['price'] = standard.apply(get_country_dependent_surcharges_standard, axis=1)

    df = pd.concat([default, express, standard])

    lst = []

    for description in df.description.unique():
        x = df[df['description'] == description].copy()
        if description in ('Zuschlag Zusätzliche Handhabung', 'Zuschlag Aufpreis Für Großes Paket'):
            x['price'] = (x['price'] * x['count'])
            lst.append(x)
        elif description not in ('Zuschlag Service Für Den Erweiterten Bereich', 'Zuschlag Abgelegenes Abholgebiet'):
            lst.append(x)
        else:
            y = x[x['shipping_country'] == 'DE'].copy()
            y['price'] = [11.50 if i <= 46 else i * 0.25 for i in list(y['weight'])]
            z = x[x['shipping_country'] != 'DE'].copy()
            z['price'] = [31 if i <= 52 else i * 0.6 for i in list(z['weight'])]
            lst.append(y)
            lst.append(z)

    df = pd.concat(lst)

    # express = df[df['description_panel'].str.contains('Express')].copy()
    # express['real_price'] = (express['real_price'] * (1 + fuel_express)).round(2)

    # standard = df[~df['description_panel'].str.contains('Express')].copy()
    # standard['real_price'] = (standard['real_price'] * (1 + fuel_standard)).round(2)

    # df = pd.concat([express, standard])

    df = df[MASK].copy()
    
    return df


def get_residential_surcharges(df, fuel_standard, fuel_express):
    df = df[(df['description'].isin(RESIDENTIAL)) & (~df['description_panel'].isna()) & (df['real_price'] > 0)].copy()
    df['type'] = 'surcharge'
    df['from_country'] = 'DE'
    df['carrier'] = 'ups'
    df['currency'] = 'EUR'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    df['description'] = 'Zuschlag für Privatzustellung'

    express = df[df['description_panel'].str.contains('Express')].copy()
    express['price'] = [1 if i == 'DE' else 1.65 for i in list(express['shipping_country'])]
    # express['real_price'] = (express['real_price'] * (1 + fuel_express)).round(2)

    standard = df[~df['description_panel'].str.contains('Express')].copy()
    standard['price'] = [0.95 if i == 'DE' else 1.6 for i in list(standard['shipping_country'])]
    # standard['real_price'] = (standard['real_price'] * (1 + fuel_standard)).round(2)

    df = pd.concat([express, standard])
    df = df[MASK].copy()
    
    return df


def get_return_surcharges(df, fuel_standard, fuel_express):
    df = df[df['description'].isin(RETURNS)].copy()
    df = df.dropna(subset=['description_panel'])
    df = df[df['direction'] != 'i']
    df['price'] = df['price_panel']
    df['description'] = 'Zuschlag Retoure'
    df['type'] = 'surcharge'
    df['from_country'] = 'DE'
    df['carrier'] = 'ups'
    df['currency'] = 'EUR'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)

    # express = df[df['description_panel'].str.contains('Express')].copy()
    # express['real_price'] = (express['real_price'] * (1 + fuel_express)).round(2)

    # standard = df[~df['description_panel'].str.contains('Express')].copy()
    # standard['real_price'] = (standard['real_price'] * (1 + fuel_standard)).round(2)

    # df = pd.concat([express, standard])

    df = df[MASK].copy()
    
    return df        


def get_shipping_method_upsde(df):    
    if df['shipping_method_id'] in (122,123,124,125,126):
        if df['weight']    <= 30:
            return 'ups:standard/service=11,kg=0-30'
        elif df['weight']    <= 40:
            return 'ups:standard/service=11,kg=30-40'
        elif df['weight']    <= 50:
            return 'ups:standard/service=11,kg=40-50'
        elif df['weight']    <= 60:
            return 'ups:standard/service=11,kg=50-60'
        else:
            return 'ups:standard/service=11,kg=60-70'           
    elif df['shipping_method_id'] in (261,267,268,269,270):
        if df['weight']    <= 30:
            return 'ups:standard/service=11,kg=0-30,signature,adult'
        elif df['weight']    <= 40:
            return 'ups:standard/service=11,kg=30-40,signature,adult'
        elif df['weight']    <= 50:
            return 'ups:standard/service=11,kg=40-50,signature,adult'
        elif df['weight']    <= 60:
            return 'ups:standard/service=11,kg=50-60,signature,adult'
        else:
            return 'ups:standard/service=11,kg=60-70,signature,adult'        
    elif df['shipping_method_id'] in (176,177,178,179,180,181,182,183,184,185):
        if df['weight']    <= 3:
            return 'ups:express/service=07,kg=0-3'
        elif df['weight']    <= 6:
            return 'ups:express/service=07,kg=3-6'
        elif df['weight']    <= 10:
            return 'ups:express/service=07,kg=6-10'
        elif df['weight']    <= 15:
            return 'ups:express/service=07,kg=10-15'
        elif df['weight']    <= 20:
            return 'ups:express/service=07,kg=15-20'
        elif df['weight']    <= 30:
            return 'ups:express/service=07,kg=20-30'  
        elif df['weight']    <= 40:
            return 'ups:express/service=07,kg=30-40'
        elif df['weight']    <= 50:
            return 'ups:express/service=07,kg=40-50'
        elif df['weight']    <= 60:
            return 'ups:express/service=07,kg=50-60'
        else:
            return 'UPS Express 60-70kg'
    elif df['shipping_method_id'] in (216,217,218,219,220,221,222,223,224,225):
        if df['weight']    <= 3:
            return 'ups:express/service=07,kg=0-3,saturday'
        elif df['weight']    <= 6:
            return 'ups:express/service=07,kg=3-6,saturday'
        elif df['weight']    <= 10:
            return 'ups:express/service=07,kg=6-10,saturday'
        elif df['weight']    <= 15:
            return 'ups:express/service=07,kg=10-15,saturday'
        elif df['weight']    <= 20:
            return 'ups:express/service=07,kg=15-20,saturday'
        elif df['weight']    <= 30:
            return 'ups:express/service=07,kg=20-30,saturday'  
        elif df['weight']    <= 40:
            return 'ups:express/service=07,kg=30-40,saturday'
        elif df['weight']    <= 50:
            return 'ups:express/service=07,kg=40-50,saturday'
        elif df['weight']    <= 60:
            return 'ups:express/service=07,kg=50-60,saturday'
        else:
            return 'ups:express/service=07,kg=60-70,saturday'
    elif df['shipping_method_id'] in (127,128,129,130,131,132,133,134,135):
        if df['weight']    <= 3:
            return 'ups:standard/service=65,kg=0-3'
        elif df['weight']    <= 6:
            return 'ups:standard/service=65,kg=3-6'
        elif df['weight']    <= 10:
            return 'ups:standard/service=65,kg=6-10'
        elif df['weight']    <= 15:
            return 'ups:standard/service=65,kg=10-15'
        elif df['weight']    <= 20:
            return 'ups:standard/service=65,kg=15-20'
        elif df['weight']    <= 30:
            return 'ups:standard/service=65,kg=20-30'  
        elif df['weight']    <= 40:
            return 'ups:standard/service=65,kg=30-40'
        elif df['weight']    <= 50:
            return 'ups:standard/service=65,kg=40-50'
        elif df['weight']    <= 60:
            return 'ups:standard/service=65,kg=50-60'
        else:
            return 'ups:standard/service=65,kg=60-70'
    elif df['shipping_method_id'] in (311,312,313,314,315):
        if df['weight']    <= 3:
            return 'ups:standard/service=65,kg=0-3,service_point'
        elif df['weight']    <= 6:
            return 'ups:standard/service=65,kg=3-6,service_point'
        elif df['weight']    <= 10:
            return 'ups:standard/service=65,kg=6-10,service_point'
        elif df['weight']    <= 15:
            return 'ups:standard/service=65,kg=10-15,service_point'
        elif df['weight']    <= 20:
            return 'ups:standard/service=65,kg=15-20,service_point'
    else:
        return 'XXX'        


def get_weight_surcharges(df, pricing):
    df = df[df['description'].isin(SHIPPING_METHODS)].copy()
    df = df.dropna(subset=['description_panel'])
    df = df.drop(['real_price'], axis=1)
    df['description'] = df.apply(get_shipping_method_upsde, axis=1)
    df['filter'] = (df['description'] != df['code'])
    df = df[df['filter'] == True].copy()
    df = df[df['description'] != 'XXX'].copy()
    df['id1'] = df['description'] + df['from_country'] + df['shipping_country']
    df['id2'] = df['code'] + df['from_country'] + df['shipping_country']
    max_ = df[df['description'] == 'XXX'].copy()
    
    pt = pd.read_csv(pricing)
    pt['id'] = pt['method_code'] + pt['from_country'] + pt['to_country']
    pr = pt[['id', 'price']].copy()
    rp = pt[['id', 'real_price']].copy()  
    
    df = df.join(pr.set_index('id'), on='id1', how='left')
    df = df.join(pr.set_index('id'), on='id2', how='left', lsuffix='_upsde', rsuffix='panel')
    df = df.join(rp.set_index('id'), on='id1', how='left')
    df = df.join(rp.set_index('id'), on='id2', how='left', lsuffix='_upsde', rsuffix='panel')     
    
    df['price'] = df['price_upsde'] - df['pricepanel']
    df['real_price'] = df['real_price_upsde'] - df['real_pricepanel']    
    
    df['des1'] = [DESCRIPTION_WEIGHT_SURCHARGES_DICT.get(i) for i in list(df['description'])]
    df['des2'] = [DESCRIPTION_WEIGHT_SURCHARGES_DICT.get(i) for i in list(df['code'])]
    df['description'] = 'Ergänzung - ausgewählte Gewichtsklasse: ' + df['des2'] + ' richtiges Gewichtsklasse: ' + df['des1']
    df = df[df['price'] > 0].copy()
    df['type'] = 'surcharge'
    df['from_country'] = 'DE'
    df['carrier'] = 'ups'
    df['currency'] = 'EUR'
    df['internal_note'] = df.apply(general_carrier.get_internal_note, axis=1)
    
    df = df[MASK]
    
    return df, max_         