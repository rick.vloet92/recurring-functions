# libraries
import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
import mrr
import pandas as pd 
import datetime as dt 
import numpy as np
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine
from workalendar.europe import Netherlands


# variabels
CONN_STR = 'postgresql://scp_ro:s25QoAXW@scp-stack-slave-bi.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/scp'
ENGINE = create_engine(CONN_STR, connect_args={'sslmode': 'require', 'options': '-c timezone=cet'})
CONN_JARVIS = 'postgresql://rick_vloet:Mxdye_}A<TUs5Zd!@jarvis.c5ikmqnwg7ko.eu-central-1.rds.amazonaws.com:5432/trusted_zone'
ENGINE_J = create_engine(CONN_JARVIS, connect_args={'sslmode': 'require'})
SAAS_DATE_LIMIT = pd.to_datetime(dt.date.today())
PERIOD = (dt.date.today() - relativedelta(months=1)).strftime('%B')
BROKER_TYPES = ['shipment', 
                'parcel_cancelled',         
                'refund', 
                'pickup_subscription',
                'pickup', 
                'pickup_cancelled',
                'earned_credit', 
				'insurance', 
				'insurance_refund']


# info corrections broker revenue
LEDKONING_CREDIT = 0.72
VERZENDINGEN_ACCOUNTS = [458, 70446]
DPD_NL_TARGET = 1000000
DPD_NL_KICKBACK = 0.1
LA_POSTE_PERCENTAGE = 0.03
MAUT = 0.09
PICKUP_CORRECTION = 77.34
NMG_REAL_PRICE = 3.01
AANTAL_RITTEN = 4
DATE_RANGE = str(dt.date.today() - dt.timedelta(120))


NET_REV_COLUMNS = ['Broker Net Revenue', 
                   'Surcharges Net Revenue', 
                   'Corrections',
                   'Corrections Manual', 
                   'Total Net Revenue Shipments', 
                   'Insurance Net Revenue',
                   'Subscription Net Revenue', 
                   'Total Net Revenue']                


MARGIN_COLUMNS = ['Broker Margin', 
                  'Surcharges Margin', 
                  'Corrections',
                  'Corrections Manual', 
                  'Total Margin Shipments', 
                  'Insurance Margin',
                  'Subscription Margin', 
                  'Total Margin']            

PLAN_DIC = {'Flex': 'Large',
            'Small': 'Small',
            'Business 0-2499': 'Business',
            'Large': 'Large',
            'Plus + SaaS 10000-20000 Yearly 10% off': 'Business',
            'Business 0-2499 Yearly 10% off': 'Business',
            'Large Yearly 10% off': 'Large',
            'Business 10000-12499': 'Business',
            'Flex Split 400-100': 'Large',
            'Business 2500-4999 Yearly 10% off': 'Business',
            'Small Yearly 10% off': 'Small',
            'Flex 0-1000 Yearly 10% off': 'Large',
            'Flex Split 0-400': 'Small',
            'Plus + SaaS ': 'Business',
            'Business 2500-4999': 'Business',
            'Plus Broker Monthly': 'Large',
            'Business 7500-9999 Yearly 10% off': 'Business',
            'Plus + SaaS': 'Business',
            'Business 5000-7499 Yearly 10% off': 'Business',
            'Business 7500-9999': 'Business',
            'Monthly B2': 'Large',
            'Plus + SaaS 1000-2500 Yearly 10% off': 'Business',
            'Monthly A3': 'Small',
            'Flex Split 0-400 Yearly 10% off': 'Small',
            'Flex Split 400-1000 Yearly 10% off': 'Large',
            'Business 5000-7499': 'Business',
            'Monthly A1': 'Small',
            'Monthly B1': 'Large',
            'Monthly B3': 'Large',
            'Monthly A0': 'Small',
            'Monthly B4': 'Large',
            'Monthly B5': 'Large',
            'Monthly A2': 'Small',
            'Monthly C1': 'Large',
            'Monthly P1': 'Business',
            'Plus + SaaS 2500-5000 Yearly 10% off': 'Business',
            'Plus Broker Yearly 1 Month Off': 'Large',
            'Yearly A1': 'Small',
            'Yearly A2': 'Small',
            'Yearly A3': 'Small',
            'Yearly A4': 'Small',
            'Yearly  B4': 'Large',
            'Yearly B5': 'Large',
            'Monthly Pick-Up': 'Small',
            'Business 10000-12499 Yearly 10% off': 'Business',
            '30 days trial SendCloud Plus': 'essential',
            'Backoffice only 14 day trial': 'essential',
            'Business 10000-19999': 'Business',
            'Business 10000-19999 Yearly 10% off': 'Business',
            'Business 20000-29999': 'Business',
            'Business 5000-9999': 'Business',
            'Business 5000-9999 Yearly 10% off': 'Business',
            'essential': 'essential',
            'Large shop': 'Large',
            'Large shop Yearly 10% off': 'Large',
            'Small shop': 'Small',
            'Small shop Yearly 10% off': 'Small', 
            'Backoffice only 3 month trial': 'essential',
            'Monthly A4': 'Small',
            'Business 20000-29999 Yearly 10% off': 'Business', 
            'Business 30000-49999': 'Business'}				  


# queries
broker_invoice_items_query = """
select 
	ii.*, 
    ui.iso_2 "country_user"
from 
	invoice_item ii
left join (select ui.user_id, sc.iso_2
			   from users_invoiceaddress ui 
			left join shipping_country sc 
			   on ui.country_id = sc.shipping_country_id) ui 
	on ii.user_id = ui.user_id
where  
	ii."date" between '{}' and '{}'
and 
	ii."type" in {}
"""


surcharges_revenue_query = """
select
	ii.*,
	ui.iso_2 "country_user"
from
	invoice_item ii
left join 
	(select 
		ui.user_id, sc.iso_2 
	from 
		users_invoiceaddress ui
		 	left join shipping_country sc 
		 		on ui.country_id = sc.shipping_country_id) ui
	on ii.user_id = ui.user_id
where 
	ii."type" = 'surcharge'
and 
	ii."date" >= '{}'
and 
	ii.internal_note ilike '%%{}%%'
"""


bpost_retail_rev_query = """
select 
	ii.*,
    co.iso_2 "country_user"
from
    invoice_item ii
left join carriers_carrier c 
	on ii.carrier_id = c.id
left join (select 
                s.iso_2, u.user_id 
            from 
                users_invoiceaddress u 
            left join shipping_country s 
                on u.country_id = s.shipping_country_id) co 
    on ii.user_id = co.user_id
where
    ii.carrier_id = 4
and 
    ii."date" between '{}' and '{}'
and 
    ii."type" = 'surcharge'
and 
    ii.description = 'bpost retail surcharge'
"""


ledkoning_correction_query = """
select
	ui.iso_2 "country_user",
	- count(*) * {} "ledkoning_credit"
from
	parcel p 
left join 
	(select 
		ui.user_id, sc.iso_2 
	from 
		users_invoiceaddress ui
		 	left join shipping_country sc 
		 		on ui.country_id = sc.shipping_country_id) ui
	on p.user_id = ui.user_id	
where
	p.user_id = 61154
and 
	carrier_code = 'postnl_fulfilment'	
and	
	from_country_id = 2
and 
	shipping_country_id = 2
and
	cancellation_status = 0
and 
	announced_at between '{}' and '{}'	
group by ui.iso_2
"""


verzendingen_account_rev_query = """
select 
	ui.iso_2 "country_user",
	- sum(price) "verzendingen_account_correctie"
from 
	invoice_item ii
left join 
	(select 
		ui.user_id, sc.iso_2 
	from 
		users_invoiceaddress ui
		 	left join shipping_country sc 
		 		on ui.country_id = sc.shipping_country_id) ui
	on ii.user_id = ui.user_id	
where  
	ii."date" between '{}' and '{}'
and 
	ii.user_id in {}    
group by ui.iso_2	
"""


subscription_rev_query = """
select 
	ii.*, 
	ii.price "net_rev",
    su.*, 
    sc."country_user"
from 
	invoice_item ii
left join (select 
		   	cast(su.id as varchar), 
                    su.started_at, 
                    su.expires_at, 
                    ss.plan_type, 
                    sp.price "price_plan",
                    su.price_override,
                    ss.code
			   from subscriptions_usersubscription su 
					left join subscriptions_subscription ss
						on su.subscription_id = ss.id
						left join subscriptions_subscriptionprice sp 
							on su.subscription_id = sp.subscription_id
							where sp.currency = 'EUR') su 
									on ii.reference = su.id
left join (select 
				ui.user_id, sc.iso_2 "country_user" 
		    from 
				users_invoiceaddress ui 
			left join shipping_country sc 
				on ui.country_id = sc.shipping_country_id) sc 
	on ii.user_id = sc.user_id
where 
	ii."date" between '{}' and '{}'
and 
	ii."type" in ('subscription', 'subscription_refund')
and
	su.plan_type in ('monthly', 'yearly')
and
	su.code != 'pickup'	
and
	ii.user_id in (select user_id from users_invoiceaddress where email not ilike '%%@sendcloud%%')
"""


subscription_rev_without_tag_query = """
select
	ii.*,
	ii.price "net_rev",
	uc.iso_2 "country_user"
from 
	invoice_item ii 
left join users_invoiceaddress ui 
	on ii.user_id = ui.user_id
left join shipping_country uc 
	on ui.country_id = uc.shipping_country_id
where 
	ii."type" in ('subscription', 'subscription_refund')
and
    ii."date" between '{}' and '{}'    
"""



saas_manual_credits_query = """
select 
	*
from 
	invoice_item
where 
	"date" >= '{}'
and 
	"type" in ('subscription', 'subscription_refund')
and 
	description not ilike '%%pick%%'
"""


la_poste_credit_query = """
select  
    ui.iso_2 "country_user",
	- sum(real_price) * {} "cogs_correction"
from 
	invoice_item ii
left join (select ui.user_id, sc.iso_2
			   from users_invoiceaddress ui 
			left join shipping_country sc 
			   on ui.country_id = sc.shipping_country_id) ui 
	on ii.user_id = ui.user_id
where  
	ii."date" between '{}' and '{}'
and 
	ii."type" in ('shipment', 'parcel_cancelled')
and 
	ii.carrier_id = 10
group by ui.iso_2	
"""


dpd_de_maut_query = """
select 
	- count(ii.*) * {} "cogs_correction"
from
	invoice_item ii
left join users_invoiceaddress ui
	on ii.user_id = ui.user_id
left join parcel p 
	on ii.parcel_id = p.parcel_id
where 
	ii.carrier_id = 3
and 
	ii."date" between '{}' and '{}'
and 
	p.cancellation_status = 0
and 
	ui.country_id = 3
and 
	ii."type" = 'shipment'
"""


postnl_nmg_credit_query = """
select 
    - count(*) * {} * 0.5 "cogs_correction"
from
    invoice_item 
where
    "date" >= '{}'
and 
    carrier_id = 2
and 
    description ilike '%%aard%%'
and 
	"type" = 'surcharge'
and 
    real_price != 0
and
	internal_note ilike '%%{}%%'
"""


postnl_mailbox_query = """
select 
	p.user_id, 
    ii.description, 
    p.announced_at, 
    ii.real_price
from
	parcel p 
left join invoice_item ii 
	on p.parcel_id = ii.parcel_id
left join contracts_contract cc 
	on cc.id = p.contract_id
where 
	p.shipping_method_id in (39, 503, 505, 507, 504, 506)
and 
	p.cancellation_status = 0
and 
	ii."date" between '{}' and '{}'
and 
	cc."type" != 'direct'
and 
	ii."type" = 'shipment'
"""


verzendingen_account_cogs_query = """
select 
	ii.*, 
    ui.iso_2 "country_user"
from 
	invoice_item ii
left join (select ui.user_id, sc.iso_2
			   from users_invoiceaddress ui 
			left join shipping_country sc 
			   on ui.country_id = sc.shipping_country_id) ui 
	on ii.user_id = ui.user_id
where  
	ii."date" between '{}' and '{}'
and 
	ii."type" = 'refund'
and 
	ii.user_id in {}    
"""


broker_parcels_shipped_query = """
select
	uc.iso_2 "country_user",
	count(*)
from 
	parcel p 
left join contracts_contract cc 
	on cc.id = p.contract_id
left join users_invoiceaddress ui 
	on p.user_id = ui.user_id
left join shipping_country uc 
	on uc.shipping_country_id = ui.country_id
where 
	cc."type" in ('broker', 'subbroker')
and 
	p.announced_at between '{}' and '{}'
and	
	p.cancellation_status = 0	
group by uc.iso_2
"""


all_parcels_shipped_query = """
select
	uc.iso_2 "country_user",
	count(*)
from 
	parcel p 
left join contracts_contract cc 
	on cc.id = p.contract_id
left join users_invoiceaddress ui 
	on p.user_id = ui.user_id
left join shipping_country uc 
	on uc.shipping_country_id = ui.country_id
where  
	p.announced_at between '{}' and '{}'
and	
	p.cancellation_status = 0
group by uc.iso_2
"""


subscription_value_in_month_query = """
select
	*
from 
	sandbox.saas_table_new
where 
	"date" = '{}'
and 
	mrr_type not in  ('churn', 'de-active', 'new-unofficial')    
"""


subscription_value_in_month_query = """
select
	*
from 
	sandbox.saas_table_new
where 
	"date" = '{}'   
"""


subscription_value_in_month_query = """
select
	*
from
	public.saas_subscriptions 
where 
	"date" = '{}'  
"""


country_user_query = """
select
    cast(ui.user_id as varchar),
    uc.iso_2 "country_user"
from
    users_invoiceaddress ui 
left join shipping_country uc 
	on uc.shipping_country_id = ui.country_id
where
    ui.user_id in {}    
"""


net_rev_broker_query = """
select 
	sc.iso_2 "country_user",
	sum(ii.price - ii.real_price) "new_net_rev_broker"
from 
	invoice_item ii 
left join users_invoiceaddress ui 
	on ii.user_id = ui.user_id
left join shipping_country uc 
	on ui.country_id = uc.shipping_country_id
left join "user" u 
	on ii.user_id = u.user_id
where 
	u.first_parcel_at between '{}' and '{}'
and 
	ii."date" between '{}' and '{}'
and 
	ii."type" in ('shipment', 'parcel_cancelled')
group by sc.iso_2
"""


# functions
def get_manual_revenue_corrections(df):
    df = pd.DataFrame(df)
    df = df.transpose()
    df.columns = ['Corrections Manual']

    return df


def get_ledkoning_correction(start_date, end_date):
    df = pd.read_sql_query(ledkoning_correction_query.format(LEDKONING_CREDIT, start_date, end_date), con=ENGINE).set_index('country_user')
    
    return df	


def get_verzendingen_account_revenue(start_date, end_date):
    tu = tuple(VERZENDINGEN_ACCOUNTS[i] for i in range(len(VERZENDINGEN_ACCOUNTS)))
    df = pd.read_sql_query(verzendingen_account_rev_query.format(start_date, end_date, tu), con=ENGINE).set_index('country_user')

    return df


def get_corrections_broker_revenue(ledkoning, verzendingen_account):
	df = ledkoning.join(verzendingen_account)
	df['Corrections'] = df.sum(axis=1)
	df = df[['Corrections']]

	return df


def get_subscription_rev(df, ot, start_date):
    ot = ot.groupby(['user_id', 'date', 'country_user']).agg({'price':sum, 'net_rev':sum}).reset_index().copy()
    df = pd.concat([df, ot])
    df['date'] = df['date'].apply(general_carrier.get_first_day_of_month)
    df = df[df['date'] == str(start_date)]
    df = pd.DataFrame(df.groupby(['country_user']).agg({'price':sum}).rename(columns={'price':'Subscription Revenue'}))         

    return df   


def get_subscription_rev(df, ot, start_date):
    df = pd.concat([df, ot])
    df = df.groupby(['user_id', 'date', 'country_user']).agg({'price':sum, 'net_rev':sum}).reset_index().copy()
    df['date'] = df['date'].apply(general_carrier.get_first_day_of_month)
    df = df[df['date'] == str(start_date)]
    df = pd.DataFrame(df.groupby(['country_user']).agg({'price':sum}).rename(columns={'price':'Subscription Revenue'}))         

    return df 	                     


def get_broker_invoice_items(start_date, end_date):
    tu = tuple(BROKER_TYPES[i] for i in range(len(BROKER_TYPES)))
    df = pd.read_sql_query(broker_invoice_items_query.format(start_date, end_date, tu), con=ENGINE)
    df['country_user'] = df['country_user'].replace('MC', 'FR')
    df['country_user'] = df['country_user'].replace('AD', 'FR')
    df['country_user'] = df['country_user'].replace('IE', 'GB')
    df['date'] = pd.to_datetime(df['date'], utc=True)
    df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
    df['date'] = [i.tz_localize(None) for i in list(df['date'])]
    
    return df


def get_surcharge_revenue(start_date, end_date):
	date_surcharges = (start_date - relativedelta(months=6)).replace(day=1)
	start_date = (start_date - relativedelta(months=1)).replace(day=1)
	month = start_date.strftime('%B')
	end_date = (end_date - relativedelta(months=1)).replace(day=1)

	df1 = pd.read_sql_query(surcharges_revenue_query.format(date_surcharges, month), con=ENGINE)
	df2 = pd.read_sql_query(bpost_retail_rev_query.format(start_date, end_date), con=ENGINE)
	df = pd.concat([df1, df2])

	df['country_user'] = df['country_user'].replace('MC', 'FR')
	df['country_user'] = df['country_user'].replace('AD', 'FR')
	df['country_user'] = df['country_user'].replace('IE', 'GB')
	df['date'] = pd.to_datetime(df['date'], utc=True)
	df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
	df['date'] = [i.tz_localize(None) for i in list(df['date'])]

	df = pd.DataFrame(df.groupby('country_user').agg({'price':sum}).rename(columns={'price':'Surcharges Revenue'}))	

	return df

def get_pivot_table_broker_revenue(broker, surcharges, corrections, corrections_manual, subscriptions):
	insurance = broker[broker['type'].isin(['insurance', 'insurance_refund'])]
	broker = broker[~broker['type'].isin(['insurance', 'insurance_refund'])]

	broker = pd.DataFrame(broker.groupby(['country_user']).agg({'price':sum}).rename(columns={'price':'Broker Revenue'}))
	insurance = pd.DataFrame(insurance.groupby(['country_user']).agg({'price':sum}).rename(columns={'price':'Insurance Revenue'}))
	
	df = broker.join(surcharges)
	df = df.join(corrections)
	df = df.join(corrections_manual)
	df['Total Revenue Shipments'] = df.sum(axis=1, numeric_only=True)
	df = df.join(insurance)
	df = df.join(subscriptions)
	df['Total Revenue'] = df.iloc[:, -3:].sum(axis=1, numeric_only=True)
	df = df.append(df.sum(numeric_only=True).rename('Global')).fillna(0).round()

	return df	


def get_surcharge_cogs(start_date, end_date):
	date_surcharges = (start_date - relativedelta(months=6)).replace(day=1)
	start_date = (start_date - relativedelta(months=1)).replace(day=1)
	month = start_date.strftime('%B')
	end_date = (end_date - relativedelta(months=1)).replace(day=1)

	df1 = pd.read_sql_query(surcharges_revenue_query.format(date_surcharges, month), con=ENGINE)
	df2 = pd.read_sql_query(bpost_retail_rev_query.format(start_date, end_date), con=ENGINE)
	df = pd.concat([df1, df2])

	df['country_user'] = df['country_user'].replace('MC', 'FR')
	df['country_user'] = df['country_user'].replace('AD', 'FR')
	df['country_user'] = df['country_user'].replace('IE', 'GB')
	df['date'] = pd.to_datetime(df['date'], utc=True)
	df['date'] = df['date'].dt.tz_convert('Europe/Berlin')
	df['date'] = [i.tz_localize(None) for i in list(df['date'])]

	df = pd.DataFrame(df.groupby('country_user').agg({'real_price':sum}).rename(columns={'real_price':'Surcharges Cogs'}))	

	return df	


def get_calculate_kickback_dpdnl(df):
    x = 0
    lst = []
    
    for c, value in enumerate(df.cumsum_parcels_shipped, 0):
        if value > DPD_NL_TARGET and x == 0:
            lst.append((df.cumsum_parcels_shipped[c] - DPD_NL_TARGET) * DPD_NL_KICKBACK)
            x = x + 1
        elif value > DPD_NL_TARGET and x != 0:
            lst.append(df.parcels_shipped[c] * DPD_NL_KICKBACK)
        else:
            lst.append(0)
            
    return lst


def get_kickback_dpdnl():
    df = pd.read_sql_query(dpdnl_kickback_query, con=ENGINE)
    df['date'] = df.date.apply(get_first_day_of_month)
    df = pd.DataFrame(df.groupby(['date']).agg({'parcel_id':'count'}).rename(columns={'parcel_id':'parcels_shipped'}))
    df['cumsum_parcels_shipped'] = df['parcels_shipped'].cumsum()
    df['kickback'] = get_calculate_kickback_dpdnl(df)
    df['kickback'] = df['kickback'] * -1
    kickback = round(df.kickback[df.index[-2]])
    
    return df, kickback


def get_la_poste_correction(start_date, end_date):
    df = pd.read_sql_query(la_poste_credit_query.format(LA_POSTE_PERCENTAGE, start_date, end_date), con=ENGINE).set_index('country_user')
    
    return df	


def get_dpd_de_maut_correction(start_date, end_date):
    df = pd.read_sql_query(dpd_de_maut_query.format(MAUT, start_date, end_date), con=ENGINE)
    df.index = ['DE']
    
    return df


def get_postnl_nmg_kickback(start_date):
	start_date = (start_date - relativedelta(months=1))
	month = start_date.strftime('%B')
	df = pd.read_sql_query(postnl_nmg_credit_query.format(NMG_REAL_PRICE, DATE_RANGE, month), con=ENGINE)
	df.index= ['NL']

	return df


def get_verzendingen_account_cogs_correction(start_date, end_date):
	tu = tuple(VERZENDINGEN_ACCOUNTS[i] for i in range(len(VERZENDINGEN_ACCOUNTS)))
	df = pd.read_sql_query(verzendingen_account_cogs_query.format(start_date, end_date, tu), con=ENGINE)
	df = pd.DataFrame({'cogs_correction':[df.real_price.abs().sum()]})
	df.index = ['NL']

	return df
	

def get_postnl_collection_pickup_correction(start_date, end_date):
    cal = Netherlands()
    wd = cal.get_working_days_delta(pd.to_datetime(start_date), end=pd.to_datetime(end_date))
    credit = (AANTAL_RITTEN * PICKUP_CORRECTION * wd)
    df = pd.DataFrame({'cogs_correction':[credit]})
    df.index = ['NL']
    
    return df


def get_postnl_mailbox_description(df):
    if df['parcels (#)'] > 833.3:
        return 'PostNL Mailbox Parcel Extra 0-2kg A'
    elif df['parcels (#)'] > 416.7:
        return 'PostNL Mailbox Parcel Extra 0-2kg A+'
    elif df['parcels (#)'] > 208.3:
        return 'PostNL Mailbox Parcel Extra 0-2kg B'
    elif df['parcels (#)'] > 83.3:
        return 'PostNL Mailbox Parcel Extra 0-2kg C'
    elif df['parcels (#)'] > 41.7:
        return 'PostNL Mailbox Parcel Extra 0-2kg D'
    else:
        return 'PostNL Mailbox Parcel Extra 0-2kg'	


def get_postnl_mailbox_credit(start_date, end_date):
    """
    Returns the credit regarding the PostNL mailbox credit.
    """
    df = pd.read_sql_query(postnl_mailbox_query.format(start_date, end_date), con=ENGINE)
    df['month'] = df['announced_at'].apply(general_carrier.get_first_day_of_month)
    pt = pd.DataFrame(df.groupby(['description', 'real_price'])['month'].count())
    pt = pt.sort_index(ascending=False)
    pt.reset_index(inplace=True)
    pt = pt.drop_duplicates('description')   
    pt = pt.drop(columns='month')
    pt = pt.sort_values(by='real_price', ascending=False)
    
    ps = pd.DataFrame(df.groupby('user_id').agg({'real_price':['count', 'mean']}))
    ps.reset_index(inplace=True)
    ps.columns = ['user_id', 'parcels (#)', 'real_price_mean']
    ps['description_to_be'] = ps.apply(get_postnl_mailbox_description, axis=1)  
    
    df = ps.join(pt.set_index('description'), on='description_to_be', how='left')
    df['credit'] = round(df['parcels (#)'] * (df['real_price_mean'] - df['real_price']), 2)
    credit = -df.credit.sum()
    df = pd.DataFrame({'cogs_correction':[credit]})
    df.index = ['NL']
    
    return df


def get_corrections_broker_cogs(la_poste, dpd_de_maut, postnl_nmg_kickback, verzendingen_account, collection_pickup_correction, postnl_mailbox_correction):
    df = pd.concat([la_poste, dpd_de_maut, postnl_nmg_kickback, verzendingen_account, collection_pickup_correction, postnl_mailbox_correction]).reset_index()
    df = pd.DataFrame(df.groupby('index').agg({'cogs_correction':sum}).rename(columns={'cogs_correction':'Corrections'}))

    return df


def get_pivot_table_broker_cogs(broker, surcharges, corrections, corrections_manual):
	insurance = broker[broker['type'].isin(['insurance', 'insurance_refund'])]
	broker = broker[~broker['type'].isin(['insurance', 'insurance_refund'])]

	broker = pd.DataFrame(broker.groupby(['country_user']).agg({'real_price':sum}).rename(columns={'real_price':'Broker Cogs'}))
	insurance = pd.DataFrame(insurance.groupby(['country_user']).agg({'real_price':sum}).rename(columns={'real_price':'Insurance Cogs'}))
	
	df = broker.join(surcharges)
	df = df.join(corrections)
	df = df.join(corrections_manual)
	df['Total Cogs Shipments'] = df.sum(axis=1, numeric_only=True)
	df = df.join(insurance)
	df['Subscription Cogs'] = 0
	df['Total Cogs'] = df.iloc[:, -3:].sum(axis=1, numeric_only=True)
	df = df.append(df.sum(numeric_only=True).rename('Global')).fillna(0).round()

	return df


def get_margin(revenue, cogs):
    rev = pd.read_csv(revenue, index_col=['country_user'])
    cogs = pd.read_csv(cogs, index_col=['country_user'])
    net_rev = rev.subtract(cogs.values)
    net_rev.columns = NET_REV_COLUMNS
    margin = round(net_rev.divide(rev.values), 4) * 100
    margin.columns = MARGIN_COLUMNS
    
    return rev, cogs, net_rev, margin


def get_broker_parcels_shipped_in_month(start_date, end_date):
    df = pd.read_sql_query(broker_parcels_shipped_query.format(start_date, end_date), con=ENGINE)
    df['country_user'] = [i.replace('MC', 'FR') for i in list(df['country_user'])]
    df['country_user'] = [i.replace('AD', 'FR') for i in list(df['country_user'])]
    df['country_user'] = df['country_user'].replace('IE', 'GB')
    df = pd.DataFrame(df.groupby('country_user').agg({'count':sum}))
    df.columns = ['Total Broker Shipments']
    df = df.append(df.sum(numeric_only=True).rename('Global'))
    
    return df


def get_total_parcels_shipped_in_month(start_date, end_date):
    df = pd.read_sql_query(all_parcels_shipped_query.format(start_date, end_date), con=ENGINE)
    df['country_user'] = [i.replace('MC', 'FR') for i in list(df['country_user'])]
    df['country_user'] = [i.replace('AD', 'FR') for i in list(df['country_user'])]
    df['country_user'] = df['country_user'].replace('IE', 'GB')
    df = pd.DataFrame(df.groupby('country_user').agg({'count':sum}))
    df.columns = ['Total Actual Shipments']
    df = df.append(df.sum(numeric_only=True).rename('Global'))
    
    return df


def get_subscription_value_in_month(end_date):
    df = pd.read_sql_query(subscription_value_in_month_query.format(end_date), con=ENGINE_J)
    tn = list(df['user_id'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(country_user_query.format(tu), con=ENGINE)
    df['user_id'] = df['user_id'].astype(str)
    df = df.join(pa.set_index('user_id'), on='user_id', how='left')
    df['country_user'] = [i.replace('MC', 'FR') for i in list(df['country_user'])]
    df['country_user'] = [i.replace('AD', 'FR') for i in list(df['country_user'])]
    df['country_user'] = df['country_user'].replace('IE', 'GB')
    df = pd.DataFrame(df.groupby('country_user').agg({'fmp_o':sum}).rename(columns={'fmp_o':'subscription_value'}))
    df = df.append(df.sum().rename('Global'))

    return df


def get_subscription_value_in_month(end_date):
    df = pd.read_sql_query(subscription_value_in_month_query.format(end_date), con=ENGINE_J)
    df = df[~df['mrr_type'].isin(['nan', 'NA'])].copy()
    tn = list(df['user_id'])
    tu = tuple(tn[i] for i in range(len(tn)))
    pa = pd.read_sql_query(country_user_query.format(tu), con=ENGINE)
    df['user_id'] = df['user_id'].astype(str)
    df = df.join(pa.set_index('user_id'), on='user_id', how='left')
    df = df[~df['mrr_type'].isna()].copy()
    df['country_user'] = [i.replace('MC', 'FR') for i in list(df['country_user'])]
    df['country_user'] = [i.replace('AD', 'FR') for i in list(df['country_user'])]
    df['country_user'] = df['country_user'].replace('IE', 'GB')
    df = pd.DataFrame(df.groupby('country_user').agg({'actual_subscription_value':sum}).rename(columns={'actual_subscription_value':'subscription_value'}))
    df = df.append(df.sum().rename('Global'))

    return df


def get_monthly_reporting_overviews(margin, broker_parcels_shipped, total_parcels_shipped, net_rev, actual_subs_value, rev, cogs):
    net_rev = net_rev[['Total Net Revenue']]
    ov = pd.concat([margin, broker_parcels_shipped, total_parcels_shipped, net_rev, actual_subs_value], axis=1).round(2)
    rc = pd.concat([rev, cogs], axis=1).round(2)

    return ov, rc    