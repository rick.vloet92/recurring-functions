# libraries

import sys
sys.path.append('C:\\Users\\Rick Vloet\\Documents\\git\\recurring-functions\\')
import general_carrier
from fbprophet import Prophet
import pandas as pd 
import numpy as np
import holidays
from dateutil.relativedelta import relativedelta
import datetime as dt


# notes

"""
The input to Prophet is always a dataframe with two columns: ds and y. The ds (datestamp) column should be of a format expected by Pandas, ideally YYYY-MM-DD for a date or YYYY-MM-DD HH:MM:SS for a timestamp. The y column must be numeric, and represents the measurement we wish to forecast.
"""


"""
** data die gebruikt is voor het forecasten zijn alle parcels (dus niet cancellation_status = 0)
** Juni is eigenlijk altijd hoger dan July en Augustus
** Januari is ongeveer net zo hoog als October van het jaar daarvoor
** Na het forecasten zijn de parcels gecorrigeerd voor 2.7% voor gecancellede parcels
** Feestdagen die altijd op dezelfde datum vallen alleen meenemen als ze niet in het weekend vallen (vb: Kerst, Koningsdag)

country user NL:

- Eerste half jaar geforecast met additive seasonality
- Tweede half jaar geforecast met multiplicative seasonality
- Daarnaast juli en augustus met 20k parcels gecorrigeerd en deze bij juni opgeteld
- Verdeling van de parcels over de maanden gecheckt met voorgaande jaren

country user BE:
- multiplicative seasonality
- december gecorrigeerd voor 15k parcels naar beneden
- juni gecorrigeerd voor 5k parcels naar beneden

country user DE:
- Januari en Februari + 15k parcels
- September -5k parcels

country user AT:
- Januari en Februari + 4k parcels
- Maart t/m October + 2k parcels

country user FR:
- augustus - 5k parcels
- october + 5k parcels
- december -12k parcels
"""


# variables

STATIC_HOLIDAYS = ('Eerste Kerstdag', 
                   'Tweede Kerstdag', 
                   'Koningsdag', 
                   'Dag van de Arbeid', 
                   'Nationale feestdag', 
                   'Allerheiligen', 
                   'Wapenstilstand', 
                   'Kerstmis', 
                   'Neujahr', 
                   'Erster Mai', 
                   'Tag der Deutschen Einheit', 
                   'Reformationstag', 
                   'Erster Weihnachtstag', 
                   'Zweiter Weihnachtstag', 
                   'Heilige Drei Könige', 
                   'Mariä Himmelfahrt', 
                   'Nationalfeiertag', 
                   'Allerheiligen', 
                   'Mariä Empfängnis', 
                   'Christtag', 
                   'Stefanitag', 
                   "Jour de l'an", 
                   'Fête du Travail', 
                   'Armistice 1945', 
                   'Fête nationale', 
                   'Assomption', 
                   'Toussaint', 
                   'Noël')


CANCELLATION_PERCENTAGE = 0.027


# query

broker_parcel_query = """
select 
	date_trunc('day', p.announced_at) "ds",
	count(*) "y"
from 
	parcel p 
left join users_invoiceaddress ui 
	on p.user_id = ui.user_id
left join contracts_contract cc 
	on cc.id = p.contract_id
where 
	ui.country_id = 2
and 
	cc."type" in ('broker', 'subbroker')
and 
	p.announced_at between '2020-10-01' and '2021-05-17'
group by date_trunc('day', p.announced_at)
"""


# functions

def get_parcel_data_daily(path, start_date):
    df = pd.read_csv(path)
    df['ds'] = pd.to_datetime(df['ds'], dayfirst=True)
    df.set_index('ds', inplace=True)
    df = df[start_date:].reset_index()

    return df


def get_parcel_distribution(df):
    df['month'] = df['ds'].dt.month 
    df['year'] = df['ds'].dt.year
    pi = df.pivot_table(index='year', columns='month', values='y', aggfunc=sum)
    pi['sum'] = np.sum(pi, axis=1)
    verh = pi.divide(pi.iloc[:,-1], axis=0)

    return pi, verh


def get_working_days_df(country, start_date, end_date):
    if country == 'FR':
        country = 'FRA'
        
    dutch_holidays = holidays.CountryHoliday(country)

    dic = {}

    for i in pd.date_range(start_date, end_date, freq='D'):
        if (dutch_holidays.get(i)):
           dic[i] = dutch_holidays.get(i)

    ho = pd.DataFrame(dic, index=['holiday']).transpose()
    ho = ho.reset_index()
    ho.columns = ['ds', 'holiday']
    df = pd.DataFrame({'ds':pd.date_range(start_date, end_date, freq='D')})
    df = df.join(ho.set_index('ds'), on='ds', how='left')
    df = df[(df['holiday'].isna()) & (df['ds'].dt.weekday < 5)]
    df['year'] = df['ds'].dt.year
    df['month'] = df['ds'].dt.month
    df['day'] = df['ds'].dt.strftime('%A')
    df['day'] = pd.Categorical(df['day'], ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'])
    wd = df.pivot_table(index=['year'], columns=['month'], values=['ds'], aggfunc=len)
    wd['sum'] = np.sum(wd, axis=1)
    dof = df.pivot_table(index=['year', 'day'], columns=['month'], values=['ds'], aggfunc=len)

    return wd, dof


def get_temper_covid_effect(df):
    perc = (np.percentile(df['y'], 75) - np.percentile(df['y'], 25)) * 3
    df = df[(df['ds'] >= '2020-03-01') & (df['ds'] < '2020-08-01')].reset_index().drop(['index'], axis=1)

    for i in range(len(df)):
        if (df['y'][i]) >= perc or ((df['y'][i]) <= np.median(df.loc[(df['ds'] >= '2020-01-01'), 'y']) and df['ds'][i].weekday() == 0):
            covid_start = df['ds'][i]
            break
            
    df = df.sort_values('ds', ascending=False).reset_index().drop(['index'], axis=1)  

    for i in range(len(df)):
        if (df['y'][i]) >= perc:
            covid_end = df['ds'][i]
            break 

    return covid_start, covid_end


def get_country_holidays_df(country, start_date, end_date):
    if country == 'FR':
        country = 'FRA'
    dutch_holidays = holidays.CountryHoliday(country)

    dic = {}

    for i in pd.date_range(start_date, end_date, freq='D'):
        if (dutch_holidays.get(i)):
           dic[i] = dutch_holidays.get(i)

    df = pd.DataFrame(dic, index=['holiday']).transpose()

    return df


def get_black_friday_and_cyber_monday_df(start_date, end_date):
    us_holidays = holidays.US()

    dic = {}

    for i in pd.date_range(start_date, end_date, freq='D'):
        if (us_holidays.get(i)) == 'Thanksgiving':
            dic[i + relativedelta(days=1)] = 'Black Friday'
            dic[i + relativedelta(days=4)] = 'Cyber Monday'

    df = pd.DataFrame(dic, index=['holiday']).transpose()

    return df


def get_remove_holidays_in_the_weekend(df):
    if df['holiday'] in STATIC_HOLIDAYS and df['weekday'] in (5, 6):
        return False
    else:
        return True


def get_total_holidays(country, start_date, end_date):
    ch = get_country_holidays_df(country, start_date, end_date)
    bf = get_black_friday_and_cyber_monday_df(start_date, end_date)
    df = pd.concat([ch, bf])
    df = df.reset_index()
    df.columns = ['ds', 'holiday']
    df['ds'] = pd.to_datetime(df['ds'])
    df = df.sort_values('ds')
    df['weekday'] = df['ds'].dt.weekday
    df['filter'] = df.apply(get_remove_holidays_in_the_weekend, axis=1)
    df = df[df['filter'] == True]
    df = df.drop(['weekday', 'filter'], axis=1)

    return df


def get_fbprophet(df, country, holidays, end_date):
    df.loc[(df['ds'] >= '2020-03-09') & (df['ds'] < '2020-07-27'), 'y'] = np.nan

    if country == 'DE':
        df.loc[df['ds'] == '2020-09-15', 'y'] = np.nan
        df.loc[(df['ds'] >= '2020-10-12') & (df['ds'] < '2020-10-26'), 'y'] = np.nan

    if country == 'AT':
        df.loc[(df['ds'] >= '2019-12-23') & (df['ds'] < '2020-01-06'), 'y'] = np.nan
        df.loc[(df['ds'] >= '2020-10-19') & (df['ds'] < '2020-10-30'), 'y'] = np.nan
        
    # model = Prophet(holidays=holidays)
    model = Prophet(holidays=holidays, seasonality_mode='multiplicative')
    model.fit(df)
    future_df = model.make_future_dataframe((pd.to_datetime(end_date) - df.iloc[-1,0]).days, 'D')
    forecast = model.predict(future_df)    

    return model, forecast


def get_prediction_df(forecast):
    pa = forecast[forecast['ds'] >= '2020-10-01'].copy()
    pa['yhat'] = round(pa['yhat'] * (1 - CANCELLATION_PERCENTAGE))
    pa['ds'] = pa['ds'].apply(general_carrier.get_first_day_of_month)
    pa = pa.groupby('ds').agg({'yhat':sum, 'yhat_lower':sum, 'yhat_upper':sum}).round()
    pa['grow'] = pa['yhat'].shift(-1)
    pa['grow'] = (pa['grow'] - pa['yhat']) / pa['yhat']
    pa['grow'] = pa['grow'].shift(1)
    pa['index'] = pa['yhat'] / pa['yhat']['2021-01-01']
    pa.iloc[:3, -1] = np.nan


    return pa                   